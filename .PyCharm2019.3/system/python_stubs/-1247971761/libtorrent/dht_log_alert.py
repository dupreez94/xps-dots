# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .alert import alert

class dht_log_alert(alert):
    # no doc
    def log_message(self, dht_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log_message( (dht_log_alert)arg1) -> str :
        
            C++ signature :
                char const* log_message(libtorrent::dht_log_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    module = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



