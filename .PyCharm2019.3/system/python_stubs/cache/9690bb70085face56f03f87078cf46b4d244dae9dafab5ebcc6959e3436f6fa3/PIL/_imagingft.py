# encoding: utf-8
# module PIL._imagingft
# from /usr/lib/python3.8/site-packages/PIL/_imagingft.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

freetype2_version = '2.10.1'

HAVE_RAQM = True

# functions

def getfont(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>'

__spec__ = None # (!) real value is "ModuleSpec(name='PIL._imagingft', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>, origin='/usr/lib/python3.8/site-packages/PIL/_imagingft.cpython-38-x86_64-linux-gnu.so')"

