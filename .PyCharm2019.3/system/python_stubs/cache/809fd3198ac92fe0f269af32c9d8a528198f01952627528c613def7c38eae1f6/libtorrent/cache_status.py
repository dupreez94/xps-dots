# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class cache_status(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    arc_mfu_ghost_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    arc_mfu_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    arc_mru_ghost_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    arc_mru_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    average_hash_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    average_job_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    average_read_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    average_write_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    blocked_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    blocks_read = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    blocks_read_hit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    blocks_written = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cache_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cumulative_hash_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cumulative_job_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cumulative_read_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cumulative_write_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_read_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_write_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peak_queued = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pending_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pinned_blocks = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    queued_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    queued_jobs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    reads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_cache_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_queue_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_read_back = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_used_buffers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    writes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    write_cache_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 248


