# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class proxy_type_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    http = 4
    http_pw = 5
    i2p_proxy = 6
    names = {
        'http': 4,
        'http_pw': 5,
        'i2p_proxy': 6,
        'none': 0,
        'socks4': 1,
        'socks5': 2,
        'socks5_pw': 3,
    }
    none = 0
    proxy_settings = None # (!) real value is "<class 'libtorrent.proxy_settings'>"
    proxy_type = None # (!) forward: proxy_type_t, real value is "<class 'libtorrent.proxy_type_t'>"
    socks4 = 1
    socks5 = 2
    socks5_pw = 3
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
    }
    __slots__ = ()


