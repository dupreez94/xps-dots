# encoding: utf-8
# module pcardext
# from /usr/lib/python3.8/site-packages/pcardext.so
# by generator 1.147
""" Python extension for HP photocard services """
# no imports

# functions

def cd(*args, **kwargs): # real signature unknown
    pass

def cp(*args, **kwargs): # real signature unknown
    pass

def df(*args, **kwargs): # real signature unknown
    pass

def info(*args, **kwargs): # real signature unknown
    pass

def ls(*args, **kwargs): # real signature unknown
    pass

def mount(*args, **kwargs): # real signature unknown
    pass

def read(*args, **kwargs): # real signature unknown
    pass

def rm(*args, **kwargs): # real signature unknown
    pass

def umount(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='pcardext', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/pcardext.so')"

