# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QTextObject(__PyQt5_QtCore.QObject):
    """ QTextObject(QTextDocument) """
    def document(self): # real signature unknown; restored from __doc__
        """ document(self) -> QTextDocument """
        return QTextDocument

    def format(self): # real signature unknown; restored from __doc__
        """ format(self) -> QTextFormat """
        return QTextFormat

    def formatIndex(self): # real signature unknown; restored from __doc__
        """ formatIndex(self) -> int """
        return 0

    def objectIndex(self): # real signature unknown; restored from __doc__
        """ objectIndex(self) -> int """
        return 0

    def setFormat(self, QTextFormat): # real signature unknown; restored from __doc__
        """ setFormat(self, QTextFormat) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass


