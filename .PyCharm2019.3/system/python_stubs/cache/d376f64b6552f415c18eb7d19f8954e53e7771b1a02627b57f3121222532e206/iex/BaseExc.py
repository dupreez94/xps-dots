# encoding: utf-8
# module iex
# from /usr/lib/python3.8/site-packages/iex.so
# by generator 1.147
# no doc
# no imports

from .RuntimeError import RuntimeError

class BaseExc(RuntimeError):
    # no doc
    def __init__(self, v=None): # reliably restored by inspect
        # no doc
        pass

    def __repr__(self): # reliably restored by inspect
        # no doc
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



