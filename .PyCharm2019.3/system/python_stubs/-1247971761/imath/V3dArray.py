# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V3dArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec3 """
    def bounds(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bounds( (V3dArray)arg1) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > bounds(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def cross(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V3dArray)arg1, (V3d)x) -> V3dArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > cross(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        cross( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > cross(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def dot(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V3dArray)arg1, (V3d)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        dot( (V3dArray)arg1, (V3dArray)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def ifelse(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V3dArray)arg1, (IntArray)arg2, (V3d)arg3) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec3<double>)
        
        ifelse( (V3dArray)arg1, (IntArray)arg2, (V3dArray)arg3) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def length(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V3dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def length2(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V3dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length2(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def max(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V3dArray)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> max(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def min(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V3dArray)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> min(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def normalize(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V3dArray)arg1) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def normalized(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V3dArray)arg1) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > normalized(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def reduce(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V3dArray)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> reduce(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __add__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __add__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __copy__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V3dArray)arg1) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __copy__(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __deepcopy__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V3dArray)arg1, (dict)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec3<double> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __div__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __div__( (V3dArray)arg1, (float)x) -> V3dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __div__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __eq__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V3dArray)arg1, (V3d)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __eq__( (V3dArray)arg1, (V3dArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __getitem__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V3dArray)arg1, (object)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},_object*)
        
        __getitem__( (V3dArray)arg1, (IntArray)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V3dArray)arg1, (int)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __getitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},long)
        
        __getitem__( (V3dArray)arg1, (int)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},long)
        """
        pass

    def __iadd__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __iadd__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __idiv__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __idiv__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __idiv__( (V3dArray)arg1, (float)x) -> V3dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __idiv__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __imul__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __imul__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __imul__( (V3dArray)arg1, (float)x) -> V3dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __imul__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V3dArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __init__( (object)arg1, (V3d)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>,unsigned long)
        
        __init__( (object)arg1, (V3iArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec3<int> >)
        
        __init__( (object)arg1, (V3fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __isub__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __isub__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __itruediv__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __itruediv__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __itruediv__( (V3dArray)arg1, (float)x) -> V3dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __itruediv__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __len__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V3dArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def __mul__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __mul__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __mul__( (V3dArray)arg1, (float)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __mul__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        
        __mul__( (V3dArray)arg1, (M44f)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Matrix44<float>)
        
        __mul__( (V3dArray)arg1, (M44d)x) -> V3dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __neg__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V3dArray)arg1) -> V3dArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __neg__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def __ne__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V3dArray)arg1, (V3d)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __ne__( (V3dArray)arg1, (V3dArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __radd__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __radd__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __rmul__( (V3dArray)arg1, (float)x) -> V3dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __rmul__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __rsub__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __setitem__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V3dArray)arg1, (object)arg2, (V3d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},_object*,Imath_2_4::Vec3<double>)
        
        __setitem__( (V3dArray)arg1, (IntArray)arg2, (V3d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec3<double>)
        
        __setitem__( (V3dArray)arg1, (object)arg2, (V3dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __setitem__( (V3dArray)arg1, (IntArray)arg2, (V3dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __setitem__( (V3dArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},long,boost::python::tuple)
        """
        pass

    def __sub__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __sub__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __truediv__(self, V3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V3dArray)arg1, (V3d)x) -> V3dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        __truediv__( (V3dArray)arg1, (V3dArray)x) -> V3dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        __truediv__( (V3dArray)arg1, (float)x) -> V3dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},double)
        
        __truediv__( (V3dArray)arg1, (DoubleArray)x) -> V3dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


