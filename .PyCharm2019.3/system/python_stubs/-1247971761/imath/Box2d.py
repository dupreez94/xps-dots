# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box2d(__Boost_Python.instance):
    # no doc
    def center(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        center( (Box2d)arg1) -> V2d :
            center() center of the box
        
            C++ signature :
                Imath_2_4::Vec2<double> center(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def extendBy(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extendBy( (Box2d)arg1, (V2d)arg2) -> None :
            extendBy(point) extend the box by a point
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        extendBy( (Box2d)arg1, (V2dArray)arg2) -> None :
            extendBy(array) extend the box the values in the array
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        extendBy( (Box2d)arg1, (Box2d)arg2) -> None :
            extendBy(box) extend the box by a box
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        """
        pass

    def hasVolume(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hasVolume( (Box2d)arg1) -> bool :
            hasVolume() returns true if the box has volume
        
            C++ signature :
                bool hasVolume(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def intersects(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersects( (Box2d)arg1, (V2d)arg2) -> bool :
            intersects(point) returns true if the box intersects the given point
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        intersects( (Box2d)arg1, (Box2d)arg2) -> bool :
            intersects(box) returns true if the box intersects the given box
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        """
        pass

    def isEmpty(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isEmpty( (Box2d)arg1) -> bool :
            isEmpty() returns true if the box is empty
        
            C++ signature :
                bool isEmpty(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def isInfinite(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isInfinite( (Box2d)arg1) -> bool :
            isInfinite() returns true if the box covers all space
        
            C++ signature :
                bool isInfinite(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def majorAxis(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        majorAxis( (Box2d)arg1) -> int :
            majorAxis() major axis of the box
        
            C++ signature :
                unsigned int majorAxis(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def makeEmpty(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeEmpty( (Box2d)arg1) -> None :
            makeEmpty() make the box empty
        
            C++ signature :
                void makeEmpty(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def makeInfinite(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeInfinite( (Box2d)arg1) -> None :
            makeInfinite() make the box cover all space
        
            C++ signature :
                void makeInfinite(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def max(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (Box2d)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> max(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def min(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (Box2d)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> min(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def setMax(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMax( (Box2d)arg1, (V2d)arg2) -> None :
            setMax() sets the max value of the box
        
            C++ signature :
                void setMax(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def setMin(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMin( (Box2d)arg1, (V2d)arg2) -> None :
            setMin() sets the min value of the box
        
            C++ signature :
                void setMin(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def size(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Box2d)arg1) -> V2d :
            size() size of the box
        
            C++ signature :
                Imath_2_4::Vec2<double> size(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def __eq__(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Box2d)arg1, (Box2d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
            Box() create empty box
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (V2d)arg2) -> None :
            Box(point)create box containing the given point
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<double>)
        
        __init__( (object)arg1, (V2d)arg2, (V2d)arg3) -> None :
            Box(point,point) create box continaing min and max
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            Box(point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
            Box(point,point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (Box2f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        
        __init__( (object)arg1, (Box2d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        
        __init__( (object)arg1, (Box2i)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def __ne__(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Box2d)arg1, (Box2d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Box<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Box2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Box2d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        """
        pass

    __instance_size__ = 48


