# encoding: utf-8
# module btrfsutil
# from /usr/lib/python3.8/site-packages/btrfsutil.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Library for managing Btrfs filesystems """
# no imports

# Variables with simple values

ERROR_DEFAULT_SUBVOL_FAILED = 19

ERROR_FS_INFO_FAILED = 26

ERROR_GET_SUBVOL_INFO_FAILED = 23

ERROR_GET_SUBVOL_ROOTREF_FAILED = 24

ERROR_INO_LOOKUP_FAILED = 13

ERROR_INO_LOOKUP_USER_FAILED = 25

ERROR_INVALID_ARGUMENT = 3

ERROR_NOT_BTRFS = 4
ERROR_NOT_SUBVOLUME = 5

ERROR_NO_MEMORY = 2

ERROR_OPEN_FAILED = 7

ERROR_RMDIR_FAILED = 8

ERROR_SEARCH_FAILED = 12

ERROR_SNAP_CREATE_FAILED = 17

ERROR_SNAP_DESTROY_FAILED = 18

ERROR_START_SYNC_FAILED = 21

ERROR_STATFS_FAILED = 11

ERROR_STAT_FAILED = 10

ERROR_STOP_ITERATION = 1

ERROR_SUBVOLUME_NOT_FOUND = 6

ERROR_SUBVOL_CREATE_FAILED = 16

ERROR_SUBVOL_GETFLAGS_FAILED = 14

ERROR_SUBVOL_SETFLAGS_FAILED = 15

ERROR_SYNC_FAILED = 20

ERROR_UNLINK_FAILED = 9

ERROR_WAIT_SYNC_FAILED = 22

# functions

def create_snapshot(source, path, recursive=False, read_only=False, async_=False, qgroup_inherit=None): # real signature unknown; restored from __doc__
    """
    create_snapshot(source, path, recursive=False, read_only=False,
                    async_=False, qgroup_inherit=None)
    
    Create a new snapshot.
    
    Arguments:
    source -- string, bytes, path-like object, or open file descriptor
    path -- string, bytes, or path-like object
    recursive -- also snapshot child subvolumes
    read_only -- create a read-only snapshot
    async_ -- create the subvolume without waiting for it to commit to
    disk and return the transaction ID
    qgroup_inherit -- optional QgroupInherit object of qgroups to
    inherit from
    """
    pass

def create_subvolume(path, async_=False, qgroup_inherit=None): # real signature unknown; restored from __doc__
    """
    create_subvolume(path, async_=False, qgroup_inherit=None)
    
    Create a new subvolume.
    
    Arguments:
    path -- string, bytes, or path-like object
    async_ -- create the subvolume without waiting for it to commit to
    disk and return the transaction ID
    qgroup_inherit -- optional QgroupInherit object of qgroups to
    inherit from
    """
    pass

def deleted_subvolumes(path): # real signature unknown; restored from __doc__
    """
    deleted_subvolumes(path)
    
    Get the list of subvolume IDs which have been deleted but not yet
    cleaned up
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    pass

def delete_subvolume(path, recursive=False): # real signature unknown; restored from __doc__
    """
    delete_subvolume(path, recursive=False)
    
    Delete a subvolume or snapshot.
    
    Arguments:
    path -- string, bytes, or path-like object
    recursive -- if the given subvolume has child subvolumes, delete
    them instead of failing
    """
    pass

def get_default_subvolume(path): # real signature unknown; restored from __doc__
    """
    get_default_subvolume(path) -> int
    
    Get the ID of the default subvolume of a filesystem.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    return 0

def get_subvolume_read_only(path): # real signature unknown; restored from __doc__
    """
    get_subvolume_read_only(path) -> bool
    
    Get whether a subvolume is read-only.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    return False

def is_subvolume(path): # real signature unknown; restored from __doc__
    """
    is_subvolume(path) -> bool
    
    Get whether a file is a subvolume.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    return False

def set_default_subvolume(path, id=0): # real signature unknown; restored from __doc__
    """
    set_default_subvolume(path, id=0)
    
    Set the default subvolume of a filesystem.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    id -- if not zero, set the default subvolume to the subvolume with
    this ID instead of the given path
    """
    pass

def set_subvolume_read_only(path, read_only=True): # real signature unknown; restored from __doc__
    """
    set_subvolume_read_only(path, read_only=True)
    
    Set whether a subvolume is read-only.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    read_only -- bool flag value
    """
    pass

def start_sync(path): # real signature unknown; restored from __doc__
    """
    start_sync(path) -> int
    
    Start a sync on a specific Btrfs filesystem and return the
    transaction ID.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    return 0

def subvolume_id(path): # real signature unknown; restored from __doc__
    """
    subvolume_id(path) -> int
    
    Get the ID of the subvolume containing a file.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    return 0

def subvolume_info(path, id=0): # real signature unknown; restored from __doc__
    """
    subvolume_info(path, id=0) -> SubvolumeInfo
    
    Get information about a subvolume.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    id -- if not zero, instead of returning information about the
    given path, return information about the subvolume with this ID
    """
    return SubvolumeInfo

def subvolume_path(path, id=0): # real signature unknown; restored from __doc__
    """
    subvolume_path(path, id=0) -> int
    
    Get the path of a subvolume relative to the filesystem root.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    id -- if not zero, instead of returning the subvolume path of the
    given path, return the path of the subvolume with this ID
    """
    return 0

def sync(path): # real signature unknown; restored from __doc__
    """
    sync(path)
    
    Sync a specific Btrfs filesystem.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    """
    pass

def wait_sync(path, transid=0): # real signature unknown; restored from __doc__
    """
    wait_sync(path, transid=0)
    
    Wait for a transaction to sync.
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor
    transid -- int transaction ID to wait for, or zero for the current
    transaction
    """
    pass

# classes

class BtrfsUtilError(OSError):
    """ Btrfs operation error. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __str__(self, *args, **kwargs): # real signature unknown
        """ Return str(self). """
        pass

    btrfsutilerror = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """btrfsutil error code"""



class QgroupInherit(object):
    """
    QgroupInherit() -> new qgroup inheritance specifier
    
    Create a new object which specifies what qgroups to inherit
    from for create_subvolume() and create_snapshot()
    """
    def add_group(self, qgroupid): # real signature unknown; restored from __doc__
        """
        add_group(qgroupid)
        
        Add a qgroup to inherit from.
        
        Arguments:
        qgroupid -- ID of qgroup to add
        """
        pass

    def __getattribute__(self, *args, **kwargs): # real signature unknown
        """ Return getattr(self, name). """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


class SubvolumeInfo(tuple):
    """ Information about a Btrfs subvolume. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    ctime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """float time when an inode was last changed"""

    ctransid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int transaction ID when an inode was last changed"""

    dir_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int inode number of the directory containing this subvolume"""

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int root item flags"""

    generation = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int transaction ID of the subvolume root"""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int ID of this subvolume"""

    otime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """float time when this subvolume was created"""

    otransid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int transaction ID when this subvolume was created"""

    parent_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int ID of the subvolume containing this subvolume"""

    parent_uuid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """bytes UUID of the subvolume this is a snapshot of"""

    received_uuid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """bytes UUID of the subvolume this was received from"""

    rtime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """float time when this subvolume was received"""

    rtransid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int transaction ID when this subvolume was received"""

    stime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """float time, usually zero"""

    stransid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """int transaction ID of the sent subvolume this subvolume was received from"""

    uuid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """bytes UUID of this subvolume"""


    n_fields = 16
    n_sequence_fields = 14
    n_unnamed_fields = 0


class SubvolumeIterator(object):
    """
    SubvolumeIterator(path, top=0, info=False, post_order=False) -> new subvolume iterator
    
    Create a new iterator that produces tuples of (path, ID) representing
    subvolumes on a filesystem.
    
    Arguments:
    path -- string, bytes, path-like object, or open file descriptor in
    filesystem to list
    top -- if not zero, instead of only listing subvolumes beneath the
    given path, list subvolumes beneath the subvolume with this ID; passing
    BTRFS_FS_TREE_OBJECTID (5) here lists all subvolumes. The subvolumes
    are listed relative to the subvolume with this ID.
    info -- bool indicating the iterator should yield SubvolumeInfo instead of
    the subvolume ID
    post_order -- bool indicating whether to yield parent subvolumes before
    child subvolumes (e.g., 'foo/bar' before 'foo')
    """
    def close(self): # real signature unknown; restored from __doc__
        """
        close()
        
        Close this iterator.
        """
        pass

    def fileno(self): # real signature unknown; restored from __doc__
        """
        fileno() -> int
        
        Get the file descriptor associated with this iterator.
        """
        return 0

    def __enter__(self, *args, **kwargs): # real signature unknown
        pass

    def __exit__(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, path, top=0, info=False, post_order=False): # real signature unknown; restored from __doc__
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __next__(self, *args, **kwargs): # real signature unknown
        """ Implement next(self). """
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='btrfsutil', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/btrfsutil.cpython-38-x86_64-linux-gnu.so')"

