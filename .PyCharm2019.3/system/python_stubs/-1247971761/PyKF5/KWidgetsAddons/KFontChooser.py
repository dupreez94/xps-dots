# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KFontChooser(__PyQt5_QtWidgets.QWidget):
    # no doc
    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def backgroundColor(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def color(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enableColumn(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def font(self, *args, **kwargs): # real signature unknown
        pass

    def fontDiffFlags(self, *args, **kwargs): # real signature unknown
        pass

    def fontSelected(self, *args, **kwargs): # real signature unknown
        pass

    def getFontList(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def sampleText(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setBackgroundColor(self, *args, **kwargs): # real signature unknown
        pass

    def setColor(self, *args, **kwargs): # real signature unknown
        pass

    def setFont(self, *args, **kwargs): # real signature unknown
        pass

    def setSampleBoxVisible(self, *args, **kwargs): # real signature unknown
        pass

    def setSampleText(self, *args, **kwargs): # real signature unknown
        pass

    def setSizeIsRelative(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def sizeHint(self, *args, **kwargs): # real signature unknown
        pass

    def sizeIsRelative(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    AllFontDiffs = 7
    DisplayFlag = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.DisplayFlag'>"
    DisplayFlags = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.DisplayFlags'>"
    DisplayFrame = 2
    FamilyList = 1
    FixedFontsOnly = 1
    FixedWidthFonts = 1
    FontColumn = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.FontColumn'>"
    FontDiff = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.FontDiff'>"
    FontDiffFamily = 1
    FontDiffFlags = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.FontDiffFlags'>"
    FontDiffSize = 4
    FontDiffStyle = 2
    FontListCriteria = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KFontChooser.FontListCriteria'>"
    NoDisplayFlags = 0
    NoFontDiffFlags = 0
    ScalableFonts = 2
    ShowDifferences = 4
    SizeList = 4
    SmoothScalableFonts = 4
    StyleList = 2


