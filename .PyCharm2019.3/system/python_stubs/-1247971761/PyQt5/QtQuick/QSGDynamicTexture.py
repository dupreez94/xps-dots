# encoding: utf-8
# module PyQt5.QtQuick
# from /usr/lib/python3.8/site-packages/PyQt5/QtQuick.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml
import sip as __sip


from .QSGTexture import QSGTexture

class QSGDynamicTexture(QSGTexture):
    """ QSGDynamicTexture() """
    def updateTexture(self): # real signature unknown; restored from __doc__
        """ updateTexture(self) -> bool """
        return False

    def __init__(self): # real signature unknown; restored from __doc__
        pass


