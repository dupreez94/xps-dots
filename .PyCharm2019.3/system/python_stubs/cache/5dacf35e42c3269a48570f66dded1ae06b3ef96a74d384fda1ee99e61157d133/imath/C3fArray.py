# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class C3fArray(__Boost_Python.instance):
    """ Fixed length array of Imath::Color3 """
    def ifelse(self, C3fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (C3fArray)arg1, (IntArray)arg2, (Color3f)arg3) -> C3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color3<float> > ifelse(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Color3<float>)
        
        ifelse( (C3fArray)arg1, (IntArray)arg2, (C3fArray)arg3) -> C3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color3<float> > ifelse(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Color3<float> >)
        """
        pass

    def __getitem__(self, C3fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (C3fArray)arg1, (object)arg2) -> C3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color3<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},_object*)
        
        __getitem__( (C3fArray)arg1, (IntArray)arg2) -> C3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color3<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (C3fArray)arg1, (int)arg2) -> Color3f :
        
            C++ signature :
                Imath_2_4::Color3<float> __getitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},long)
        
        __getitem__( (C3fArray)arg1, (int)arg2) -> Color3f :
        
            C++ signature :
                Imath_2_4::Color3<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (C3fArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Color3<float> >)
        
        __init__( (object)arg1, (Color3f)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color3<float>,unsigned long)
        
        __init__( (object)arg1, (V3fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        
        __init__( (object)arg1, (V3dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __len__(self, C3fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (C3fArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, C3fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (C3fArray)arg1, (object)arg2, (Color3f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},_object*,Imath_2_4::Color3<float>)
        
        __setitem__( (C3fArray)arg1, (IntArray)arg2, (Color3f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Color3<float>)
        
        __setitem__( (C3fArray)arg1, (object)arg2, (C3fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Color3<float> >)
        
        __setitem__( (C3fArray)arg1, (IntArray)arg2, (C3fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color3<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Color3<float> >)
        """
        pass

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


