# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class listen_failed_alert_socket_type_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    i2p = 3
    names = {
        'i2p': 3,
        'socks5': 4,
        'tcp': 0,
        'tcp_ssl': 1,
        'udp': 2,
        'utp_ssl': 5,
    }
    socks5 = 4
    tcp = 0
    tcp_ssl = 1
    udp = 2
    utp_ssl = 5
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
    }
    __slots__ = ()


