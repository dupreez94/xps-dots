# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KAboutData(__sip.wrapper):
    # no doc
    def aboutTranslationTeam(self, *args, **kwargs): # real signature unknown
        pass

    def addAuthor(self, *args, **kwargs): # real signature unknown
        pass

    def addCredit(self, *args, **kwargs): # real signature unknown
        pass

    def addLicense(self, *args, **kwargs): # real signature unknown
        pass

    def addLicenseText(self, *args, **kwargs): # real signature unknown
        pass

    def addLicenseTextFile(self, *args, **kwargs): # real signature unknown
        pass

    def applicationData(self, *args, **kwargs): # real signature unknown
        pass

    def authors(self, *args, **kwargs): # real signature unknown
        pass

    def bugAddress(self, *args, **kwargs): # real signature unknown
        pass

    def componentName(self, *args, **kwargs): # real signature unknown
        pass

    def copyrightStatement(self, *args, **kwargs): # real signature unknown
        pass

    def credits(self, *args, **kwargs): # real signature unknown
        pass

    def customAuthorPlainText(self, *args, **kwargs): # real signature unknown
        pass

    def customAuthorRichText(self, *args, **kwargs): # real signature unknown
        pass

    def customAuthorTextEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def desktopFileName(self, *args, **kwargs): # real signature unknown
        pass

    def displayName(self, *args, **kwargs): # real signature unknown
        pass

    def fromPluginMetaData(self, *args, **kwargs): # real signature unknown
        pass

    def homepage(self, *args, **kwargs): # real signature unknown
        pass

    def internalBugAddress(self, *args, **kwargs): # real signature unknown
        pass

    def internalProgramName(self, *args, **kwargs): # real signature unknown
        pass

    def internalVersion(self, *args, **kwargs): # real signature unknown
        pass

    def licenses(self, *args, **kwargs): # real signature unknown
        pass

    def ocsProviderUrl(self, *args, **kwargs): # real signature unknown
        pass

    def organizationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def otherText(self, *args, **kwargs): # real signature unknown
        pass

    def pluginData(self, *args, **kwargs): # real signature unknown
        pass

    def processCommandLine(self, *args, **kwargs): # real signature unknown
        pass

    def productName(self, *args, **kwargs): # real signature unknown
        pass

    def programIconName(self, *args, **kwargs): # real signature unknown
        pass

    def programLogo(self, *args, **kwargs): # real signature unknown
        pass

    def registerPluginData(self, *args, **kwargs): # real signature unknown
        pass

    def setApplicationData(self, *args, **kwargs): # real signature unknown
        pass

    def setBugAddress(self, *args, **kwargs): # real signature unknown
        pass

    def setComponentName(self, *args, **kwargs): # real signature unknown
        pass

    def setCopyrightStatement(self, *args, **kwargs): # real signature unknown
        pass

    def setCustomAuthorText(self, *args, **kwargs): # real signature unknown
        pass

    def setDesktopFileName(self, *args, **kwargs): # real signature unknown
        pass

    def setDisplayName(self, *args, **kwargs): # real signature unknown
        pass

    def setHomepage(self, *args, **kwargs): # real signature unknown
        pass

    def setLicense(self, *args, **kwargs): # real signature unknown
        pass

    def setLicenseText(self, *args, **kwargs): # real signature unknown
        pass

    def setLicenseTextFile(self, *args, **kwargs): # real signature unknown
        pass

    def setOcsProvider(self, *args, **kwargs): # real signature unknown
        pass

    def setOrganizationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def setOtherText(self, *args, **kwargs): # real signature unknown
        pass

    def setProductName(self, *args, **kwargs): # real signature unknown
        pass

    def setProgramIconName(self, *args, **kwargs): # real signature unknown
        pass

    def setProgramLogo(self, *args, **kwargs): # real signature unknown
        pass

    def setShortDescription(self, *args, **kwargs): # real signature unknown
        pass

    def setTranslator(self, *args, **kwargs): # real signature unknown
        pass

    def setupCommandLine(self, *args, **kwargs): # real signature unknown
        pass

    def setVersion(self, *args, **kwargs): # real signature unknown
        pass

    def shortDescription(self, *args, **kwargs): # real signature unknown
        pass

    def translators(self, *args, **kwargs): # real signature unknown
        pass

    def unsetCustomAuthorText(self, *args, **kwargs): # real signature unknown
        pass

    def version(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



