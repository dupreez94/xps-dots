# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2fArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec2 """
    def bounds(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bounds( (V2fArray)arg1) -> Box2f :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<float> > bounds(PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def cross(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2fArray)arg1, (V2f)x) -> FloatArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> cross(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        cross( (V2fArray)arg1, (V2fArray)x) -> FloatArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> cross(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def dot(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2fArray)arg1, (V2f)x) -> FloatArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> dot(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        dot( (V2fArray)arg1, (V2fArray)x) -> FloatArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> dot(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def ifelse(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V2fArray)arg1, (IntArray)arg2, (V2f)arg3) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<float>)
        
        ifelse( (V2fArray)arg1, (IntArray)arg2, (V2fArray)arg3) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def length(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2fArray)arg1) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> length(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def length2(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2fArray)arg1) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> length2(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def max(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V2fArray)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> max(PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def min(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V2fArray)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> min(PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def normalize(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2fArray)arg1) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def normalized(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2fArray)arg1) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > normalized(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def reduce(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V2fArray)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> reduce(PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __add__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __add__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __copy__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2fArray)arg1) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __copy__(PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __deepcopy__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2fArray)arg1, (dict)arg2) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec2<float> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __div__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __div__( (V2fArray)arg1, (float)x) -> V2fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __div__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __eq__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2fArray)arg1, (V2f)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __eq__( (V2fArray)arg1, (V2fArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __getitem__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2fArray)arg1, (object)arg2) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},_object*)
        
        __getitem__( (V2fArray)arg1, (IntArray)arg2) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V2fArray)arg1, (int)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},long)
        
        __getitem__( (V2fArray)arg1, (int)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},long)
        """
        pass

    def __iadd__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __iadd__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __idiv__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __idiv__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __idiv__( (V2fArray)arg1, (float)x) -> V2fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __idiv__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __imul__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __imul__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __imul__( (V2fArray)arg1, (float)x) -> V2fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __imul__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V2fArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __init__( (object)arg1, (V2f)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<float>,unsigned long)
        
        __init__( (object)arg1, (V2iArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __init__( (object)arg1, (V2dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __isub__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __isub__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __itruediv__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __itruediv__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __itruediv__( (V2fArray)arg1, (float)x) -> V2fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __itruediv__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __len__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2fArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def __mul__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __mul__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __mul__( (V2fArray)arg1, (float)x) -> V2fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __mul__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __neg__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2fArray)arg1) -> V2fArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __neg__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def __ne__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2fArray)arg1, (V2f)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __ne__( (V2fArray)arg1, (V2fArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __radd__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __radd__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __rmul__( (V2fArray)arg1, (float)x) -> V2fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __rmul__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __rsub__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        """
        pass

    def __setitem__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2fArray)arg1, (object)arg2, (V2f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},_object*,Imath_2_4::Vec2<float>)
        
        __setitem__( (V2fArray)arg1, (IntArray)arg2, (V2f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<float>)
        
        __setitem__( (V2fArray)arg1, (object)arg2, (V2fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __setitem__( (V2fArray)arg1, (IntArray)arg2, (V2fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __setitem__( (V2fArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},long,boost::python::tuple)
        
        __setitem__( (V2fArray)arg1, (int)arg2, (list)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},long,boost::python::list)
        """
        pass

    def __sub__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __sub__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __truediv__(self, V2fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2fArray)arg1, (V2f)x) -> V2fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        __truediv__( (V2fArray)arg1, (V2fArray)x) -> V2fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __truediv__( (V2fArray)arg1, (float)x) -> V2fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},float)
        
        __truediv__( (V2fArray)arg1, (FloatArray)x) -> V2fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


