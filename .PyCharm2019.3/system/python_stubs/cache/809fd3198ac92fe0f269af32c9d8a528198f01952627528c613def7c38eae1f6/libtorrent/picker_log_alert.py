# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .peer_alert import peer_alert

class picker_log_alert(peer_alert):
    # no doc
    def blocks(self, picker_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        blocks( (picker_log_alert)arg1) -> object :
        
            C++ signature :
                std::vector<libtorrent::piece_block, std::allocator<libtorrent::piece_block> > blocks(libtorrent::picker_log_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    picker_flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



