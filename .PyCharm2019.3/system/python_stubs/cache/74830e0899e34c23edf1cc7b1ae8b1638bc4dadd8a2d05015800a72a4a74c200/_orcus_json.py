# encoding: utf-8
# module _orcus_json
# from /usr/lib/python3.8/site-packages/_orcus_json.so
# by generator 1.147
# no doc
# no imports

# functions

def loads(*args, **kwargs): # real signature unknown
    """ Load JSON string into a Python object. """
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>'

__spec__ = None # (!) real value is "ModuleSpec(name='_orcus_json', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>, origin='/usr/lib/python3.8/site-packages/_orcus_json.so')"

