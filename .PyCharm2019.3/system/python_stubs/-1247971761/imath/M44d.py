# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M44d(__Boost_Python.instance):
    """ M44d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def determinant(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        determinant( (M44d)arg1) -> float :
            determinant() return the determinant of this matrix
        
            C++ signature :
                double determinant(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def equalWithAbsError(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (M44d)arg1, (M44d)arg2, (float)arg3) -> bool :
            m1.equalWithAbsError(m2,e) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(m1[i] - m2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>,double)
        """
        pass

    def equalWithRelError(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (M44d)arg1, (M44d)arg2, (float)arg3) -> bool :
            m1.equalWithAbsError(m2,e) true if the elements of m1 and m2 are the same with an absolute error of no more than e, i.e., abs(m1[i] - m2[i]) <= e * abs(m1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>,double)
        """
        pass

    def extractAndRemoveScalingAndShear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractAndRemoveScalingAndShear( (M44d)arg1, (V3d)arg2, (V3d)arg3 [, (int)arg4]) -> None :
            M.extractAndRemoveScalingAndShear(scl, shr, [exc]) -- extracts the scaling component of M into scl and the shearing component of M into shr.  Also removes the scaling and shearing components from M.  Returns 1 unless the scaling component is nearly 0, in which case 0 is returned. If optional arg. exc == 1, then if the scaling component is nearly 0, then MathExc is thrown.
        
            C++ signature :
                void extractAndRemoveScalingAndShear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue} [,int])
        """
        pass

    def extractEulerXYZ(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractEulerXYZ( (M44d)arg1, (V3d)arg2) -> None :
            extract Euler
        
            C++ signature :
                void extractEulerXYZ(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def extractEulerZYX(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractEulerZYX( (M44d)arg1, (V3d)arg2) -> None :
            extract Euler
        
            C++ signature :
                void extractEulerZYX(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def extractScaling(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractScaling( (M44d)arg1, (V3d)arg2 [, (int)arg3]) -> None :
            extract scaling
        
            C++ signature :
                void extractScaling(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue} [,int])
        """
        pass

    def extractScalingAndShear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractScalingAndShear( (M44d)arg1, (V3d)arg2, (V3d)arg3 [, (int)arg4]) -> None :
            extract scaling
        
            C++ signature :
                void extractScalingAndShear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue} [,int])
        """
        pass

    def extractSHRT(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractSHRT( (M44d)arg1, (V3d)arg2, (V3d)arg3, (V3d)arg4, (V3d)arg5 [, (int)arg6]) -> int :
            M.extractSHRT(Vs, Vh, Vr, Vt, [exc]) -- extracts the scaling component of M into Vs, the shearing component of M in Vh (as XY, XZ, YZ shear factors), the rotation of M into Vr (as Euler angles in the order XYZ), and the translaation of M into Vt. If optional arg. exc == 1, then if the scaling component is nearly 0, then MathExc is thrown. 
        
            C++ signature :
                int extractSHRT(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue} [,int])
        """
        pass

    def fastMinor(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fastMinor( (M44d)arg1, (int)arg2, (int)arg3, (int)arg4, (int)arg5, (int)arg6, (int)arg7) -> float :
            fastMinor() return matrix minor using the specified rows and columns of this matrix
        
            C++ signature :
                double fastMinor(Imath_2_4::Matrix44<double> {lvalue},int,int,int,int,int,int)
        """
        pass

    def gjInverse(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        gjInverse( (M44d)arg1 [, (bool)arg2]) -> M44d :
            gjInverse() return a inverted copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> gjInverse(Imath_2_4::Matrix44<double> {lvalue} [,bool])
        """
        pass

    def gjInvert(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        gjInvert( (M44d)arg1 [, (bool)arg2]) -> M44d :
            gjInvert() invert this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> gjInvert(Imath_2_4::Matrix44<double> {lvalue} [,bool])
        """
        pass

    def inverse(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        inverse( (M44d)arg1 [, (bool)arg2]) -> M44d :
            inverse() return a inverted copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> inverse(Imath_2_4::Matrix44<double> {lvalue} [,bool])
        """
        pass

    def invert(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        invert( (M44d)arg1 [, (bool)arg2]) -> M44d :
            invert() invert this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> invert(Imath_2_4::Matrix44<double> {lvalue} [,bool])
        """
        pass

    def makeIdentity(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeIdentity( (M44d)arg1) -> None :
            makeIdentity() make this matrix the identity matrix
        
            C++ signature :
                void makeIdentity(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def minorOf(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        minorOf( (M44d)arg1, (int)arg2, (int)arg3) -> float :
            minorOf() return matrix minor of the (row,col) element of this matrix
        
            C++ signature :
                double minorOf(Imath_2_4::Matrix44<double> {lvalue},int,int)
        """
        pass

    def multDirMatrix(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        multDirMatrix( (M44d)arg1, (V3d)arg2, (V3d)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double> {lvalue})
        
        multDirMatrix( (M44d)arg1, (V3d)arg2) -> V3d :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec3<double> multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        multDirMatrix( (M44d)arg1, (V3dArray)arg2) -> V3dArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        multDirMatrix( (M44d)arg1, (V3f)arg2, (V3f)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float> {lvalue})
        
        multDirMatrix( (M44d)arg1, (V3f)arg2) -> V3f :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec3<float> multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<float>)
        
        multDirMatrix( (M44d)arg1, (V3fArray)arg2) -> V3fArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > multDirMatrix(Imath_2_4::Matrix44<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def multVecMatrix(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        multVecMatrix( (M44d)arg1, (V3d)arg2, (V3d)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double> {lvalue})
        
        multVecMatrix( (M44d)arg1, (V3d)arg2) -> V3d :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec3<double> multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        multVecMatrix( (M44d)arg1, (V3dArray)arg2) -> V3dArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        multVecMatrix( (M44d)arg1, (V3f)arg2, (V3f)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float> {lvalue})
        
        multVecMatrix( (M44d)arg1, (V3f)arg2) -> V3f :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec3<float> multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<float>)
        
        multVecMatrix( (M44d)arg1, (V3fArray)arg2) -> V3fArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > multVecMatrix(Imath_2_4::Matrix44<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def negate(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (M44d)arg1) -> M44d :
            negate() negate all entries in this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> negate(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def removeScaling(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        removeScaling( (M44d)arg1 [, (int)arg2]) -> int :
            remove scaling
        
            C++ signature :
                int removeScaling(Imath_2_4::Matrix44<double> {lvalue} [,int])
        """
        pass

    def removeScalingAndShear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        removeScalingAndShear( (M44d)arg1 [, (int)arg2]) -> int :
            remove scaling
        
            C++ signature :
                int removeScalingAndShear(Imath_2_4::Matrix44<double> {lvalue} [,int])
        """
        pass

    def rotate(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rotate( (M44d)arg1, (V3d)arg2) -> M44d :
            rotate matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> rotate(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def rotationMatrix(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rotationMatrix( (M44d)arg1, (object)arg2, (object)arg3) -> M44d :
            rotationMatrix()
        
            C++ signature :
                Imath_2_4::Matrix44<double> rotationMatrix(Imath_2_4::Matrix44<double> {lvalue},boost::python::api::object,boost::python::api::object)
        """
        pass

    def rotationMatrixWithUpDir(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rotationMatrixWithUpDir( (M44d)arg1, (object)arg2, (object)arg3, (object)arg4) -> M44d :
            roationMatrixWithUp()
        
            C++ signature :
                Imath_2_4::Matrix44<double> rotationMatrixWithUpDir(Imath_2_4::Matrix44<double> {lvalue},boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def sansScaling(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        sansScaling( (M44d)arg1 [, (bool)arg2]) -> M44d :
            sans scaling
        
            C++ signature :
                Imath_2_4::Matrix44<double> sansScaling(Imath_2_4::Matrix44<double> [,bool])
        """
        pass

    def sansScalingAndShear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        sansScalingAndShear( (M44d)arg1 [, (bool)arg2]) -> M44d :
            sans scaling and shear
        
            C++ signature :
                Imath_2_4::Matrix44<double> sansScalingAndShear(Imath_2_4::Matrix44<double> [,bool])
        """
        pass

    def scale(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        scale( (M44d)arg1, (float)arg2) -> M44d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> scale(Imath_2_4::Matrix44<double> {lvalue},double)
        
        scale( (M44d)arg1, (V3d)arg2) -> M44d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> scale(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        scale( (M44d)arg1, (tuple)arg2) -> M44d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> scale(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        """
        pass

    def setScale(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setScale( (M44d)arg1, (float)arg2) -> M44d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setScale(Imath_2_4::Matrix44<double> {lvalue},double)
        
        setScale( (M44d)arg1, (V3d)arg2) -> M44d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setScale(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        setScale( (M44d)arg1, (tuple)arg2) -> M44d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setScale(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        """
        pass

    def setShear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setShear( (M44d)arg1, (V3d)arg2) -> M44d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setShear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        setShear( (M44d)arg1, (Shear6d)arg2) -> M44d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setShear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Shear6<double>)
        
        setShear( (M44d)arg1, (tuple)arg2) -> M44d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setShear(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        """
        pass

    def setTranslation(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setTranslation( (M44d)arg1, (V3d)arg2) -> M44d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setTranslation(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        setTranslation( (M44d)arg1, (tuple)arg2) -> M44d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setTranslation(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        
        setTranslation( (M44d)arg1, (object)arg2) -> M44d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix44<double> setTranslation(Imath_2_4::Matrix44<double> {lvalue},boost::python::api::object)
        """
        pass

    def setValue(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (M44d)arg1, (M44d)arg2) -> None :
            setValue()
        
            C++ signature :
                void setValue(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def shear(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        shear( (M44d)arg1, (V3d)arg2) -> M44d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> shear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Vec3<double>)
        
        shear( (M44d)arg1, (Shear6d)arg2) -> M44d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> shear(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Shear6<double>)
        
        shear( (M44d)arg1, (tuple)arg2) -> M44d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix44<double> shear(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        """
        pass

    def singularValueDecomposition(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        singularValueDecomposition( (M44d)matrix, (bool)forcePositiveDeterminant) -> tuple :
            Decomposes the matrix using the singular value decomposition (SVD) into three
            matrices U, S, and V which have the following properties: 
              1. U and V are both orthonormal matrices, 
              2. S is the diagonal matrix of singular values, 
              3. U * S * V.transposed() gives back the original matrix.
            The result is returned as a tuple [U, S, V].  Note that since S is diagonal we
            don't need to return the entire matrix, so we return it as a three-vector.  
            
            The 'forcePositiveDeterminant' argument can be used to force the U and V^T to
            have positive determinant (that is, to be proper rotation matrices); if
            forcePositiveDeterminant is False, then the singular values are guaranteed to
            be nonnegative but the U and V matrices might contain negative scale along one
            of the axes; if forcePositiveDeterminant is True, then U and V cannot contain
            negative scale but S[3] might be negative.  
            
            Our SVD implementation uses two-sided Jacobi rotations to iteratively
            diagonalize the matrix, which should be quite robust and significantly faster
            than the more general SVD solver in LAPACK.  
            
        
            C++ signature :
                boost::python::tuple singularValueDecomposition(Imath_2_4::Matrix44<double>,bool)
        """
        pass

    def symmetricEigensolve(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        symmetricEigensolve( (M44d)arg1) -> tuple :
            Decomposes the matrix A using a symmetric eigensolver into matrices Q and S 
            which have the following properties: 
              1. Q is the orthonormal matrix of eigenvectors, 
              2. S is the diagonal matrix of eigenvalues, 
              3. Q.transposed() * S * Q gives back the original matrix.
            
            IMPORTANT: It is vital that the passed-in matrix be symmetric, or the result 
            won't make any sense.  This function will return an error if passed an 
            unsymmetric matrix.
            
            The result is returned as a tuple [Q, S].  Note that since S is diagonal 
            we don't need to return the entire matrix, so we return it as a three-vector. 
            
            Our eigensolver implementation uses one-sided Jacobi rotations to iteratively 
            diagonalize the matrix, which should be quite robust and significantly faster 
            than the more general symmetric eigenvalue solver in LAPACK.  
            
        
            C++ signature :
                boost::python::tuple symmetricEigensolve(Imath_2_4::Matrix44<double>)
        """
        pass

    def translate(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        translate( (M44d)arg1, (object)arg2) -> M44d :
            translate()
        
            C++ signature :
                Imath_2_4::Matrix44<double> translate(Imath_2_4::Matrix44<double> {lvalue},boost::python::api::object)
        
        translate( (M44d)arg1, (tuple)arg2) -> M44d :
            translate()
        
            C++ signature :
                Imath_2_4::Matrix44<double> translate(Imath_2_4::Matrix44<double> {lvalue},boost::python::tuple)
        """
        pass

    def translation(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        translation( (M44d)arg1) -> V3d :
            translation()
        
            C++ signature :
                Imath_2_4::Vec3<double> translation(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def transpose(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        transpose( (M44d)arg1) -> M44d :
            transpose() transpose this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> transpose(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def transposed(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        transposed( (M44d)arg1) -> M44d :
            transposed() return a transposed copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix44<double> transposed(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def __add__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __add__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __add__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __add__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __copy__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (M44d)arg1) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __copy__(Imath_2_4::Matrix44<double>)
        """
        pass

    def __deepcopy__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (M44d)arg1, (dict)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __deepcopy__(Imath_2_4::Matrix44<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __div__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __eq__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (M44d)arg1, (M44d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __getitem__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M44d)arg1, (int)arg2) -> M44dRow :
        
            C++ signature :
                PyImath::MatrixRow<double, 4> __getitem__(Imath_2_4::Matrix44<double> {lvalue},long)
        """
        pass

    def __ge__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (M44d)arg1, (M44d)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __gt__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (M44d)arg1, (M44d)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __iadd__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (M44d)arg1, (M44f)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __iadd__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __iadd__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __iadd__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __iadd__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __iadd__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __idiv__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __idiv__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __imul__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __imul__(Imath_2_4::Matrix44<double> {lvalue},double)
        
        __imul__( (M44d)arg1, (M44f)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __imul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<float> {lvalue})
        
        __imul__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __imul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (M44d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Matrix44<double>)
        
        __init__( (object)arg1) -> None :
            initialize to identity
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (float)arg2) -> None :
            initialize all entries to a single value
        
            C++ signature :
                void __init__(_object*,double)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4, (tuple)arg5) -> object :
            tuple constructor1
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (M44f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<float>)
        
        __init__( (object)arg1, (M44d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<double>)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (float)arg8, (float)arg9, (float)arg10, (float)arg11, (float)arg12, (float)arg13, (float)arg14, (float)arg15, (float)arg16, (float)arg17) -> None :
            make from components
        
            C++ signature :
                void __init__(_object*,double,double,double,double,double,double,double,double,double,double,double,double,double,double,double,double)
        """
        pass

    def __isub__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (M44d)arg1, (M44f)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __isub__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __isub__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __isub__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __isub__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __isub__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __itruediv__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __itruediv__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __len__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M44d)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Matrix44<double>)
        """
        pass

    def __le__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (M44d)arg1, (M44d)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __lt__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (M44d)arg1, (M44d)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __mul__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __mul__(Imath_2_4::Matrix44<double> {lvalue},double)
        
        __mul__( (M44d)arg1, (M44f)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __mul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<float> {lvalue})
        
        __mul__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __mul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def __neg__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (M44d)arg1) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __neg__(Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def __ne__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (M44d)arg1, (M44d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __radd__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __radd__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (M44d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Matrix44<double>)
        """
        pass

    def __rmul__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __rmul__(Imath_2_4::Matrix44<double> {lvalue},double)
        
        __rmul__( (M44d)arg1, (M44f)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __rmul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<float> {lvalue})
        
        __rmul__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __rmul__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double> {lvalue})
        """
        pass

    def __rsub__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __rsub__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __sub__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (M44d)arg1, (M44d)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __sub__(Imath_2_4::Matrix44<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __sub__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __sub__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    def __truediv__(self, M44d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (M44d)arg1, (float)arg2) -> M44d :
        
            C++ signature :
                Imath_2_4::Matrix44<double> __truediv__(Imath_2_4::Matrix44<double> {lvalue},double)
        """
        pass

    __instance_size__ = 144


