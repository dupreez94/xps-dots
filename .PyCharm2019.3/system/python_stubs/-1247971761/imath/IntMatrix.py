# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class IntMatrix(__Boost_Python.instance):
    """ Fixed size matrix of ints """
    def columns(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        columns( (IntMatrix)arg1) -> int :
        
            C++ signature :
                int columns(PyImath::FixedMatrix<int> {lvalue})
        """
        pass

    def rows(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rows( (IntMatrix)arg1) -> int :
        
            C++ signature :
                int rows(PyImath::FixedMatrix<int> {lvalue})
        """
        pass

    def __add__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __add__(PyImath::FixedMatrix<int>,PyImath::FixedMatrix<int>)
        
        __add__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __add__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __div__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __div__(PyImath::FixedMatrix<int>,PyImath::FixedMatrix<int>)
        
        __div__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __div__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __getitem__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (IntMatrix)arg1, (object)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __getitem__(PyImath::FixedMatrix<int> {lvalue},_object*)
        
        __getitem__( (IntMatrix)arg1, (int)arg2) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> const* __getitem__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __iadd__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __iadd__(PyImath::FixedMatrix<int> {lvalue},PyImath::FixedMatrix<int>)
        
        __iadd__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __iadd__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __idiv__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __idiv__(PyImath::FixedMatrix<int> {lvalue},PyImath::FixedMatrix<int>)
        
        __idiv__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __idiv__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __imul__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __imul__(PyImath::FixedMatrix<int> {lvalue},PyImath::FixedMatrix<int>)
        
        __imul__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __imul__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            return an unitialized array of the specified rows and cols
        
            C++ signature :
                void __init__(_object*,int,int)
        """
        pass

    def __isub__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __isub__(PyImath::FixedMatrix<int> {lvalue},PyImath::FixedMatrix<int>)
        
        __isub__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __isub__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __itruediv__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __itruediv__(PyImath::FixedMatrix<int> {lvalue},PyImath::FixedMatrix<int>)
        
        __itruediv__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> {lvalue} __itruediv__(PyImath::FixedMatrix<int> {lvalue},int)
        """
        pass

    def __len__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (IntMatrix)arg1) -> int :
        
            C++ signature :
                int __len__(PyImath::FixedMatrix<int> {lvalue})
        """
        pass

    def __mul__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __mul__(PyImath::FixedMatrix<int>,PyImath::FixedMatrix<int>)
        
        __mul__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __mul__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __neg__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (IntMatrix)arg1) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __neg__(PyImath::FixedMatrix<int>)
        """
        pass

    def __radd__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __radd__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __rmul__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __rsub__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __rsub__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __setitem__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (IntMatrix)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<int> {lvalue},_object*,int)
        
        __setitem__( (IntMatrix)arg1, (object)arg2, (IntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<int> {lvalue},_object*,PyImath::FixedArray<int>)
        
        __setitem__( (IntMatrix)arg1, (object)arg2, (IntMatrix)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<int> {lvalue},_object*,PyImath::FixedMatrix<int>)
        """
        pass

    def __sub__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __sub__(PyImath::FixedMatrix<int>,PyImath::FixedMatrix<int>)
        
        __sub__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __sub__(PyImath::FixedMatrix<int>,int)
        """
        pass

    def __truediv__(self, IntMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (IntMatrix)arg1, (IntMatrix)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __truediv__(PyImath::FixedMatrix<int>,PyImath::FixedMatrix<int>)
        
        __truediv__( (IntMatrix)arg1, (int)arg2) -> IntMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<int> __truediv__(PyImath::FixedMatrix<int>,int)
        """
        pass

    __instance_size__ = 48


