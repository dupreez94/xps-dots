# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V4fArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec4 """
    def dot(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V4fArray)arg1, (V4f)x) -> FloatArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> dot(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        dot( (V4fArray)arg1, (V4fArray)x) -> FloatArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<float> dot(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def ifelse(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V4fArray)arg1, (IntArray)arg2, (V4f)arg3) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > ifelse(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec4<float>)
        
        ifelse( (V4fArray)arg1, (IntArray)arg2, (V4fArray)arg3) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > ifelse(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def length(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V4fArray)arg1) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> length(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def length2(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V4fArray)arg1) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> length2(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def max(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V4fArray)arg1) -> V4f :
        
            C++ signature :
                Imath_2_4::Vec4<float> max(PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def min(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V4fArray)arg1) -> V4f :
        
            C++ signature :
                Imath_2_4::Vec4<float> min(PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def normalize(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V4fArray)arg1) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def normalized(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V4fArray)arg1) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > normalized(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def reduce(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V4fArray)arg1) -> V4f :
        
            C++ signature :
                Imath_2_4::Vec4<float> reduce(PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __add__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __add__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __add__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __add__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __copy__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V4fArray)arg1) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __copy__(PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __deepcopy__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V4fArray)arg1, (dict)arg2) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec4<float> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __div__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __div__( (V4fArray)arg1, (float)x) -> V4fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __div__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __eq__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V4fArray)arg1, (V4f)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __eq__( (V4fArray)arg1, (V4fArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __getitem__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V4fArray)arg1, (object)arg2) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},_object*)
        
        __getitem__( (V4fArray)arg1, (IntArray)arg2) -> V4fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V4fArray)arg1, (int)arg2) -> V4f :
        
            C++ signature :
                Imath_2_4::Vec4<float> __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},long)
        
        __getitem__( (V4fArray)arg1, (int)arg2) -> V4f :
        
            C++ signature :
                Imath_2_4::Vec4<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},long)
        """
        pass

    def __iadd__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __iadd__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __idiv__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __idiv__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __idiv__( (V4fArray)arg1, (float)x) -> V4fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __idiv__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __imul__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __imul__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __imul__( (V4fArray)arg1, (float)x) -> V4fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __imul__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V4fArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __init__( (object)arg1, (V4f)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec4<float>,unsigned long)
        
        __init__( (object)arg1, (V4iArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec4<int> >)
        
        __init__( (object)arg1, (V4dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __isub__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __isub__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __itruediv__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __itruediv__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __itruediv__( (V4fArray)arg1, (float)x) -> V4fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __itruediv__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __len__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V4fArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def __mul__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __mul__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __mul__( (V4fArray)arg1, (float)x) -> V4fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __mul__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __neg__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V4fArray)arg1) -> V4fArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __neg__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue})
        """
        pass

    def __ne__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V4fArray)arg1, (V4f)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __ne__( (V4fArray)arg1, (V4fArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __radd__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __radd__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __rmul__( (V4fArray)arg1, (float)x) -> V4fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __rmul__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __rsub__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        """
        pass

    def __setitem__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V4fArray)arg1, (object)arg2, (V4f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},_object*,Imath_2_4::Vec4<float>)
        
        __setitem__( (V4fArray)arg1, (IntArray)arg2, (V4f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec4<float>)
        
        __setitem__( (V4fArray)arg1, (object)arg2, (V4fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __setitem__( (V4fArray)arg1, (IntArray)arg2, (V4fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __setitem__( (V4fArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},long,boost::python::tuple)
        """
        pass

    def __sub__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __sub__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __sub__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __sub__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        """
        pass

    def __truediv__(self, V4fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V4fArray)arg1, (V4f)x) -> V4fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},Imath_2_4::Vec4<float>)
        
        __truediv__( (V4fArray)arg1, (V4fArray)x) -> V4fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<float> >)
        
        __truediv__( (V4fArray)arg1, (float)x) -> V4fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},float)
        
        __truediv__( (V4fArray)arg1, (FloatArray)x) -> V4fArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<float> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<float> > {lvalue},PyImath::FixedArray<float>)
        """
        pass

    w = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


