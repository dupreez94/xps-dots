# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box2iArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Box """
    def ifelse(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (Box2iArray)arg1, (IntArray)arg2, (Box2i)arg3) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > ifelse(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        
        ifelse( (Box2iArray)arg1, (IntArray)arg2, (Box2iArray)arg3) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > ifelse(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >)
        """
        pass

    def __copy__(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Box2iArray)arg1) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > __copy__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >)
        """
        pass

    def __deepcopy__(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Box2iArray)arg1, (dict)arg2) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > __deepcopy__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >,boost::python::dict {lvalue})
        """
        pass

    def __getitem__(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Box2iArray)arg1, (object)arg2) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},_object*)
        
        __getitem__( (Box2iArray)arg1, (IntArray)arg2) -> Box2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (Box2iArray)arg1, (int)arg2) -> Box2i :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<int> > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},long)
        
        __getitem__( (Box2iArray)arg1, (int)arg2) -> Box2i :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (Box2iArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >)
        
        __init__( (object)arg1, (Box2i)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Box<Imath_2_4::Vec2<int> >,unsigned long)
        """
        pass

    def __len__(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Box2iArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, Box2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Box2iArray)arg1, (object)arg2, (Box2i)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},_object*,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        
        __setitem__( (Box2iArray)arg1, (IntArray)arg2, (Box2i)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        
        __setitem__( (Box2iArray)arg1, (object)arg2, (Box2iArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >)
        
        __setitem__( (Box2iArray)arg1, (IntArray)arg2, (Box2iArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > >)
        
        __setitem__( (Box2iArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec2<int> > > {lvalue},long,boost::python::tuple)
        """
        pass

    max = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    min = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


