# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KStringHandler(__sip.simplewrapper):
    # no doc
    def capwords(self, *args, **kwargs): # real signature unknown
        pass

    def csqueeze(self, *args, **kwargs): # real signature unknown
        pass

    def from8Bit(self, *args, **kwargs): # real signature unknown
        pass

    def isUtf8(self, *args, **kwargs): # real signature unknown
        pass

    def logicalLength(self, *args, **kwargs): # real signature unknown
        pass

    def lsqueeze(self, *args, **kwargs): # real signature unknown
        pass

    def obscure(self, *args, **kwargs): # real signature unknown
        pass

    def perlSplit(self, *args, **kwargs): # real signature unknown
        pass

    def preProcessWrap(self, *args, **kwargs): # real signature unknown
        pass

    def rsqueeze(self, *args, **kwargs): # real signature unknown
        pass

    def tagUrls(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



