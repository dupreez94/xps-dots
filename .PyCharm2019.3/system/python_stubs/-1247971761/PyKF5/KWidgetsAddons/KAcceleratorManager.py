# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KAcceleratorManager(__sip.wrapper):
    # no doc
    def addStandardActionNames(self, *args, **kwargs): # real signature unknown
        pass

    def last_manage(self, *args, **kwargs): # real signature unknown
        pass

    def manage(self, *args, **kwargs): # real signature unknown
        pass

    def setNoAccel(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



