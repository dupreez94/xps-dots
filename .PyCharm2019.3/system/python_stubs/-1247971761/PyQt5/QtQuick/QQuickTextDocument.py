# encoding: utf-8
# module PyQt5.QtQuick
# from /usr/lib/python3.8/site-packages/PyQt5/QtQuick.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml
import sip as __sip


class QQuickTextDocument(__PyQt5_QtCore.QObject):
    """ QQuickTextDocument(QQuickItem) """
    def textDocument(self): # real signature unknown; restored from __doc__
        """ textDocument(self) -> QTextDocument """
        pass

    def __init__(self, QQuickItem): # real signature unknown; restored from __doc__
        pass


