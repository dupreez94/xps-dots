# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V3d(__Boost_Python.instance):
    """ V3d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def closestVertex(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestVertex( (V3d)arg1, (V3d)arg2, (V3d)arg3, (V3d)arg4) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> closestVertex(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def cross(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V3d)arg1, (V3d)arg2) -> V3d :
            v1.cross(v2) right handed cross product
        
            C++ signature :
                Imath_2_4::Vec3<double> cross(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        cross( (V3d)arg1, (V3dArray)arg2) -> V3dArray :
            v1.cross(v2) right handed array cross product
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > cross(Imath_2_4::Vec3<double>,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V3d)arg1, (V3d)arg2) -> float :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                double dot(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        dot( (V3d)arg1, (V3dArray)arg2) -> DoubleArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<double> dot(Imath_2_4::Vec3<double>,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def equalWithAbsError(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V3d)arg1, (V3d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>,double)
        
        equalWithAbsError( (V3d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec3<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V3d)arg1, (V3d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>,double)
        
        equalWithRelError( (V3d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec3<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V3d)arg1) -> float :
            length() magnitude of the vector
        
            C++ signature :
                double length(Imath_2_4::Vec3<double>)
        """
        pass

    def length2(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V3d)arg1) -> float :
            length2() square magnitude of the vector
        
            C++ signature :
                double length2(Imath_2_4::Vec3<double>)
        """
        pass

    def negate(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V3d)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> negate(Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def normalize(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V3d)arg1) -> V3d :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec3<double> normalize(Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def normalized(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V3d)arg1) -> V3d :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec3<double> normalized(Imath_2_4::Vec3<double>)
        """
        pass

    def normalizedExc(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V3d)arg1) -> V3d :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec3<double> normalizedExc(Imath_2_4::Vec3<double>)
        """
        pass

    def normalizedNonNull(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V3d)arg1) -> V3d :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec3<double> normalizedNonNull(Imath_2_4::Vec3<double>)
        """
        pass

    def normalizeExc(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V3d)arg1) -> V3d :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec3<double> normalizeExc(Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def normalizeNonNull(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V3d)arg1) -> V3d :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec3<double> normalizeNonNull(Imath_2_4::Vec3<double> {lvalue})
        """
        pass

    def orthogonal(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> orthogonal(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def project(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> project(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def reflect(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> reflect(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def setValue(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V3d)arg1, (float)arg2, (float)arg3, (float)arg4) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec3<double> {lvalue},double,double,double)
        """
        pass

    def __add__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __add__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<int>)
        
        __add__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<float>)
        
        __add__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __add__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,double)
        
        __add__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __add__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __add__(Imath_2_4::Vec3<double>,boost::python::list)
        """
        pass

    def __copy__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V3d)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __copy__(Imath_2_4::Vec3<double>)
        """
        pass

    def __deepcopy__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V3d)arg1, (dict)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __deepcopy__(Imath_2_4::Vec3<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __div__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<int> {lvalue})
        
        __div__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        __div__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        
        __div__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __div__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double>,boost::python::list)
        
        __div__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __div__(Imath_2_4::Vec3<double>,double)
        """
        pass

    def __eq__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V3d)arg1, (V3d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>)
        
        __eq__( (V3d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec3<double>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V3d)arg1, (int)arg2) -> float :
        
            C++ signature :
                double {lvalue} __getitem__(Imath_2_4::Vec3<double> {lvalue},long)
        """
        pass

    def __ge__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V3d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec3<double>,boost::python::api::object)
        """
        pass

    def __gt__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V3d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec3<double>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __iadd__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<int>)
        
        __iadd__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __iadd__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<float>)
        
        __iadd__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __iadd__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __idiv__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V3d)arg1, (object)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __idiv__(Imath_2_4::Vec3<double> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<int>)
        
        __imul__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<float>)
        
        __imul__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>)
        
        __imul__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},double)
        
        __imul__( (V3d)arg1, (M44f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __imul__( (V3d)arg1, (M44d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __imul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V3d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3, (object)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __isub__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<int>)
        
        __isub__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __isub__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<float>)
        
        __isub__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __isub__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __itruediv__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V3d)arg1, (object)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __itruediv__(Imath_2_4::Vec3<double> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V3d)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec3<double>)
        """
        pass

    def __le__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V3d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec3<double>,boost::python::api::object)
        """
        pass

    def __lt__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V3d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec3<double>,boost::python::api::object)
        """
        pass

    def __mod__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mod__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def __mul__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<int> {lvalue})
        
        __mul__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<float> {lvalue})
        
        __mul__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double> {lvalue})
        
        __mul__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,double)
        
        __mul__( (V3d)arg1, (DoubleArray)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __mul__(Imath_2_4::Vec3<double>,PyImath::FixedArray<double>)
        
        __mul__( (V3d)arg1, (M33f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix33<float>)
        
        __mul__( (V3d)arg1, (M33d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        __mul__( (V3d)arg1, (M44f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __mul__( (V3d)arg1, (M44d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __mul__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __mul__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __mul__(Imath_2_4::Vec3<double>,boost::python::tuple)
        """
        pass

    def __neg__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V3d)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __neg__(Imath_2_4::Vec3<double>)
        """
        pass

    def __ne__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V3d)arg1, (V3d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double>)
        
        __ne__( (V3d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec3<double>,boost::python::tuple)
        """
        pass

    def __radd__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __radd__(Imath_2_4::Vec3<double>,double)
        
        __radd__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __radd__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __radd__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __radd__(Imath_2_4::Vec3<double>,boost::python::list)
        
        __radd__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __radd__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def __rdiv__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rdiv__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __rdiv__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rdiv__(Imath_2_4::Vec3<double>,boost::python::list)
        
        __rdiv__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rdiv__(Imath_2_4::Vec3<double>,double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V3d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec3<double>)
        """
        pass

    def __rmul__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rmul__(Imath_2_4::Vec3<double> {lvalue},double)
        
        __rmul__( (V3d)arg1, (DoubleArray)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(Imath_2_4::Vec3<double>,PyImath::FixedArray<double>)
        
        __rmul__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rmul__(Imath_2_4::Vec3<double>,boost::python::tuple)
        """
        pass

    def __rsub__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rsub__(Imath_2_4::Vec3<double>,double)
        
        __rsub__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rsub__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __rsub__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rsub__(Imath_2_4::Vec3<double>,boost::python::list)
        """
        pass

    def __setitem__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V3d)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec3<double> {lvalue},long,double)
        """
        pass

    def __str__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V3d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec3<double>)
        """
        pass

    def __sub__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __sub__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<int>)
        
        __sub__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<float>)
        
        __sub__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __sub__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,double)
        
        __sub__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __sub__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __sub__(Imath_2_4::Vec3<double>,boost::python::list)
        """
        pass

    def __truediv__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __truediv__( (V3d)arg1, (V3i)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<int> {lvalue})
        
        __truediv__( (V3d)arg1, (V3f)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        __truediv__( (V3d)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        
        __truediv__( (V3d)arg1, (tuple)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double>,boost::python::tuple)
        
        __truediv__( (V3d)arg1, (list)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double>,boost::python::list)
        
        __truediv__( (V3d)arg1, (float)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __truediv__(Imath_2_4::Vec3<double>,double)
        """
        pass

    def __xor__(self, V3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V3d)arg1, (V3d)arg2) -> float :
        
            C++ signature :
                double __xor__(Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 40


