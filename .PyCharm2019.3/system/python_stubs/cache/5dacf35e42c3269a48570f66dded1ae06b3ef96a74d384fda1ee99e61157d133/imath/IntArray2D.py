# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class IntArray2D(__Boost_Python.instance):
    """ Fixed length array of ints """
    def ifelse(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (IntArray2D)arg1, (IntArray2D)arg2, (int)arg3) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> ifelse(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>,int)
        
        ifelse( (IntArray2D)arg1, (IntArray2D)arg2, (IntArray2D)arg3) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> ifelse(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        """
        pass

    def item(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        item( (IntArray2D)arg1, (int)arg2, (int)arg3) -> int :
        
            C++ signature :
                int item(PyImath::FixedArray2D<int> {lvalue},long,long)
        """
        pass

    def size(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (IntArray2D)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple size(PyImath::FixedArray2D<int> {lvalue})
        """
        pass

    def __add__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __add__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __add__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __add__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __div__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __div__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __div__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __div__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __eq__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __eq__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __getitem__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (IntArray2D)arg1, (object)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __getitem__(PyImath::FixedArray2D<int> {lvalue},_object*)
        
        __getitem__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __getitem__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        """
        pass

    def __ge__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __ge__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __gt__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __gt__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __iadd__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __iadd__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __iadd__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __iadd__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __idiv__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __idiv__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __idiv__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __idiv__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __imod__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __imod__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __imod__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __imod__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __imul__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __imul__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __imul__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __imul__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long,unsigned long)
        
        __init__( (object)arg1, (IntArray2D)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<int>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3, (int)arg4) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,int,unsigned long,unsigned long)
        
        __init__( (object)arg1, (FloatArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<float>)
        
        __init__( (object)arg1, (DoubleArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<double>)
        """
        pass

    def __isub__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __isub__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __isub__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __isub__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __itruediv__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __itruediv__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>)
        
        __itruediv__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> {lvalue} __itruediv__(PyImath::FixedArray2D<int> {lvalue},int)
        """
        pass

    def __len__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (IntArray2D)arg1) -> int :
        
            C++ signature :
                unsigned long __len__(PyImath::FixedArray2D<int> {lvalue})
        """
        pass

    def __le__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __le__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __lt__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __lt__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __mod__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __mod__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __mod__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __mod__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __mul__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __mul__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __mul__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __mul__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __neg__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (IntArray2D)arg1) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __neg__(PyImath::FixedArray2D<int>)
        """
        pass

    def __ne__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __ne__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __radd__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __radd__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __rmul__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __rsub__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __rsub__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __setitem__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (IntArray2D)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},_object*,int)
        
        __setitem__( (IntArray2D)arg1, (IntArray2D)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>,int)
        
        __setitem__( (IntArray2D)arg1, (object)arg2, (IntArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},_object*,PyImath::FixedArray2D<int>)
        
        __setitem__( (IntArray2D)arg1, (IntArray2D)arg2, (IntArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __setitem__( (IntArray2D)arg1, (object)arg2, (IntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},_object*,PyImath::FixedArray<int>)
        
        __setitem__( (IntArray2D)arg1, (IntArray2D)arg2, (IntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<int> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray<int>)
        """
        pass

    def __sub__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __sub__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __sub__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __sub__(PyImath::FixedArray2D<int>,int)
        """
        pass

    def __truediv__(self, IntArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (IntArray2D)arg1, (IntArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __truediv__(PyImath::FixedArray2D<int>,PyImath::FixedArray2D<int>)
        
        __truediv__( (IntArray2D)arg1, (int)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __truediv__(PyImath::FixedArray2D<int>,int)
        """
        pass

    __instance_size__ = 72


