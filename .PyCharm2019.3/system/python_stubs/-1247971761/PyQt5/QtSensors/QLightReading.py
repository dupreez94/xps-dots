# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorReading import QSensorReading

class QLightReading(QSensorReading):
    # no doc
    def lux(self): # real signature unknown; restored from __doc__
        """ lux(self) -> float """
        return 0.0

    def setLux(self, p_float): # real signature unknown; restored from __doc__
        """ setLux(self, float) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


