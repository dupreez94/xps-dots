# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class file_entry(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    executable_attribute = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    filehash = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    hidden_attribute = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    mtime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    offset = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pad_file = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    symlink_attribute = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    symlink_path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 128


