# encoding: utf-8
# module PyKF5.KCompletion
# from /usr/lib/python3.8/site-packages/PyKF5/KCompletion.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


# no functions
# classes

class KCompletionBase(__sip.wrapper):
    # no doc
    def completionMode(self, *args, **kwargs): # real signature unknown
        pass

    def completionObject(self, *args, **kwargs): # real signature unknown
        pass

    def compObj(self, *args, **kwargs): # real signature unknown
        pass

    def delegate(self, *args, **kwargs): # real signature unknown
        pass

    def emitSignals(self, *args, **kwargs): # real signature unknown
        pass

    def getKeyBinding(self, *args, **kwargs): # real signature unknown
        pass

    def handleSignals(self, *args, **kwargs): # real signature unknown
        pass

    def isCompletionObjectAutoDeleted(self, *args, **kwargs): # real signature unknown
        pass

    def keyBinding(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoDeleteCompletionObject(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedItems(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedText(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionMode(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionObject(self, *args, **kwargs): # real signature unknown
        pass

    def setDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def setEmitSignals(self, *args, **kwargs): # real signature unknown
        pass

    def setEnableSignals(self, *args, **kwargs): # real signature unknown
        pass

    def setHandleSignals(self, *args, **kwargs): # real signature unknown
        pass

    def setKeyBinding(self, *args, **kwargs): # real signature unknown
        pass

    def useGlobalKeyBindings(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    KeyBindingType = None # (!) real value is "<class 'PyKF5.KCompletion.KCompletionBase.KeyBindingType'>"
    NextCompletionMatch = 2
    PrevCompletionMatch = 1
    SubstringCompletion = 3
    TextCompletion = 0


class KComboBox(__PyQt5_QtWidgets.QComboBox, KCompletionBase):
    # no doc
    def aboutToShowContextMenu(self, *args, **kwargs): # real signature unknown
        pass

    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def addUrl(self, *args, **kwargs): # real signature unknown
        pass

    def autoCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def changeURL(self, *args, **kwargs): # real signature unknown
        pass

    def changeUrl(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def completion(self, *args, **kwargs): # real signature unknown
        pass

    def completionBox(self, *args, **kwargs): # real signature unknown
        pass

    def completionModeChanged(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contains(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def cursorPosition(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def delegate(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def initStyleOption(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def insertURL(self, *args, **kwargs): # real signature unknown
        pass

    def insertUrl(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def makeCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def minimumSizeHint(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def returnPressed(self, *args, **kwargs): # real signature unknown
        pass

    def rotateText(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedItems(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedText(self, *args, **kwargs): # real signature unknown
        pass

    def setContextMenuEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setCurrentItem(self, *args, **kwargs): # real signature unknown
        pass

    def setDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def setEditable(self, *args, **kwargs): # real signature unknown
        pass

    def setEditUrl(self, *args, **kwargs): # real signature unknown
        pass

    def setLineEdit(self, *args, **kwargs): # real signature unknown
        pass

    def setTrapReturnKey(self, *args, **kwargs): # real signature unknown
        pass

    def setUrlDropsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def substringCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def textRotation(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def trapReturnKey(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def urlDropsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KCompletion(__PyQt5_QtCore.QObject):
    # no doc
    def addItem(self, *args, **kwargs): # real signature unknown
        pass

    def allMatches(self, *args, **kwargs): # real signature unknown
        pass

    def allWeightedMatches(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def clear(self, *args, **kwargs): # real signature unknown
        pass

    def completionMode(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def hasMultipleMatches(self, *args, **kwargs): # real signature unknown
        pass

    def ignoreCase(self, *args, **kwargs): # real signature unknown
        pass

    def insertItems(self, *args, **kwargs): # real signature unknown
        pass

    def isEmpty(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def items(self, *args, **kwargs): # real signature unknown
        pass

    def lastMatch(self, *args, **kwargs): # real signature unknown
        pass

    def makeCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def match(self, *args, **kwargs): # real signature unknown
        pass

    def matches(self, *args, **kwargs): # real signature unknown
        pass

    def multipleMatches(self, *args, **kwargs): # real signature unknown
        pass

    def nextMatch(self, *args, **kwargs): # real signature unknown
        pass

    def order(self, *args, **kwargs): # real signature unknown
        pass

    def postProcessMatch(self, *args, **kwargs): # real signature unknown
        pass

    def postProcessMatches(self, *args, **kwargs): # real signature unknown
        pass

    def previousMatch(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeItem(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionMode(self, *args, **kwargs): # real signature unknown
        pass

    def setIgnoreCase(self, *args, **kwargs): # real signature unknown
        pass

    def setItems(self, *args, **kwargs): # real signature unknown
        pass

    def setOrder(self, *args, **kwargs): # real signature unknown
        pass

    def setSoundsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def slotMakeCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def slotNextMatch(self, *args, **kwargs): # real signature unknown
        pass

    def slotPreviousMatch(self, *args, **kwargs): # real signature unknown
        pass

    def soundsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def substringCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    CompletionAuto = 2
    CompletionMan = 3
    CompletionMode = None # (!) real value is "<class 'PyKF5.KCompletion.KCompletion.CompletionMode'>"
    CompletionNone = 1
    CompletionPopup = 5
    CompletionPopupAuto = 6
    CompletionShell = 4
    CompOrder = None # (!) real value is "<class 'PyKF5.KCompletion.KCompletion.CompOrder'>"
    Insertion = 1
    Sorted = 0
    Weighted = 2


class KCompletionBox(__PyQt5_QtWidgets.QListWidget):
    # no doc
    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def activated(self, *args, **kwargs): # real signature unknown
        pass

    def activateOnSelect(self, *args, **kwargs): # real signature unknown
        pass

    def calculateGeometry(self, *args, **kwargs): # real signature unknown
        pass

    def cancelledText(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def closeEditor(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def commitData(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def currentChanged(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dataChanged(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def dirtyRegionOffset(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def down(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def drawFrame(self, *args, **kwargs): # real signature unknown
        pass

    def dropIndicatorPosition(self, *args, **kwargs): # real signature unknown
        pass

    def dropMimeData(self, *args, **kwargs): # real signature unknown
        pass

    def edit(self, *args, **kwargs): # real signature unknown
        pass

    def editorDestroyed(self, *args, **kwargs): # real signature unknown
        pass

    def end(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def eventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def executeDelayedItemsLayout(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def globalPositionHint(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def home(self, *args, **kwargs): # real signature unknown
        pass

    def horizontalOffset(self, *args, **kwargs): # real signature unknown
        pass

    def horizontalScrollbarAction(self, *args, **kwargs): # real signature unknown
        pass

    def horizontalScrollbarValueChanged(self, *args, **kwargs): # real signature unknown
        pass

    def indexFromItem(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def initStyleOption(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def insertItems(self, *args, **kwargs): # real signature unknown
        pass

    def isIndexHidden(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def isTabHandling(self, *args, **kwargs): # real signature unknown
        pass

    def itemFromIndex(self, *args, **kwargs): # real signature unknown
        pass

    def items(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def mimeData(self, *args, **kwargs): # real signature unknown
        pass

    def mimeTypes(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveCursor(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def pageDown(self, *args, **kwargs): # real signature unknown
        pass

    def pageUp(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def popup(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def rectForIndex(self, *args, **kwargs): # real signature unknown
        pass

    def resizeAndReposition(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def rowsAboutToBeRemoved(self, *args, **kwargs): # real signature unknown
        pass

    def rowsInserted(self, *args, **kwargs): # real signature unknown
        pass

    def scheduleDelayedItemsLayout(self, *args, **kwargs): # real signature unknown
        pass

    def scrollContentsBy(self, *args, **kwargs): # real signature unknown
        pass

    def scrollDirtyRegion(self, *args, **kwargs): # real signature unknown
        pass

    def selectedIndexes(self, *args, **kwargs): # real signature unknown
        pass

    def selectionChanged(self, *args, **kwargs): # real signature unknown
        pass

    def selectionCommand(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setActivateOnSelect(self, *args, **kwargs): # real signature unknown
        pass

    def setCancelledText(self, *args, **kwargs): # real signature unknown
        pass

    def setDirtyRegion(self, *args, **kwargs): # real signature unknown
        pass

    def setItems(self, *args, **kwargs): # real signature unknown
        pass

    def setPositionForIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setSelection(self, *args, **kwargs): # real signature unknown
        pass

    def setState(self, *args, **kwargs): # real signature unknown
        pass

    def setTabHandling(self, *args, **kwargs): # real signature unknown
        pass

    def setViewportMargins(self, *args, **kwargs): # real signature unknown
        pass

    def setVisible(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def sizeAndPosition(self, *args, **kwargs): # real signature unknown
        pass

    def sizeHint(self, *args, **kwargs): # real signature unknown
        pass

    def slotActivated(self, *args, **kwargs): # real signature unknown
        pass

    def startDrag(self, *args, **kwargs): # real signature unknown
        pass

    def state(self, *args, **kwargs): # real signature unknown
        pass

    def supportedDropActions(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def up(self, *args, **kwargs): # real signature unknown
        pass

    def updateEditorData(self, *args, **kwargs): # real signature unknown
        pass

    def updateEditorGeometries(self, *args, **kwargs): # real signature unknown
        pass

    def updateGeometries(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def userCancelled(self, *args, **kwargs): # real signature unknown
        pass

    def verticalOffset(self, *args, **kwargs): # real signature unknown
        pass

    def verticalScrollbarAction(self, *args, **kwargs): # real signature unknown
        pass

    def verticalScrollbarValueChanged(self, *args, **kwargs): # real signature unknown
        pass

    def viewOptions(self, *args, **kwargs): # real signature unknown
        pass

    def viewportEvent(self, *args, **kwargs): # real signature unknown
        pass

    def viewportMargins(self, *args, **kwargs): # real signature unknown
        pass

    def viewportSizeHint(self, *args, **kwargs): # real signature unknown
        pass

    def visualRegionForSelection(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KCompletionMatches(__sip.wrapper):
    # no doc
    def list(self, *args, **kwargs): # real signature unknown
        pass

    def removeDuplicates(self, *args, **kwargs): # real signature unknown
        pass

    def sorting(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KHistoryComboBox(KComboBox):
    # no doc
    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def addToHistory(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def cleared(self, *args, **kwargs): # real signature unknown
        pass

    def clearHistory(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def delegate(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def historyItems(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def initStyleOption(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def insertItems(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def makeCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def minimumSizeHint(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def pixmapProvider(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeFromHistory(self, *args, **kwargs): # real signature unknown
        pass

    def reset(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedText(self, *args, **kwargs): # real signature unknown
        pass

    def setDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def setHistoryItems(self, *args, **kwargs): # real signature unknown
        pass

    def setPixmapProvider(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def useCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KLineEdit(__PyQt5_QtWidgets.QLineEdit, KCompletionBase):
    # no doc
    def aboutToShowContextMenu(self, *args, **kwargs): # real signature unknown
        pass

    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def autoSuggest(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def clearButtonClicked(self, *args, **kwargs): # real signature unknown
        pass

    def clearButtonUsedSize(self, *args, **kwargs): # real signature unknown
        pass

    def clickMessage(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def completion(self, *args, **kwargs): # real signature unknown
        pass

    def completionBox(self, *args, **kwargs): # real signature unknown
        pass

    def completionBoxActivated(self, *args, **kwargs): # real signature unknown
        pass

    def completionModeChanged(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def copy(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def createStandardContextMenu(self, *args, **kwargs): # real signature unknown
        pass

    def cursorRect(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def delegate(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def doCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def initStyleOption(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def isClearButtonShown(self, *args, **kwargs): # real signature unknown
        pass

    def isContextMenuEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def isSqueezedTextEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def makeCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def originalText(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def passwordMode(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def returnPressed(self, *args, **kwargs): # real signature unknown
        pass

    def rotateText(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setClearButtonShown(self, *args, **kwargs): # real signature unknown
        pass

    def setClickMessage(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedItems(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletedText(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionBox(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionMode(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionModeDisabled(self, *args, **kwargs): # real signature unknown
        pass

    def setCompletionObject(self, *args, **kwargs): # real signature unknown
        pass

    def setContextMenuEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def setPasswordMode(self, *args, **kwargs): # real signature unknown
        pass

    def setReadOnly(self, *args, **kwargs): # real signature unknown
        pass

    def setSqueezedText(self, *args, **kwargs): # real signature unknown
        pass

    def setSqueezedTextEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setText(self, *args, **kwargs): # real signature unknown
        pass

    def setTrapReturnKey(self, *args, **kwargs): # real signature unknown
        pass

    def setUrl(self, *args, **kwargs): # real signature unknown
        pass

    def setUrlDropsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setUserSelection(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def substringCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def textRotation(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def trapReturnKey(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def urlDropsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def userCancelled(self, *args, **kwargs): # real signature unknown
        pass

    def userText(self, *args, **kwargs): # real signature unknown
        pass

    def userTextChanged(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KPixmapProvider(__sip.wrapper):
    # no doc
    def pixmapFor(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KCompletion', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KCompletion.so')"

