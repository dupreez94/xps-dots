# encoding: utf-8
# module PyQt5.QtLocation
# from /usr/lib/python3.8/site-packages/PyQt5/QtLocation.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QPlaceReply import QPlaceReply

class QPlaceSearchSuggestionReply(QPlaceReply):
    """ QPlaceSearchSuggestionReply(parent: QObject = None) """
    def setSuggestions(self, Iterable, p_str=None): # real signature unknown; restored from __doc__
        """ setSuggestions(self, Iterable[str]) """
        pass

    def suggestions(self): # real signature unknown; restored from __doc__
        """ suggestions(self) -> List[str] """
        return []

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


