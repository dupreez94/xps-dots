# encoding: utf-8
# module dbus.mainloop.pyqt5
# from /usr/lib/python3.8/site-packages/dbus/mainloop/pyqt5.abi3.so
# by generator 1.147
# no doc
# no imports

# functions

def DBusQtMainLoop(set_as_default=False): # real signature unknown; restored from __doc__
    """
    DBusQtMainLoop([set_as_default=False]) -> NativeMainLoop
    
    Return a NativeMainLoop object.
    
    If the keyword argument set_as_default is given and is True, set the new
    main loop as the default for all new Connection or Bus instances.
    """
    pass

# no classes
# variables with complex values



