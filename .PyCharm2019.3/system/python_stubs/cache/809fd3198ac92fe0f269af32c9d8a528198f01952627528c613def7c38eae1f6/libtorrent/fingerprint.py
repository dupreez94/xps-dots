# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class fingerprint(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (str)id, (int)major, (int)minor, (int)revision, (int)tag) -> None :
        
            C++ signature :
                void __init__(_object*,char const*,int,int,int,int)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __str__(self, fingerprint, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (fingerprint)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(libtorrent::fingerprint {lvalue})
        """
        pass

    major_version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    minor_version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    revision_version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tag_version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



