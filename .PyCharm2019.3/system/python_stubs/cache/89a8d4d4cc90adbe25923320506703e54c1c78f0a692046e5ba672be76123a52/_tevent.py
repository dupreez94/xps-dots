# encoding: utf-8
# module _tevent
# from /usr/lib/python3.8/site-packages/_tevent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Python wrapping of talloc-maintained objects. """
# no imports

# Variables with simple values

__version__ = '0.9.39'

# functions

def backend_list(): # real signature unknown; restored from __doc__
    """ backend_list() -> list """
    return []

def register_backend(backend): # real signature unknown; restored from __doc__
    """ register_backend(backend) """
    pass

def set_default_backend(backend): # real signature unknown; restored from __doc__
    """ set_default_backend(backend) """
    pass

# classes

class Context(object):
    # no doc
    def add_fd(self, fd, flags, handler): # real signature unknown; restored from __doc__
        """ S.add_fd(fd, flags, handler) -> fd """
        pass

    def add_signal(self, signum, sa_flags, handler): # real signature unknown; restored from __doc__
        """ S.add_signal(signum, sa_flags, handler) -> signal """
        pass

    def add_timer(self, next_event, handler): # real signature unknown; restored from __doc__
        """ S.add_timer(next_event, handler) -> timer """
        pass

    def add_timer_offset(self, *args, **kwargs): # real signature unknown
        """ S.add_timer(offset_seconds, handler) -> timer """
        pass

    def loop_once(self): # real signature unknown; restored from __doc__
        """ S.loop_once() """
        pass

    def loop_wait(self): # real signature unknown; restored from __doc__
        """ S.loop_wait() """
        pass

    def reinitialise(self): # real signature unknown; restored from __doc__
        """ S.reinitialise() """
        pass

    def wakeup_send(self, wakeup_time): # real signature unknown; restored from __doc__
        """ S.wakeup_send(wakeup_time) -> req """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    signal_support = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """if this platform and tevent context support signal handling"""



class Fd(object):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class Queue(object):
    # no doc
    def add(self, ctx, req, trigger, baton): # real signature unknown; restored from __doc__
        """ S.add(ctx, req, trigger, baton) """
        pass

    def start(self): # real signature unknown; restored from __doc__
        """ S.start() """
        pass

    def stop(self): # real signature unknown; restored from __doc__
        """ S.stop() """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    length = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The number of elements in the queue."""



class Request(object):
    # no doc
    def cancel(self): # real signature unknown; restored from __doc__
        """ cancel() """
        pass

    def done(self): # real signature unknown; restored from __doc__
        """ done() """
        pass

    def is_error(self): # real signature unknown; restored from __doc__
        """ is_error() -> (error, state) """
        pass

    def notify_callback(self): # real signature unknown; restored from __doc__
        """ notify_callback() """
        pass

    def poll(self, ctx): # real signature unknown; restored from __doc__
        """ poll(ctx) """
        pass

    def post(self, ctx): # real signature unknown; restored from __doc__
        """ post(ctx) -> req """
        pass

    def received(self, *args, **kwargs): # real signature unknown
        """ Receive finished """
        pass

    def set_endtime(self, ctx, endtime): # real signature unknown; restored from __doc__
        """ set_endtime(ctx, endtime) """
        pass

    def set_error(self, error): # real signature unknown; restored from __doc__
        """ set_error(error) """
        pass

    def wakeup_recv(self, *args, **kwargs): # real signature unknown
        """ Wakeup received """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    in_progress = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Whether the request is in progress"""



class Signal(object):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class Timer(object):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    active = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """true if the timer is scheduled to run"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='_tevent', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/_tevent.cpython-38-x86_64-linux-gnu.so')"

