# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M33dArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Matrix33 """
    def ifelse(self, M33dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (M33dArray)arg1, (IntArray)arg2, (M33d)arg3) -> M33dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<double> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix33<double>)
        
        ifelse( (M33dArray)arg1, (IntArray)arg2, (M33dArray)arg3) -> M33dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<double> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        """
        pass

    def __getitem__(self, M33dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M33dArray)arg1, (object)arg2) -> M33dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},_object*)
        
        __getitem__( (M33dArray)arg1, (IntArray)arg2) -> M33dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (M33dArray)arg1, (int)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},long)
        
        __getitem__( (M33dArray)arg1, (int)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (M33dArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        
        __init__( (object)arg1, (M33d)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Matrix33<double>,unsigned long)
        
        __init__( (object)arg1, (M33dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        """
        pass

    def __len__(self, M33dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M33dArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, M33dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (M33dArray)arg1, (object)arg2, (M33d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},_object*,Imath_2_4::Matrix33<double>)
        
        __setitem__( (M33dArray)arg1, (IntArray)arg2, (M33d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix33<double>)
        
        __setitem__( (M33dArray)arg1, (object)arg2, (M33dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        
        __setitem__( (M33dArray)arg1, (IntArray)arg2, (M33dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        
        __setitem__( (M33dArray)arg1, (int)arg2, (M33d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<double> > {lvalue},long,Imath_2_4::Matrix33<double>)
        """
        pass

    __instance_size__ = 72


