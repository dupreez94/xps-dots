# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Plane3f(__Boost_Python.instance):
    # no doc
    def distance(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        distance( (Plane3f)arg1) -> float :
            distance()
        
            C++ signature :
                float distance(Imath_2_4::Plane3<float> {lvalue})
        """
        pass

    def distanceTo(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        distanceTo( (Plane3f)arg1, (V3f)arg2) -> float :
            distanceTo()
        
            C++ signature :
                float distanceTo(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>)
        
        distanceTo( (Plane3f)arg1, (tuple)arg2) -> float :
        
            C++ signature :
                float distanceTo(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple)
        """
        pass

    def intersect(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersect( (Plane3f)arg1, (Line3f)arg2, (V3f)arg3) -> bool :
            pl.intersect(ln, pt) -- returns true if the line intersects
            the plane, false if it doesn't.  The point where plane
            pl and line ln intersect is stored in pt
        
            C++ signature :
                bool intersect(Imath_2_4::Plane3<float>,Imath_2_4::Line3<float>,Imath_2_4::Vec3<float> {lvalue})
        
        intersect( (Plane3f)arg1, (Line3f)arg2) -> object :
            pl.intersect(ln) -- returns the point where plane
            pl and line ln intersect, or None if pl and ln do
            not intersect
        
            C++ signature :
                boost::python::api::object intersect(Imath_2_4::Plane3<float>,Imath_2_4::Line3<float>)
        
        intersect( (Plane3f)arg1, (Line3d)arg2) -> object :
            pl.intersect(ln) -- returns the point where plane
            pl and line ln intersect, or None if pl and ln do
            not intersect
        
            C++ signature :
                boost::python::api::object intersect(Imath_2_4::Plane3<float>,Imath_2_4::Line3<double>)
        """
        pass

    def intersectT(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersectT( (Plane3f)arg1, (Line3f)arg2) -> object :
            pl.intersectT(ln) -- computes the intersection,
            i, of plane pl and line ln, and returns t, so that
            ln.pos() + t * ln.dir() == i.
            If pl and ln do not intersect, pl.intersectT(ln)
            returns None.
            
        
            C++ signature :
                boost::python::api::object intersectT(Imath_2_4::Plane3<float>,Imath_2_4::Line3<float>)
        
        intersectT( (Plane3f)arg1, (Line3d)arg2) -> object :
        
            C++ signature :
                boost::python::api::object intersectT(Imath_2_4::Plane3<float>,Imath_2_4::Line3<double>)
        """
        pass

    def normal(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normal( (Plane3f)arg1) -> V3f :
            normal()
        
            C++ signature :
                Imath_2_4::Vec3<float> normal(Imath_2_4::Plane3<float> {lvalue})
        """
        pass

    def reflectPoint(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflectPoint( (Plane3f)arg1, (V3f)arg2) -> V3f :
            reflectPoint()
        
            C++ signature :
                Imath_2_4::Vec3<float> reflectPoint(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>)
        
        reflectPoint( (Plane3f)arg1, (tuple)arg2) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> reflectPoint(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple)
        """
        pass

    def reflectVector(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflectVector( (Plane3f)arg1, (V3f)arg2) -> V3f :
            reflectVector()
        
            C++ signature :
                Imath_2_4::Vec3<float> reflectVector(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>)
        
        reflectVector( (Plane3f)arg1, (tuple)arg2) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> reflectVector(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple)
        """
        pass

    def set(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set( (Plane3f)arg1, (V3f)arg2, (float)arg3) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>,float)
        
        set( (Plane3f)arg1, (V3f)arg2, (V3f)arg3) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        set( (Plane3f)arg1, (V3f)arg2, (V3f)arg3, (V3f)arg4) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        set( (Plane3f)arg1, (tuple)arg2, (float)arg3) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple,float)
        
        set( (Plane3f)arg1, (tuple)arg2, (tuple)arg3) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple,boost::python::tuple)
        
        set( (Plane3f)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4) -> None :
            set()
        
            C++ signature :
                void set(Imath_2_4::Plane3<float> {lvalue},boost::python::tuple,boost::python::tuple,boost::python::tuple)
        """
        pass

    def setDistance(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setDistance( (Plane3f)arg1, (float)arg2) -> None :
            setDistance()
        
            C++ signature :
                void setDistance(Imath_2_4::Plane3<float> {lvalue},float)
        """
        pass

    def setNormal(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setNormal( (Plane3f)arg1, (V3f)arg2) -> None :
            setNormal()
        
            C++ signature :
                void setNormal(Imath_2_4::Plane3<float> {lvalue},Imath_2_4::Vec3<float>)
        """
        pass

    def __copy__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Plane3f)arg1) -> Plane3f :
        
            C++ signature :
                Imath_2_4::Plane3<float> __copy__(Imath_2_4::Plane3<float>)
        """
        pass

    def __deepcopy__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Plane3f)arg1, (dict)arg2) -> Plane3f :
        
            C++ signature :
                Imath_2_4::Plane3<float> __deepcopy__(Imath_2_4::Plane3<float>,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Plane3f)arg1, (Plane3f)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Plane3<float>,Imath_2_4::Plane3<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> object :
            initialize normal to  (1,0,0), distance to 0
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (tuple)arg2, (float)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,float)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (V3f)arg2, (float)arg3) -> None :
            Plane3(normal, distance) construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<float>,float)
        
        __init__( (object)arg1, (V3f)arg2, (V3f)arg3) -> None :
            Plane3(point, normal) construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        __init__( (object)arg1, (V3f)arg2, (V3f)arg3, (V3f)arg4) -> None :
            Plane3(point1, point2, point3) construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        """
        pass

    def __mul__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Plane3f)arg1, (M44f)arg2) -> Plane3f :
        
            C++ signature :
                Imath_2_4::Plane3<float> __mul__(Imath_2_4::Plane3<float>,Imath_2_4::Matrix44<float>)
        """
        pass

    def __neg__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Plane3f)arg1) -> Plane3f :
        
            C++ signature :
                Imath_2_4::Plane3<float> __neg__(Imath_2_4::Plane3<float>)
        """
        pass

    def __ne__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Plane3f)arg1, (Plane3f)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Plane3<float>,Imath_2_4::Plane3<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Plane3f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Plane3<float>)
        """
        pass

    def __str__(self, Plane3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Plane3f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Plane3<float>)
        """
        pass

    __instance_size__ = 32


