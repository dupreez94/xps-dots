# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KRandomSequence(__sip.wrapper):
    # no doc
    def getBool(self, *args, **kwargs): # real signature unknown
        pass

    def getDouble(self, *args, **kwargs): # real signature unknown
        pass

    def getInt(self, *args, **kwargs): # real signature unknown
        pass

    def getLong(self, *args, **kwargs): # real signature unknown
        pass

    def modulate(self, *args, **kwargs): # real signature unknown
        pass

    def setSeed(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



