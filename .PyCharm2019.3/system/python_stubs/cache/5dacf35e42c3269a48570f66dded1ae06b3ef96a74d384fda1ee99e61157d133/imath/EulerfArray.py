# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class EulerfArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Euler """
    def ifelse(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (EulerfArray)arg1, (IntArray)arg2, (Eulerf)arg3) -> EulerfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<float> > ifelse(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Euler<float>)
        
        ifelse( (EulerfArray)arg1, (IntArray)arg2, (EulerfArray)arg3) -> EulerfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<float> > ifelse(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        """
        pass

    def __eq__(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (EulerfArray)arg1, (Eulerf)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},Imath_2_4::Euler<float>)
        
        __eq__( (EulerfArray)arg1, (EulerfArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Euler<float> >)
        """
        pass

    def __getitem__(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (EulerfArray)arg1, (object)arg2) -> EulerfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},_object*)
        
        __getitem__( (EulerfArray)arg1, (IntArray)arg2) -> EulerfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (EulerfArray)arg1, (int)arg2) -> Eulerf :
        
            C++ signature :
                Imath_2_4::Euler<float> __getitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},long)
        
        __getitem__( (EulerfArray)arg1, (int)arg2) -> Eulerf :
        
            C++ signature :
                Imath_2_4::Euler<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (EulerfArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        
        __init__( (object)arg1, (Eulerf)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Euler<float>,unsigned long)
        
        __init__( (object)arg1, (QuatfArray)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        
        __init__( (object)arg1, (M33fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        
        __init__( (object)arg1, (M44fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        
        __init__( (object)arg1, (EulerdArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        """
        pass

    def __len__(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (EulerfArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue})
        """
        pass

    def __ne__(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (EulerfArray)arg1, (Eulerf)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},Imath_2_4::Euler<float>)
        
        __ne__( (EulerfArray)arg1, (EulerfArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Euler<float> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, EulerfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (EulerfArray)arg1, (object)arg2, (Eulerf)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},_object*,Imath_2_4::Euler<float>)
        
        __setitem__( (EulerfArray)arg1, (IntArray)arg2, (Eulerf)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Euler<float>)
        
        __setitem__( (EulerfArray)arg1, (object)arg2, (EulerfArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        
        __setitem__( (EulerfArray)arg1, (IntArray)arg2, (EulerfArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        """
        pass

    __instance_size__ = 72


