# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class DoubleArray(__Boost_Python.instance):
    """ Fixed length array of doubles """
    def ifelse(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (DoubleArray)arg1, (IntArray)arg2, (float)arg3) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> ifelse(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<int>,double)
        
        ifelse( (DoubleArray)arg1, (IntArray)arg2, (DoubleArray)arg3) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> ifelse(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<double>)
        """
        pass

    def reduce(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (DoubleArray)arg1) -> float :
        
            C++ signature :
                double reduce(PyImath::FixedArray<double>)
        """
        pass

    def __add__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<double> __add__(PyImath::FixedArray<double> {lvalue},double)
        
        __add__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<double> __add__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __div__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<double> __div__(PyImath::FixedArray<double> {lvalue},double)
        
        __div__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<double> __div__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __eq__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (DoubleArray)arg1, (float)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<double> {lvalue},double)
        
        __eq__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __getitem__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (DoubleArray)arg1, (object)arg2) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> __getitem__(PyImath::FixedArray<double> {lvalue},_object*)
        
        __getitem__( (DoubleArray)arg1, (IntArray)arg2) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> __getitem__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (DoubleArray)arg1, (int)arg2) -> float :
        
            C++ signature :
                double __getitem__(PyImath::FixedArray<double> {lvalue},long)
        
        __getitem__( (DoubleArray)arg1, (int)arg2) -> float :
        
            C++ signature :
                double __getitem__(PyImath::FixedArray<double> {lvalue},long)
        """
        pass

    def __ge__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (DoubleArray)arg1, (float)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<double> {lvalue},double)
        
        __ge__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __gt__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (DoubleArray)arg1, (float)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<double> {lvalue},double)
        
        __gt__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __iadd__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __iadd__(PyImath::FixedArray<double> {lvalue},double)
        
        __iadd__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __iadd__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __idiv__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __idiv__(PyImath::FixedArray<double> {lvalue},double)
        
        __idiv__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __idiv__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __imul__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __imul__(PyImath::FixedArray<double> {lvalue},double)
        
        __imul__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __imul__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (DoubleArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<double>)
        
        __init__( (object)arg1, (float)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,double,unsigned long)
        
        __init__( (object)arg1, (IntArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<int>)
        
        __init__( (object)arg1, (FloatArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<float>)
        """
        pass

    def __ipow__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __ipow__(x) - x**=self
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __ipow__(PyImath::FixedArray<double> {lvalue},double)
        
        __ipow__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __ipow__(x) - x**=self
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __ipow__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __isub__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __isub__(PyImath::FixedArray<double> {lvalue},double)
        
        __isub__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __isub__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __itruediv__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __itruediv__(PyImath::FixedArray<double> {lvalue},double)
        
        __itruediv__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<double> {lvalue} __itruediv__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __len__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (DoubleArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<double> {lvalue})
        """
        pass

    def __le__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (DoubleArray)arg1, (float)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<double> {lvalue},double)
        
        __le__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __lt__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (DoubleArray)arg1, (float)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<double> {lvalue},double)
        
        __lt__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __mul__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<double> __mul__(PyImath::FixedArray<double> {lvalue},double)
        
        __mul__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<double> __mul__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __neg__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (DoubleArray)arg1) -> DoubleArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<double> __neg__(PyImath::FixedArray<double> {lvalue})
        """
        pass

    def __ne__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (DoubleArray)arg1, (float)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<double> {lvalue},double)
        
        __ne__( (DoubleArray)arg1, (DoubleArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __pow__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __pow__(x) - self**x
        
            C++ signature :
                PyImath::FixedArray<double> __pow__(PyImath::FixedArray<double> {lvalue},double)
        
        __pow__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __pow__(x) - self**x
        
            C++ signature :
                PyImath::FixedArray<double> __pow__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __radd__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<double> __radd__(PyImath::FixedArray<double> {lvalue},double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<double> __rmul__(PyImath::FixedArray<double> {lvalue},double)
        """
        pass

    def __rpow__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rpow__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __rpow__(x) - x**self
        
            C++ signature :
                PyImath::FixedArray<double> __rpow__(PyImath::FixedArray<double> {lvalue},double)
        """
        pass

    def __rsub__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<double> __rsub__(PyImath::FixedArray<double> {lvalue},double)
        """
        pass

    def __setitem__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (DoubleArray)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<double> {lvalue},_object*,double)
        
        __setitem__( (DoubleArray)arg1, (IntArray)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<int>,double)
        
        __setitem__( (DoubleArray)arg1, (object)arg2, (DoubleArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<double> {lvalue},_object*,PyImath::FixedArray<double>)
        
        __setitem__( (DoubleArray)arg1, (IntArray)arg2, (DoubleArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<double>)
        """
        pass

    def __sub__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<double> __sub__(PyImath::FixedArray<double> {lvalue},double)
        
        __sub__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<double> __sub__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __truediv__(self, DoubleArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (DoubleArray)arg1, (float)x) -> DoubleArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<double> __truediv__(PyImath::FixedArray<double> {lvalue},double)
        
        __truediv__( (DoubleArray)arg1, (DoubleArray)x) -> DoubleArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<double> __truediv__(PyImath::FixedArray<double> {lvalue},PyImath::FixedArray<double>)
        """
        pass

    __instance_size__ = 72


