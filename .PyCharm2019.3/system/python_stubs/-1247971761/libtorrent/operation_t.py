# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class operation_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    alloc_cache_piece = 34
    alloc_recvbuf = 5
    alloc_sndbuf = 6
    available = 14
    bittorrent = 1
    check_resume = 32
    connect = 16
    encryption = 15
    enum_if = 23
    exception = 33
    file = 9
    file_copy = 25
    file_fallocate = 26
    file_hard_link = 27
    file_open = 30
    file_read = 8
    file_remove = 28
    file_rename = 29
    file_stat = 24
    file_write = 7
    getname = 4
    getpeername = 3
    get_interface = 18
    hostname_lookup = 38
    iocontrol = 2
    mkdir = 31
    names = {
        'alloc_cache_piece': 34,
        'alloc_recvbuf': 5,
        'alloc_sndbuf': 6,
        'available': 14,
        'bittorrent': 1,
        'check_resume': 32,
        'connect': 16,
        'encryption': 15,
        'enum_if': 23,
        'exception': 33,
        'file': 9,
        'file_copy': 25,
        'file_fallocate': 26,
        'file_hard_link': 27,
        'file_open': 30,
        'file_read': 8,
        'file_remove': 28,
        'file_rename': 29,
        'file_stat': 24,
        'file_write': 7,
        'get_interface': 18,
        'getname': 4,
        'getpeername': 3,
        'hostname_lookup': 38,
        'iocontrol': 2,
        'mkdir': 31,
        'parse_address': 22,
        'partfile_move': 35,
        'partfile_read': 36,
        'partfile_write': 37,
        'sock_accept': 21,
        'sock_bind': 13,
        'sock_bind_to_device': 20,
        'sock_listen': 19,
        'sock_open': 12,
        'sock_read': 11,
        'sock_write': 10,
        'ssl_handshake': 17,
        'symlink': 39,
        'unknown': 0,
    }
    parse_address = 22
    partfile_move = 35
    partfile_read = 36
    partfile_write = 37
    sock_accept = 21
    sock_bind = 13
    sock_bind_to_device = 20
    sock_listen = 19
    sock_open = 12
    sock_read = 11
    sock_write = 10
    ssl_handshake = 17
    symlink = 39
    unknown = 0
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
        8: 8,
        9: 9,
        10: 10,
        11: 11,
        12: 12,
        13: 13,
        14: 14,
        15: 15,
        16: 16,
        17: 17,
        18: 18,
        19: 19,
        20: 20,
        21: 21,
        22: 22,
        23: 23,
        24: 24,
        25: 25,
        26: 26,
        27: 27,
        28: 28,
        29: 29,
        30: 30,
        31: 31,
        32: 32,
        33: 33,
        34: 34,
        35: 35,
        36: 36,
        37: 37,
        38: 38,
        39: 39,
    }
    __slots__ = ()


