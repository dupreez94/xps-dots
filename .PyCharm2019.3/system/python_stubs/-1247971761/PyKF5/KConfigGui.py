# encoding: utf-8
# module PyKF5.KConfigGui
# from /usr/lib/python3.8/site-packages/PyKF5/KConfigGui.so
# by generator 1.147
# no doc

# imports
import PyKF5.KConfigCore as __PyKF5_KConfigCore
import sip as __sip


# no functions
# classes

class KConfigGui(__sip.simplewrapper):
    # no doc
    def hasSessionConfig(self, *args, **kwargs): # real signature unknown
        pass

    def sessionConfig(self, *args, **kwargs): # real signature unknown
        pass

    def sessionConfigName(self, *args, **kwargs): # real signature unknown
        pass

    def setSessionConfig(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KConfigSkeleton(__PyKF5_KConfigCore.KCoreConfigSkeleton):
    # no doc
    def addItemColor(self, *args, **kwargs): # real signature unknown
        pass

    def addItemFont(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def usrRead(self, *args, **kwargs): # real signature unknown
        pass

    def usrReadConfig(self, *args, **kwargs): # real signature unknown
        pass

    def usrSave(self, *args, **kwargs): # real signature unknown
        pass

    def usrSetDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrUseDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrWriteConfig(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    ItemColor = None # (!) real value is "<class 'PyKF5.KConfigGui.KConfigSkeleton.ItemColor'>"
    ItemFont = None # (!) real value is "<class 'PyKF5.KConfigGui.KConfigSkeleton.ItemFont'>"


class KConfigLoader(KConfigSkeleton):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def findItem(self, *args, **kwargs): # real signature unknown
        pass

    def findItemByName(self, *args, **kwargs): # real signature unknown
        pass

    def groupList(self, *args, **kwargs): # real signature unknown
        pass

    def hasGroup(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def property(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def usrRead(self, *args, **kwargs): # real signature unknown
        pass

    def usrReadConfig(self, *args, **kwargs): # real signature unknown
        pass

    def usrSave(self, *args, **kwargs): # real signature unknown
        pass

    def usrSetDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrUseDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrWriteConfig(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KStandardShortcut(__sip.simplewrapper):
    # no doc
    def addBookmark(self, *args, **kwargs): # real signature unknown
        pass

    def back(self, *args, **kwargs): # real signature unknown
        pass

    def backwardWord(self, *args, **kwargs): # real signature unknown
        pass

    def begin(self, *args, **kwargs): # real signature unknown
        pass

    def beginningOfLine(self, *args, **kwargs): # real signature unknown
        pass

    def close(self, *args, **kwargs): # real signature unknown
        pass

    def completion(self, *args, **kwargs): # real signature unknown
        pass

    def copy(self, *args, **kwargs): # real signature unknown
        pass

    def cut(self, *args, **kwargs): # real signature unknown
        pass

    def deleteFile(self, *args, **kwargs): # real signature unknown
        pass

    def deleteWordBack(self, *args, **kwargs): # real signature unknown
        pass

    def deleteWordForward(self, *args, **kwargs): # real signature unknown
        pass

    def end(self, *args, **kwargs): # real signature unknown
        pass

    def endOfLine(self, *args, **kwargs): # real signature unknown
        pass

    def find(self, *args, **kwargs): # real signature unknown
        pass

    def findNext(self, *args, **kwargs): # real signature unknown
        pass

    def findPrev(self, *args, **kwargs): # real signature unknown
        pass

    def forward(self, *args, **kwargs): # real signature unknown
        pass

    def forwardWord(self, *args, **kwargs): # real signature unknown
        pass

    def fullScreen(self, *args, **kwargs): # real signature unknown
        pass

    def gotoLine(self, *args, **kwargs): # real signature unknown
        pass

    def hardcodedDefaultShortcut(self, *args, **kwargs): # real signature unknown
        pass

    def help(self, *args, **kwargs): # real signature unknown
        pass

    def home(self, *args, **kwargs): # real signature unknown
        pass

    def label(self, *args, **kwargs): # real signature unknown
        pass

    def moveToTrash(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def next(self, *args, **kwargs): # real signature unknown
        pass

    def nextCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def open(self, *args, **kwargs): # real signature unknown
        pass

    def openNew(self, *args, **kwargs): # real signature unknown
        pass

    def paste(self, *args, **kwargs): # real signature unknown
        pass

    def pasteSelection(self, *args, **kwargs): # real signature unknown
        pass

    def preferences(self, *args, **kwargs): # real signature unknown
        pass

    def prevCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def print(self, *args, **kwargs): # real signature unknown
        pass

    def prior(self, *args, **kwargs): # real signature unknown
        pass

    def quit(self, *args, **kwargs): # real signature unknown
        pass

    def redo(self, *args, **kwargs): # real signature unknown
        pass

    def reload(self, *args, **kwargs): # real signature unknown
        pass

    def renameFile(self, *args, **kwargs): # real signature unknown
        pass

    def replace(self, *args, **kwargs): # real signature unknown
        pass

    def rotateDown(self, *args, **kwargs): # real signature unknown
        pass

    def rotateUp(self, *args, **kwargs): # real signature unknown
        pass

    def save(self, *args, **kwargs): # real signature unknown
        pass

    def saveShortcut(self, *args, **kwargs): # real signature unknown
        pass

    def selectAll(self, *args, **kwargs): # real signature unknown
        pass

    def shortcut(self, *args, **kwargs): # real signature unknown
        pass

    def showMenubar(self, *args, **kwargs): # real signature unknown
        pass

    def substringCompletion(self, *args, **kwargs): # real signature unknown
        pass

    def tabNext(self, *args, **kwargs): # real signature unknown
        pass

    def tabPrev(self, *args, **kwargs): # real signature unknown
        pass

    def undo(self, *args, **kwargs): # real signature unknown
        pass

    def up(self, *args, **kwargs): # real signature unknown
        pass

    def whatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def zoomIn(self, *args, **kwargs): # real signature unknown
        pass

    def zoomOut(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AboutApp = 77
    AboutKDE = 78
    AccelNone = 0
    ActualSize = 56
    AddBookmark = 35
    Back = 27
    BackwardWord = 33
    Begin = 22
    BeginningOfLine = 30
    Clear = 55
    Close = 3
    ConfigureNotifications = 73
    ConfigureToolbars = 72
    Copy = 10
    Cut = 9
    DeleteFile = 79
    DeleteWordBack = 15
    DeleteWordForward = 16
    Deselect = 14
    DocumentBack = 63
    DocumentForward = 64
    Donate = 82
    EditBookmarks = 65
    End = 23
    EndOfLine = 31
    Find = 17
    FindNext = 18
    FindPrev = 19
    FitToHeight = 59
    FitToPage = 57
    FitToWidth = 58
    Forward = 28
    ForwardWord = 34
    FullScreen = 38
    Goto = 61
    GotoLine = 32
    GotoPage = 62
    Help = 42
    Home = 21
    KeyBindings = 70
    Mail = 54
    MoveToTrash = 81
    New = 2
    Next = 25
    NextCompletion = 46
    Open = 1
    OpenRecent = 50
    Paste = 11
    PasteSelection = 12
    Preferences = 71
    PrevCompletion = 45
    Print = 5
    PrintPreview = 53
    Prior = 24
    Quit = 6
    Redo = 8
    Reload = 29
    RenameFile = 80
    Replace = 20
    ReportBug = 75
    Revert = 52
    RotateDown = 49
    RotateUp = 48
    Save = 4
    SaveAs = 51
    SaveOptions = 69
    SelectAll = 13
    ShowMenubar = 39
    ShowStatusbar = 68
    ShowToolbar = 67
    Spelling = 66
    StandardShortcut = None # (!) real value is "<class 'PyKF5.KConfigGui.KStandardShortcut.StandardShortcut'>"
    StandardShortcutCount = 83
    SubstringCompletion = 47
    SwitchApplicationLanguage = 76
    TabNext = 40
    TabPrev = 41
    TextCompletion = 44
    TipofDay = 74
    Undo = 7
    Up = 26
    WhatsThis = 43
    Zoom = 60
    ZoomIn = 36
    ZoomOut = 37


class KWindowConfig(__sip.simplewrapper):
    # no doc
    def restoreWindowSize(self, *args, **kwargs): # real signature unknown
        pass

    def saveWindowSize(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KConfigGui', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KConfigGui.so')"

