# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorFilter import QSensorFilter

class QLightFilter(QSensorFilter):
    """
    QLightFilter()
    QLightFilter(QLightFilter)
    """
    def filter(self, QLightReading): # real signature unknown; restored from __doc__
        """ filter(self, QLightReading) -> bool """
        return False

    def __init__(self, QLightFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


