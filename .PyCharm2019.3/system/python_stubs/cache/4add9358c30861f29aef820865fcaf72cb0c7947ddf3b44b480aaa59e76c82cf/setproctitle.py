# encoding: utf-8
# module setproctitle
# from /usr/lib/python3.8/site-packages/setproctitle.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Allow customization of the process title. """
# no imports

# Variables with simple values

__version__ = '1.1.10'

# functions

def getproctitle(): # real signature unknown; restored from __doc__
    """ getproctitle() -- Get the current process title. """
    pass

def setproctitle(title): # real signature unknown; restored from __doc__
    """ setproctitle(title) -- Change the process title. """
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='setproctitle', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/setproctitle.cpython-38-x86_64-linux-gnu.so')"

