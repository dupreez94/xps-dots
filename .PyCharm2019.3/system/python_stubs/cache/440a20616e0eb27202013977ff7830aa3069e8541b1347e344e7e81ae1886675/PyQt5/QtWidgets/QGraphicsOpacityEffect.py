# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QGraphicsEffect import QGraphicsEffect

class QGraphicsOpacityEffect(QGraphicsEffect):
    """ QGraphicsOpacityEffect(parent: QObject = None) """
    def draw(self, QPainter): # real signature unknown; restored from __doc__
        """ draw(self, QPainter) """
        pass

    def opacity(self): # real signature unknown; restored from __doc__
        """ opacity(self) -> float """
        return 0.0

    def opacityChanged(self, p_float): # real signature unknown; restored from __doc__
        """ opacityChanged(self, float) [signal] """
        pass

    def opacityMask(self): # real signature unknown; restored from __doc__
        """ opacityMask(self) -> QBrush """
        pass

    def opacityMaskChanged(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ opacityMaskChanged(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) [signal] """
        pass

    def setOpacity(self, p_float): # real signature unknown; restored from __doc__
        """ setOpacity(self, float) """
        pass

    def setOpacityMask(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setOpacityMask(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


