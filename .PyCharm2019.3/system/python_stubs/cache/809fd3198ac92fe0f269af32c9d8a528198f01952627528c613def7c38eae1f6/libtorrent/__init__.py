# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


# Variables with simple values

create_smart_ban_plugin = 'smart_ban'

create_ut_metadata_plugin = 'ut_metadata'

create_ut_pex_plugin = 'ut_pex'

version = '1.2.3.0'

version_major = 1
version_minor = 2

__version__ = '1.2.3.0'

# functions

def add_files(file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    add_files( (file_storage)fs, (str)path [, (object)flags=0]) -> None :
    
        C++ signature :
            void add_files(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::create_flags_tag, void>=0])
    
    add_files( (file_storage)fs, (str)path, (object)predicate [, (object)flags=0]) -> None :
    
        C++ signature :
            void add_files(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::api::object [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::create_flags_tag, void>=0])
    """
    pass

def add_magnet_uri(session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    add_magnet_uri( (session)arg1, (str)arg2, (dict)arg3) -> torrent_handle :
    
        C++ signature :
            libtorrent::torrent_handle add_magnet_uri(libtorrent::session {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::dict)
    """
    pass

def bdecode(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    bdecode( (object)arg1) -> object :
    
        C++ signature :
            libtorrent::entry bdecode(bytes)
    """
    pass

def bdecode_category(): # real signature unknown; restored from __doc__
    """
    bdecode_category() -> error_category :
    
        C++ signature :
            category_holder bdecode_category()
    """
    return error_category

def bencode(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    bencode( (object)arg1) -> object :
    
        C++ signature :
            bytes bencode(libtorrent::entry)
    """
    pass

def client_fingerprint(sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    client_fingerprint( (sha1_hash)arg1) -> object :
    
        C++ signature :
            boost::python::api::object client_fingerprint(libtorrent::digest32<160l>)
    """
    pass

def default_settings(): # real signature unknown; restored from __doc__
    """
    default_settings() -> dict :
    
        C++ signature :
            boost::python::dict default_settings()
    """
    return {}

def find_metric_idx(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    find_metric_idx( (str)arg1) -> int :
    
        C++ signature :
            int find_metric_idx(char const*)
    """
    pass

def generate_fingerprint(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    generate_fingerprint( (str)arg1, (int)arg2, (int)arg3, (int)arg4, (int)arg5) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > generate_fingerprint(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,int,int,int,int)
    """
    pass

def generic_category(): # real signature unknown; restored from __doc__
    """
    generic_category() -> error_category :
    
        C++ signature :
            category_holder generic_category()
    """
    return error_category

def get_bdecode_category(): # real signature unknown; restored from __doc__
    """
    get_bdecode_category() -> error_category :
    
        C++ signature :
            category_holder get_bdecode_category()
    """
    return error_category

def get_http_category(): # real signature unknown; restored from __doc__
    """
    get_http_category() -> error_category :
    
        C++ signature :
            category_holder get_http_category()
    """
    return error_category

def get_i2p_category(): # real signature unknown; restored from __doc__
    """
    get_i2p_category() -> error_category :
    
        C++ signature :
            category_holder get_i2p_category()
    """
    return error_category

def get_libtorrent_category(): # real signature unknown; restored from __doc__
    """
    get_libtorrent_category() -> error_category :
    
        C++ signature :
            category_holder get_libtorrent_category()
    """
    return error_category

def get_socks_category(): # real signature unknown; restored from __doc__
    """
    get_socks_category() -> error_category :
    
        C++ signature :
            category_holder get_socks_category()
    """
    return error_category

def get_upnp_category(): # real signature unknown; restored from __doc__
    """
    get_upnp_category() -> error_category :
    
        C++ signature :
            category_holder get_upnp_category()
    """
    return error_category

def high_performance_seed(): # real signature unknown; restored from __doc__
    """
    high_performance_seed() -> dict :
    
        C++ signature :
            boost::python::dict high_performance_seed()
    """
    return {}

def http_category(): # real signature unknown; restored from __doc__
    """
    http_category() -> error_category :
    
        C++ signature :
            category_holder http_category()
    """
    return error_category

def i2p_category(): # real signature unknown; restored from __doc__
    """
    i2p_category() -> error_category :
    
        C++ signature :
            category_holder i2p_category()
    """
    return error_category

def identify_client(sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    identify_client( (sha1_hash)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > identify_client(libtorrent::digest32<160l>)
    """
    pass

def libtorrent_category(): # real signature unknown; restored from __doc__
    """
    libtorrent_category() -> error_category :
    
        C++ signature :
            category_holder libtorrent_category()
    """
    return error_category

def make_magnet_uri(torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    make_magnet_uri( (torrent_handle)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > make_magnet_uri(libtorrent::torrent_handle)
    
    make_magnet_uri( (torrent_info)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > make_magnet_uri(libtorrent::torrent_info)
    """
    pass

def min_memory_usage(): # real signature unknown; restored from __doc__
    """
    min_memory_usage() -> dict :
    
        C++ signature :
            boost::python::dict min_memory_usage()
    """
    return {}

def operation_name(operation_t, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    operation_name( (operation_t)arg1) -> str :
    
        C++ signature :
            char const* operation_name(libtorrent::operation_t)
    """
    pass

def parse_magnet_uri(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    parse_magnet_uri( (str)arg1) -> add_torrent_params :
    
        C++ signature :
            libtorrent::add_torrent_params parse_magnet_uri(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    """
    pass

def parse_magnet_uri_dict(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    parse_magnet_uri_dict( (str)arg1) -> dict :
    
        C++ signature :
            boost::python::dict parse_magnet_uri_dict(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    """
    pass

def read_resume_data(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    read_resume_data( (object)arg1) -> add_torrent_params :
    
        C++ signature :
            libtorrent::add_torrent_params read_resume_data(bytes)
    """
    pass

def session_stats_metrics(): # real signature unknown; restored from __doc__
    """
    session_stats_metrics() -> object :
    
        C++ signature :
            std::vector<libtorrent::stats_metric, std::allocator<libtorrent::stats_metric> > session_stats_metrics()
    """
    return object()

def set_piece_hashes(create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    set_piece_hashes( (create_torrent)arg1, (str)arg2) -> None :
    
        C++ signature :
            void set_piece_hashes(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    
    set_piece_hashes( (create_torrent)arg1, (str)arg2, (object)arg3) -> None :
    
        C++ signature :
            void set_piece_hashes(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::api::object)
    """
    pass

def socks_category(): # real signature unknown; restored from __doc__
    """
    socks_category() -> error_category :
    
        C++ signature :
            category_holder socks_category()
    """
    return error_category

def system_category(): # real signature unknown; restored from __doc__
    """
    system_category() -> error_category :
    
        C++ signature :
            category_holder system_category()
    """
    return error_category

def upnp_category(): # real signature unknown; restored from __doc__
    """
    upnp_category() -> error_category :
    
        C++ signature :
            category_holder upnp_category()
    """
    return error_category

def write_resume_data(add_torrent_params, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    write_resume_data( (add_torrent_params)arg1) -> object :
    
        C++ signature :
            libtorrent::entry write_resume_data(libtorrent::add_torrent_params)
    """
    pass

def write_resume_data_buf(add_torrent_params, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    write_resume_data_buf( (add_torrent_params)arg1) -> object :
    
        C++ signature :
            bytes write_resume_data_buf(libtorrent::add_torrent_params)
    """
    pass

# classes

from .add_piece_flags_t import add_piece_flags_t
from .alert import alert
from .torrent_alert import torrent_alert
from .add_torrent_alert import add_torrent_alert
from .add_torrent_params import add_torrent_params
from .add_torrent_params_flags_t import add_torrent_params_flags_t
from .alerts_dropped_alert import alerts_dropped_alert
from .announce_entry import announce_entry
from .anonymous_mode_alert import anonymous_mode_alert
from .bandwidth_mixed_algo_t import bandwidth_mixed_algo_t
from .peer_alert import peer_alert
from .block_downloading_alert import block_downloading_alert
from .block_finished_alert import block_finished_alert
from .block_timeout_alert import block_timeout_alert
from .block_uploaded_alert import block_uploaded_alert
from .cache_flushed_alert import cache_flushed_alert
from .cache_status import cache_status
from .choking_algorithm_t import choking_algorithm_t
from .create_torrent import create_torrent
from .create_torrent_flags_t import create_torrent_flags_t
from .deadline_flags_t import deadline_flags_t
from .deprecated_move_flags_t import deprecated_move_flags_t
from .dht_announce_alert import dht_announce_alert
from .dht_get_peers_alert import dht_get_peers_alert
from .dht_get_peers_reply_alert import dht_get_peers_reply_alert
from .dht_immutable_item_alert import dht_immutable_item_alert
from .dht_log_alert import dht_log_alert
from .dht_lookup import dht_lookup
from .dht_mutable_item_alert import dht_mutable_item_alert
from .dht_outgoing_get_peers_alert import dht_outgoing_get_peers_alert
from .dht_pkt_alert import dht_pkt_alert
from .dht_put_alert import dht_put_alert
from .tracker_alert import tracker_alert
from .dht_reply_alert import dht_reply_alert
from .dht_settings import dht_settings
from .dht_stats_alert import dht_stats_alert
from .enc_level import enc_level
from .enc_policy import enc_policy
from .error_category import error_category
from .error_code import error_code
from .external_ip_alert import external_ip_alert
from .fastresume_rejected_alert import fastresume_rejected_alert
from .file_completed_alert import file_completed_alert
from .file_entry import file_entry
from .file_error_alert import file_error_alert
from .file_flags_t import file_flags_t
from .file_open_mode import file_open_mode
from .file_progress_flags import file_progress_flags
from .file_renamed_alert import file_renamed_alert
from .file_rename_failed_alert import file_rename_failed_alert
from .file_slice import file_slice
from .file_storage import file_storage
from .fingerprint import fingerprint
from .hash_failed_alert import hash_failed_alert
from .i2p_alert import i2p_alert
from .incoming_connection_alert import incoming_connection_alert
from .invalid_request_alert import invalid_request_alert
from .io_buffer_mode_t import io_buffer_mode_t
from .ip_filter import ip_filter
from .kind import kind
from .listen_failed_alert import listen_failed_alert
from .listen_failed_alert_socket_type_t import listen_failed_alert_socket_type_t
from .listen_on_flags_t import listen_on_flags_t
from .listen_succeded_alert_socket_type_t import listen_succeded_alert_socket_type_t
from .listen_succeeded_alert import listen_succeeded_alert
from .log_alert import log_alert
from .lsd_error_alert import lsd_error_alert
from .metadata_failed_alert import metadata_failed_alert
from .metadata_received_alert import metadata_received_alert
from .metric_type_t import metric_type_t
from .move_flags_t import move_flags_t
from .open_file_state import open_file_state
from .operation_t import operation_t
from .options_t import options_t
from .pause_flags_t import pause_flags_t
from .peer_ban_alert import peer_ban_alert
from .peer_blocked_alert import peer_blocked_alert
from .peer_class_type_filter import peer_class_type_filter
from .peer_class_type_filter_socket_type_t import peer_class_type_filter_socket_type_t
from .peer_connect_alert import peer_connect_alert
from .peer_disconnected_alert import peer_disconnected_alert
from .peer_error_alert import peer_error_alert
from .sha1_hash import sha1_hash
from .peer_id import peer_id
from .peer_info import peer_info
from .peer_log_alert import peer_log_alert
from .peer_request import peer_request
from .peer_snubbed_alert import peer_snubbed_alert
from .peer_unsnubbed_alert import peer_unsnubbed_alert
from .performance_alert import performance_alert
from .performance_warning_t import performance_warning_t
from .pe_settings import pe_settings
from .picker_log_alert import picker_log_alert
from .piece_finished_alert import piece_finished_alert
from .portmap_alert import portmap_alert
from .portmap_error_alert import portmap_error_alert
from .portmap_log_alert import portmap_log_alert
from .portmap_protocol import portmap_protocol
from .portmap_transport import portmap_transport
from .protocol_type import protocol_type
from .proxy_type_t import proxy_type_t
from .read_piece_alert import read_piece_alert
from .reannounce_flags_t import reannounce_flags_t
from .reason_t import reason_t
from .request_dropped_alert import request_dropped_alert
from .save_resume_data_alert import save_resume_data_alert
from .save_resume_data_failed_alert import save_resume_data_failed_alert
from .save_resume_flags_t import save_resume_flags_t
from .save_state_flags_t import save_state_flags_t
from .scrape_failed_alert import scrape_failed_alert
from .scrape_reply_alert import scrape_reply_alert
from .seed_choking_algorithm_t import seed_choking_algorithm_t
from .session import session
from .session_flags_t import session_flags_t
from .session_stats_alert import session_stats_alert
from .session_stats_header_alert import session_stats_header_alert
from .session_status import session_status
from .socket_type_t import socket_type_t
from .state_changed_alert import state_changed_alert
from .state_update_alert import state_update_alert
from .stats_alert import stats_alert
from .stats_channel import stats_channel
from .stats_metric import stats_metric
from .status_flags_t import status_flags_t
from .storage_mode_t import storage_mode_t
from .storage_moved_alert import storage_moved_alert
from .storage_moved_failed_alert import storage_moved_failed_alert
from .suggest_mode_t import suggest_mode_t
from .torrent_added_alert import torrent_added_alert
from .torrent_checked_alert import torrent_checked_alert
from .torrent_deleted_alert import torrent_deleted_alert
from .torrent_delete_failed_alert import torrent_delete_failed_alert
from .torrent_error_alert import torrent_error_alert
from .torrent_finished_alert import torrent_finished_alert
from .torrent_flags import torrent_flags
from .torrent_handle import torrent_handle
from .torrent_info import torrent_info
from .torrent_log_alert import torrent_log_alert
from .torrent_need_cert_alert import torrent_need_cert_alert
from .torrent_paused_alert import torrent_paused_alert
from .torrent_removed_alert import torrent_removed_alert
from .torrent_resumed_alert import torrent_resumed_alert
from .torrent_status import torrent_status
from .torrent_update_alert import torrent_update_alert
from .tracker_announce_alert import tracker_announce_alert
from .tracker_error_alert import tracker_error_alert
from .tracker_reply_alert import tracker_reply_alert
from .tracker_source import tracker_source
from .tracker_warning_alert import tracker_warning_alert
from .udp_error_alert import udp_error_alert
from .unwanted_block_alert import unwanted_block_alert
from .url_seed_alert import url_seed_alert
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='libtorrent', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so')"

