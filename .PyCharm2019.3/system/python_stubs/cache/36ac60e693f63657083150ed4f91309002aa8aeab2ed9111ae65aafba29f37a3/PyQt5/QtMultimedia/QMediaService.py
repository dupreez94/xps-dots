# encoding: utf-8
# module PyQt5.QtMultimedia
# from /usr/lib/python3.8/site-packages/PyQt5/QtMultimedia.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QMediaService(__PyQt5_QtCore.QObject):
    """ QMediaService(QObject) """
    def releaseControl(self, QMediaControl): # real signature unknown; restored from __doc__
        """ releaseControl(self, QMediaControl) """
        pass

    def requestControl(self, p_str): # real signature unknown; restored from __doc__
        """ requestControl(self, str) -> QMediaControl """
        return QMediaControl

    def __init__(self, QObject): # real signature unknown; restored from __doc__
        pass


