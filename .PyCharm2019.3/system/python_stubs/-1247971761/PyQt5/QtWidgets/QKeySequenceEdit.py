# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QWidget import QWidget

class QKeySequenceEdit(QWidget):
    """
    QKeySequenceEdit(parent: QWidget = None)
    QKeySequenceEdit(Union[QKeySequence, QKeySequence.StandardKey, str, int], parent: QWidget = None)
    """
    def clear(self): # real signature unknown; restored from __doc__
        """ clear(self) """
        pass

    def editingFinished(self): # real signature unknown; restored from __doc__
        """ editingFinished(self) [signal] """
        pass

    def event(self, QEvent): # real signature unknown; restored from __doc__
        """ event(self, QEvent) -> bool """
        return False

    def keyPressEvent(self, QKeyEvent): # real signature unknown; restored from __doc__
        """ keyPressEvent(self, QKeyEvent) """
        pass

    def keyReleaseEvent(self, QKeyEvent): # real signature unknown; restored from __doc__
        """ keyReleaseEvent(self, QKeyEvent) """
        pass

    def keySequence(self): # real signature unknown; restored from __doc__
        """ keySequence(self) -> QKeySequence """
        pass

    def keySequenceChanged(self, Union, QKeySequence=None, QKeySequence_StandardKey=None, p_str=None, p_int=None): # real signature unknown; restored from __doc__
        """ keySequenceChanged(self, Union[QKeySequence, QKeySequence.StandardKey, str, int]) [signal] """
        pass

    def setKeySequence(self, Union, QKeySequence=None, QKeySequence_StandardKey=None, p_str=None, p_int=None): # real signature unknown; restored from __doc__
        """ setKeySequence(self, Union[QKeySequence, QKeySequence.StandardKey, str, int]) """
        pass

    def timerEvent(self, QTimerEvent): # real signature unknown; restored from __doc__
        """ timerEvent(self, QTimerEvent) """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


