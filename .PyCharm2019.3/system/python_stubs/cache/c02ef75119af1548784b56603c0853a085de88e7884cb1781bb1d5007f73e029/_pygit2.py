# encoding: utf-8
# module _pygit2
# from /usr/lib/python3.8/site-packages/_pygit2.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Python bindings for libgit2. """
# no imports

# Variables with simple values

GIT_BLAME_NORMAL = 0

GIT_BLAME_TRACK_COPIES_ANY_COMMIT_COPIES = 8

GIT_BLAME_TRACK_COPIES_SAME_COMMIT_COPIES = 4
GIT_BLAME_TRACK_COPIES_SAME_COMMIT_MOVES = 2

GIT_BLAME_TRACK_COPIES_SAME_FILE = 1

GIT_BRANCH_ALL = 3
GIT_BRANCH_LOCAL = 1
GIT_BRANCH_REMOTE = 2

GIT_CHECKOUT_ALLOW_CONFLICTS = 16

GIT_CHECKOUT_DISABLE_PATHSPEC_MATCH = 8192

GIT_CHECKOUT_DONT_UPDATE_INDEX = 256

GIT_CHECKOUT_FORCE = 2
GIT_CHECKOUT_NONE = 0

GIT_CHECKOUT_NO_REFRESH = 512

GIT_CHECKOUT_RECREATE_MISSING = 4

GIT_CHECKOUT_REMOVE_IGNORED = 64
GIT_CHECKOUT_REMOVE_UNTRACKED = 32

GIT_CHECKOUT_SAFE = 1

GIT_CHECKOUT_UPDATE_ONLY = 128

GIT_CONFIG_LEVEL_GLOBAL = 4
GIT_CONFIG_LEVEL_LOCAL = 5
GIT_CONFIG_LEVEL_SYSTEM = 2
GIT_CONFIG_LEVEL_XDG = 3

GIT_DELTA_ADDED = 1
GIT_DELTA_COPIED = 5
GIT_DELTA_DELETED = 2
GIT_DELTA_IGNORED = 6
GIT_DELTA_MODIFIED = 3
GIT_DELTA_RENAMED = 4
GIT_DELTA_TYPECHANGE = 8
GIT_DELTA_UNMODIFIED = 0
GIT_DELTA_UNREADABLE = 9
GIT_DELTA_UNTRACKED = 7

GIT_DESCRIBE_ALL = 2
GIT_DESCRIBE_DEFAULT = 0
GIT_DESCRIBE_TAGS = 1

GIT_DIFF_DISABLE_PATHSPEC_MATCH = 4096

GIT_DIFF_FIND_AND_BREAK_REWRITES = 48

GIT_DIFF_FIND_COPIES = 4

GIT_DIFF_FIND_COPIES_FROM_UNMODIFIED = 8

GIT_DIFF_FIND_RENAMES = 1

GIT_DIFF_FIND_RENAMES_FROM_REWRITES = 2

GIT_DIFF_FLAG_BINARY = 1

GIT_DIFF_FLAG_NOT_BINARY = 2

GIT_DIFF_FLAG_VALID_ID = 4

GIT_DIFF_FORCE_TEXT = 1048576

GIT_DIFF_IGNORE_CASE = 1024
GIT_DIFF_IGNORE_SUBMODULES = 512
GIT_DIFF_IGNORE_WHITESPACE = 4194304

GIT_DIFF_IGNORE_WHITESPACE_CHANGE = 8388608
GIT_DIFF_IGNORE_WHITESPACE_EOL = 16777216

GIT_DIFF_INCLUDE_IGNORED = 2
GIT_DIFF_INCLUDE_TYPECHANGE = 64

GIT_DIFF_INCLUDE_TYPECHANGE_TREES = 128

GIT_DIFF_INCLUDE_UNMODIFIED = 32
GIT_DIFF_INCLUDE_UNTRACKED = 8

GIT_DIFF_MINIMAL = 536870912
GIT_DIFF_NORMAL = 0
GIT_DIFF_PATIENCE = 268435456

GIT_DIFF_RECURSE_IGNORED_DIRS = 4

GIT_DIFF_RECURSE_UNTRACKED_DIRS = 16

GIT_DIFF_REVERSE = 1

GIT_DIFF_SHOW_BINARY = 1073741824

GIT_DIFF_SHOW_UNTRACKED_CONTENT = 33554432

GIT_DIFF_SKIP_BINARY_CHECK = 8192

GIT_DIFF_STATS_FULL = 1

GIT_DIFF_STATS_INCLUDE_SUMMARY = 8

GIT_DIFF_STATS_NONE = 0
GIT_DIFF_STATS_NUMBER = 4
GIT_DIFF_STATS_SHORT = 2

GIT_FILEMODE_BLOB = 33188

GIT_FILEMODE_BLOB_EXECUTABLE = 33261

GIT_FILEMODE_COMMIT = 57344
GIT_FILEMODE_LINK = 40960
GIT_FILEMODE_TREE = 16384

GIT_MERGE_ANALYSIS_FASTFORWARD = 4
GIT_MERGE_ANALYSIS_NONE = 0
GIT_MERGE_ANALYSIS_NORMAL = 1
GIT_MERGE_ANALYSIS_UNBORN = 8

GIT_MERGE_ANALYSIS_UP_TO_DATE = 2

GIT_OBJ_ANY = -2
GIT_OBJ_BLOB = 3
GIT_OBJ_COMMIT = 1
GIT_OBJ_TAG = 4
GIT_OBJ_TREE = 2

GIT_OID_HEXSZ = 40

GIT_OID_HEX_ZERO = '0000000000000000000000000000000000000000'

GIT_OID_MINPREFIXLEN = 4
GIT_OID_RAWSZ = 20

GIT_OPT_ENABLE_CACHING = 8

GIT_OPT_ENABLE_FSYNC_GITDIR = 19

GIT_OPT_ENABLE_OFS_DELTA = 18

GIT_OPT_ENABLE_STRICT_HASH_VERIFICATION = 22

GIT_OPT_ENABLE_STRICT_OBJECT_CREATION = 14

GIT_OPT_ENABLE_STRICT_SYMBOLIC_REF_CREATION = 15

GIT_OPT_ENABLE_UNSAVED_INDEX_SAFETY = 24

GIT_OPT_GET_CACHED_MEMORY = 9

GIT_OPT_GET_MWINDOW_MAPPED_LIMIT = 2

GIT_OPT_GET_MWINDOW_SIZE = 0

GIT_OPT_GET_PACK_MAX_OBJECTS = 25

GIT_OPT_GET_SEARCH_PATH = 4

GIT_OPT_GET_TEMPLATE_PATH = 10

GIT_OPT_GET_USER_AGENT = 17

GIT_OPT_GET_WINDOWS_SHAREMODE = 20

GIT_OPT_SET_ALLOCATOR = 23

GIT_OPT_SET_CACHE_MAX_SIZE = 7

GIT_OPT_SET_CACHE_OBJECT_LIMIT = 6

GIT_OPT_SET_MWINDOW_MAPPED_LIMIT = 3

GIT_OPT_SET_MWINDOW_SIZE = 1

GIT_OPT_SET_PACK_MAX_OBJECTS = 26

GIT_OPT_SET_SEARCH_PATH = 5

GIT_OPT_SET_SSL_CERT_LOCATIONS = 12

GIT_OPT_SET_SSL_CIPHERS = 16

GIT_OPT_SET_TEMPLATE_PATH = 11

GIT_OPT_SET_USER_AGENT = 13

GIT_OPT_SET_WINDOWS_SHAREMODE = 21

GIT_REF_INVALID = 0
GIT_REF_LISTALL = 3
GIT_REF_OID = 1
GIT_REF_SYMBOLIC = 2

GIT_RESET_HARD = 3
GIT_RESET_MIXED = 2
GIT_RESET_SOFT = 1

GIT_SORT_NONE = 0
GIT_SORT_REVERSE = 4
GIT_SORT_TIME = 2
GIT_SORT_TOPOLOGICAL = 1

GIT_STASH_APPLY_DEFAULT = 0

GIT_STASH_APPLY_REINSTATE_INDEX = 1

GIT_STASH_DEFAULT = 0

GIT_STASH_INCLUDE_IGNORED = 4
GIT_STASH_INCLUDE_UNTRACKED = 2

GIT_STASH_KEEP_INDEX = 1

GIT_STATUS_CONFLICTED = 32768
GIT_STATUS_CURRENT = 0
GIT_STATUS_IGNORED = 16384

GIT_STATUS_INDEX_DELETED = 4
GIT_STATUS_INDEX_MODIFIED = 2
GIT_STATUS_INDEX_NEW = 1
GIT_STATUS_INDEX_RENAMED = 8
GIT_STATUS_INDEX_TYPECHANGE = 16

GIT_STATUS_WT_DELETED = 512
GIT_STATUS_WT_MODIFIED = 256
GIT_STATUS_WT_NEW = 128
GIT_STATUS_WT_RENAMED = 2048
GIT_STATUS_WT_TYPECHANGE = 1024
GIT_STATUS_WT_UNREADABLE = 4096

LIBGIT2_VERSION = '0.28.3'

LIBGIT2_VER_MAJOR = 0
LIBGIT2_VER_MINOR = 28
LIBGIT2_VER_REVISION = 3

# functions

def discover_repository(path, across_fs=None, ceiling_dirs=None): # real signature unknown; restored from __doc__
    """
    discover_repository(path[, across_fs[, ceiling_dirs]]) -> str
    
    Look for a git repository and return its path. If not found returns None.
    """
    return ""

def hash(data): # real signature unknown; restored from __doc__
    """
    hash(data) -> Oid
    
    Returns the oid of a new blob from a string without actually writing to
    the odb.
    """
    return Oid

def hashfile(path): # real signature unknown; restored from __doc__
    """
    hashfile(path) -> Oid
    
    Returns the oid of a new blob from a file path without actually writing
    to the odb.
    """
    return Oid

def init_file_backend(path): # real signature unknown; restored from __doc__
    """
    init_file_backend(path) -> object
    
    open repo backend given path.
    """
    return object()

def option(option, *more): # real signature unknown; restored from __doc__
    """
    option(option, ...)
    
    Get or set a libgit2 option.
    
    Parameters:
    
    GIT_OPT_GET_SEARCH_PATH, level
        Get the config search path for the given level.
    
    GIT_OPT_SET_SEARCH_PATH, level, path
        Set the config search path for the given level.
    
    GIT_OPT_GET_MWINDOW_SIZE
        Get the maximum mmap window size.
    
    GIT_OPT_SET_MWINDOW_SIZE, size
        Set the maximum mmap window size.
    """
    pass

def reference_is_valid_name(refname): # real signature unknown; restored from __doc__
    """
    reference_is_valid_name(refname) -> bool
    
    Check if the passed string is a valid reference name.
    """
    return False

# classes

class AlreadyExistsError(ValueError):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class Object(object):
    """ Base class for Git objects. """
    def peel(self, target_type): # real signature unknown; restored from __doc__
        """
        peel(target_type) -> Object
        
        Peel the current object and returns the first object of the given type
        """
        return Object

    def read_raw(self): # real signature unknown; restored from __doc__
        """
        read_raw()
        
        Returns the byte string with the raw contents of the object.
        """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __hash__(self, *args, **kwargs): # real signature unknown
        """ Return hash(self). """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    hex = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Hexadecimal representation of the object id. This is a shortcut for
Object.oid.hex
This attribute is deprecated, please use 'id'
"""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The object id, an instance of the Oid type."""

    oid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The object id, an instance of the Oid type.
This attribute is deprecated, please use 'id'
"""

    short_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """An unambiguous short (abbreviated) hex Oid string for the object."""

    type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """One of the GIT_OBJ_COMMIT, GIT_OBJ_TREE, GIT_OBJ_BLOB or GIT_OBJ_TAG
constants."""

    _pointer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Get the object's pointer. For internal use only."""



class Blob(Object):
    """
    Blob object.
    
    Blobs implement the buffer interface, which means you can get access
    to its data via `memoryview(blob)` without the need to create a copy.
    """
    def diff(self, blob=None, flag=None, old_as_path=None, new_as_path=None): # real signature unknown; restored from __doc__
        """
        diff([blob, flag, old_as_path, new_as_path])
        
        Directly generate a :py:class:`pygit2.Patch` from the difference
        between two blobs.
        
        Returns: Patch.
        
        Parameters:
        
        blob : Blob
            The :py:class:`~pygit2.Blob` to diff.
        
        flag
            A GIT_DIFF_* constant.
        
        old_as_path : str
            Treat old blob as if it had this filename.
        
        new_as_path : str
            Treat new blob as if it had this filename.
        """
        pass

    def diff_to_buffer(self, buffer=None, flag=None, old_as_path=None, buffer_as_path=None): # real signature unknown; restored from __doc__
        """
        diff_to_buffer([buffer, flag, old_as_path, buffer_as_path])
        
        Directly generate a :py:class:`~pygit2.Patch` from the difference
        between a blob and a buffer.
        
        Returns: Patch.
        
        Parameters:
        
        buffer : Blob
            Raw data for new side of diff.
        
        flag
            A GIT_DIFF_* constant.
        
        old_as_path : str
            Treat old blob as if it had this filename.
        
        buffer_as_path : str
            Treat buffer as if it had this filename.
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    data = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The contents of the blob, a bytes string. This is the same as
Blob.read_raw()"""

    is_binary = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """True if binary data, False if not."""

    size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Size."""



class Reference(object):
    """ Reference. """
    def delete(self): # real signature unknown; restored from __doc__
        """
        delete()
        
        Delete this reference. It will no longer be valid!
        """
        pass

    def log(self): # real signature unknown; restored from __doc__
        """
        log() -> RefLogIter
        
        Retrieves the current reference log.
        """
        pass

    def peel(self, type=None): # real signature unknown; restored from __doc__
        """
        peel(type=None) -> object
        
        Retrieve an object of the given type by recursive peeling.
        
        If no type is provided, the first non-tag object will be returned.
        """
        return object()

    def rename(self, new_name): # real signature unknown; restored from __doc__
        """
        rename(new_name)
        
        Rename the reference.
        """
        pass

    def resolve(self): # real signature unknown; restored from __doc__
        """
        resolve() -> Reference
        
        Resolve a symbolic reference and return a direct reference.
        """
        return Reference

    def set_target(self, target, message=None): # real signature unknown; restored from __doc__
        """
        set_target(target, [message])
        
        Set the target of this reference.
        
        Update the reference using the given signature and message.
        These will be used to fill the reflog entry which will be created
        as a result of this update.
        
        Parameters:
        
        target
            The new target for this reference
        
        message
            Message to use for the reflog.
        """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The full name of the reference."""

    raw_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The full name of the reference (Bytes)."""

    raw_shorthand = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The shorthand "human-readable" name of the reference (Bytes)."""

    shorthand = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The shorthand "human-readable" name of the reference."""

    target = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The reference target: If direct the value will be an Oid object, if it
is symbolic it will be an string with the full name of the target
reference.
"""

    type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Type, either GIT_REF_OID or GIT_REF_SYMBOLIC."""


    __hash__ = None


class Branch(Reference):
    """ Branch. """
    def delete(self): # real signature unknown; restored from __doc__
        """
        delete()
        
        Delete this branch. It will no longer be valid!
        """
        pass

    def is_checked_out(self): # real signature unknown; restored from __doc__
        """
        is_checked_out()
        
        True if branch is checked out by any repo connected to the current one,  False otherwise.
        """
        pass

    def is_head(self): # real signature unknown; restored from __doc__
        """
        is_head()
        
        True if HEAD points at the branch, False otherwise.
        """
        pass

    def rename(self, name, force=False): # real signature unknown; restored from __doc__
        """
        rename(name, force=False)
        
        Move/rename an existing local branch reference. The new branch name will be checked for validity.
        Returns the new branch.
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    branch_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The name of the local or remote branch."""

    remote_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The name of the remote set to be the upstream of this branch."""

    upstream = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The branch's upstream branch or None if this branch does not have an upstream set. Set to None to unset the upstream configuration."""

    upstream_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The name of the reference set to be the upstream of this one"""



class Commit(Object):
    """ Commit objects. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    author = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The author of the commit."""

    committer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The committer of the commit."""

    commit_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Commit time."""

    commit_time_offset = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Commit time offset."""

    gpg_signature = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """A tuple with the GPG signature and the signed payload."""

    message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The commit message, a text string."""

    message_encoding = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Message encoding."""

    parents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The list of parent commits."""

    parent_ids = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The list of parent commits' ids."""

    raw_message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Message (bytes)."""

    tree = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The tree object attached to the commit."""

    tree_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The id of the tree attached to the commit."""



class Diff(object):
    """ Diff objects. """
    def find_similar(self, flags=None, rename_threshold=None, copy_threshold=None, rename_from_rewrite_threshold=None, break_rewrite_threshold=None, rename_limit=None): # real signature unknown; restored from __doc__
        """
        find_similar([flags, rename_threshold, copy_threshold, rename_from_rewrite_threshold, break_rewrite_threshold, rename_limit])
        
        Find renamed files in diff and updates them in-place in the diff itself.
        """
        pass

    def from_c(self, *args, **kwargs): # real signature unknown
        """ Method exposed for Index to hook into """
        pass

    def merge(self, diff): # real signature unknown; restored from __doc__
        """
        merge(diff)
        
        Merge one diff into another.
        """
        pass

    def parse_diff(self, git_diff): # real signature unknown; restored from __doc__
        """
        parse_diff(git_diff: str) -> Diff
        
        Parses a git unified diff into a diff object without a repository
        """
        return Diff

    def __getitem__(self, *args, **kwargs): # real signature unknown
        """ Return self[key]. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    def __len__(self, *args, **kwargs): # real signature unknown
        """ Return len(self). """
        pass

    deltas = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Iterate over the diff deltas."""

    patch = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Patch diff string. Can be None in some cases, such as empty commits."""

    stats = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Accumulate diff statistics for all patches."""



class DiffDelta(object):
    """ DiffDelta object. """
    def status_char(self): # real signature unknown; restored from __doc__
        """
        status_char()
        
        Return the single character abbreviation for a delta status code.
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Combination of GIT_DIFF_FLAG_* flags."""

    is_binary = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """True if binary data, False if not."""

    new_file = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """"to" side of the diff."""

    nfiles = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Number of files in the delta."""

    old_file = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """"from" side of the diff."""

    similarity = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """For renamed and copied."""

    status = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """A GIT_DELTA_* constant."""



class DiffFile(object):
    """ DiffFile object. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Combination of GIT_DIFF_FLAG_* flags."""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Oid of the item."""

    mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Mode of the entry."""

    path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Path to the entry."""

    raw_path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Path to the entry (bytes)."""

    size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Size of the entry."""



class DiffHunk(object):
    """ DiffHunk object. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    header = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Header."""

    lines = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Lines."""

    new_lines = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """New lines."""

    new_start = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """New start."""

    old_lines = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Old lines."""

    old_start = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Old start."""



class DiffLine(object):
    """ DiffLine object. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    content = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Content of the diff line"""

    content_offset = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Offset in the original file to the content"""

    new_lineno = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Line number in new file or -1 for deleted line"""

    num_lines = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Number of newline characters in content"""

    old_lineno = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Line number in old file or -1 for added line"""

    origin = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Type of the diff line"""

    raw_content = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Content of the diff line (byte string)"""



class DiffStats(object):
    """ DiffStats object. """
    def format(self, format, width): # real signature unknown; restored from __doc__
        """
        format(format, width)
        
        Format the stats as a string.
        
        Returns: str.
        
        Parameters:
        
        format
            The format to use. A pygit2.GIT_DIFF_STATS_* constant.
        
        width
            The width of the output. The output will be scaled to fit.
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    deletions = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Total number of deletions"""

    files_changed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Total number of files changed"""

    insertions = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Total number of insertions"""



class GitError(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class InvalidSpecError(ValueError):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class Mailmap(object):
    """ Mailmap object. """
    def add_entry(self, real_name=None, real_email=None, replace_name=None, replace_email): # real signature unknown; restored from __doc__
        """
        add_entry(real_name=None, real_email=None, replace_name=None, replace_email)
        
        Add a new entry to the mailmap, overriding existing entries.
        """
        pass

    def from_buffer(self, buffer): # real signature unknown; restored from __doc__
        """
        from_buffer(buffer: str) -> Mailmap
        
        Parse a passed-in buffer and construct a mailmap object.
        """
        return Mailmap

    def from_repository(self, repository): # real signature unknown; restored from __doc__
        """
        from_repository(repository: Repository) -> Mailmap
        
        Create a new mailmap instance from a repository, loading mailmap files based on the repository's configuration.
        
        Mailmaps are loaded in the following order:
         1. '.mailmap' in the root of the repository's working directory, if present.
         2. The blob object identified by the 'mailmap.blob' config entry, if set.
        	   [NOTE: 'mailmap.blob' defaults to 'HEAD:.mailmap' in bare repositories]
         3. The path in the 'mailmap.file' config entry, if set.
        """
        return Mailmap

    def resolve(self, name, email): # real signature unknown; restored from __doc__
        """
        resolve(name: str, email: str) -> (str, str)
        
        Resolve name & email to a real name and email.
        """
        pass

    def resolve_signature(self, sig): # real signature unknown; restored from __doc__
        """
        resolve_signature(sig: Signature) -> Signature
        
        Resolve signature to real name and email.
        """
        return Signature

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


class Note(object):
    """ Note object. """
    def remove(self, *args, **kwargs): # real signature unknown
        """ Removes a note for an annotated object """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    annotated_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """id of the annotated object."""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Gets the id of the blob containing the note message
"""

    message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Gets message of the note
"""



class Oid(object):
    """ Object id. """
    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __hash__(self, *args, **kwargs): # real signature unknown
        """ Return hash(self). """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    def __str__(self, *args, **kwargs): # real signature unknown
        """ Return str(self). """
        pass

    hex = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Hex oid, a 40 chars long string (type str).
This attribute is deprecated, please use the built-int str() or unicode()
"""

    raw = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Raw oid, a 20 bytes string."""



class Patch(object):
    """ Diff patch object. """
    def create_from(self, *args, **kwargs): # real signature unknown
        """ Create a patch from blobs, buffers, or a blob and a buffer """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    data = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The raw bytes of the patch's contents."""

    delta = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Get the delta associated with a patch."""

    hunks = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """hunks"""

    line_stats = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Get line counts of each type in a patch."""

    patch = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Patch diff string (deprecated -- use Patch.text instead).
Can be None in some cases, such as empty commits. Note that this decodes the content to Unicode assuming UTF-8 encoding. For non-UTF-8 content that can lead be a lossy, non-reversible process. To access the raw, un-decoded patch, use `patch.data`."""

    text = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Patch diff string. Can be None in some cases, such as empty commits.
Note that this decodes the content to Unicode assuming UTF-8 encoding. For non-UTF-8 content that can lead be a lossy, non-reversible process. To access the raw, un-decoded patch, use `patch.data`."""



class RefLogEntry(object):
    """ Reference log object. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    committer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Committer."""

    message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Message."""

    oid_new = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """New oid."""

    oid_old = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Old oid."""



class Repository(object):
    """
    Repository(backend) -> Repository
    
    Git repository.
    """
    def add_worktree(self, name, path, ref=None): # real signature unknown; restored from __doc__
        """
        add_worktree(name, path, [ref])
        
        Create a new worktree for this repository. If ref is specified, no new     branch will be created and the provided ref will be checked out instead.
        """
        pass

    def apply(self, id): # real signature unknown; restored from __doc__
        """
        apply(id)
        
        Applies the given id into HEAD.
        
        Applies a diff into HEAD, writing the results into the
        working directory.
        """
        pass

    def cherrypick(self, id): # real signature unknown; restored from __doc__
        """
        cherrypick(id)
        
        Cherry-pick the given oid, producing changes in the index and working directory.
        
        Merges the given commit into HEAD as a cherrypick, writing the results into the
        working directory. Any changes are staged for commit and any conflicts
        are written to the index. Callers should inspect the repository's
        index after this completes, resolve any conflicts and prepare a
        commit.
        """
        pass

    def create_blob(self, data): # real signature unknown; restored from __doc__
        """
        create_blob(data) -> Oid
        
        Create a new blob from a bytes string. The blob is added to the Git
        object database. Returns the oid of the blob.
        """
        return Oid

    def create_blob_fromdisk(self, path): # real signature unknown; restored from __doc__
        """
        create_blob_fromdisk(path) -> Oid
        
        Create a new blob from a file anywhere (no working directory check).
        """
        return Oid

    def create_blob_fromiobase(self, io_IOBase): # real signature unknown; restored from __doc__
        """
        create_blob_fromiobase(io.IOBase) -> Oid
        
        Create a new blob from an IOBase object.
        """
        return Oid

    def create_blob_fromworkdir(self, path): # real signature unknown; restored from __doc__
        """
        create_blob_fromworkdir(path) -> Oid
        
        Create a new blob from a file within the working directory. The given
        path must be relative to the working directory, if it is not an error
        is raised.
        """
        return Oid

    def create_branch(self, name, commit, force=False): # real signature unknown; restored from __doc__
        """
        create_branch(name, commit, force=False)
        
        Create a new branch "name" which points to a commit.
        
        Returns: Branch
        
        Parameters:
        
        force
            If True branches will be overridden, otherwise (the default) an
            exception is raised.
        
        Examples::
        
            repo.create_branch('foo', repo.head.peel(), force=False)
        """
        pass

    def create_commit(self, reference_name, author, committer, message, tree, parents, encoding=None): # real signature unknown; restored from __doc__
        """
        create_commit(reference_name, author, committer, message, tree, parents[, encoding]) -> Oid
        
        Create a new commit object, return its oid.
        """
        return Oid

    def create_note(self, message, author, committer, annotated_id, ref=None, force=None): # real signature unknown; restored from __doc__
        """
        create_note(message, author, committer, annotated_id [,ref, force]) -> Oid
        
        Create a new note for an object, return its SHA-ID.If no ref is given 'refs/notes/commits' will be used.
        """
        return Oid

    def create_reference_direct(self, name, target, force): # real signature unknown; restored from __doc__
        """
        create_reference_direct(name, target, force)
        
        Create a new reference "name" which points to an object.
        
        Returns: Reference
        
        Parameters:
        
        force
            If True references will be overridden, otherwise (the default) an
            exception is raised.
        
        Examples::
        
            repo.create_reference_direct('refs/heads/foo', repo.head.target, False)
        """
        pass

    def create_reference_symbolic(self, name, source, force): # real signature unknown; restored from __doc__
        """
        create_reference_symbolic(name, source, force)
        
        Create a new reference "name" which points to another reference.
        
        Returns: Reference
        
        Parameters:
        
        force
            If True references will be overridden, otherwise (the default) an
            exception is raised.
        
        Examples::
        
            repo.create_reference_symbolic('refs/tags/foo', 'refs/heads/master', False)
        """
        pass

    def create_tag(self, name, oid, type, tagger, message): # real signature unknown; restored from __doc__
        """
        create_tag(name, oid, type, tagger, message) -> Oid
        
        Create a new tag object, return its oid.
        """
        return Oid

    def descendant_of(self, oid, oid_1): # real signature unknown; restored from __doc__
        """
        descendant_of(oid, oid) -> bool
        
        Determine if the first commit is a descendant of the second commit.
        Note that a commit is not considered a descendant of itself.
        """
        return False

    def expand_id(self, hex): # real signature unknown; restored from __doc__
        """
        expand_id(hex) -> Oid
        
        Expand a string into a full Oid according to the objects in this repsitory.
        """
        return Oid

    def free(self): # real signature unknown; restored from __doc__
        """
        free()
        
        Releases handles to the Git database without deallocating the repository.
        """
        pass

    def git_object_lookup_prefix(self, oid): # real signature unknown; restored from __doc__
        """
        git_object_lookup_prefix(oid) -> Object
        
        Returns the Git object with the given oid.
        """
        return Object

    def init_submodules(self, *args, **kwargs): # real signature unknown
        """
        init_submodule(submodules=None, overwrite=False)
        
        Initialize all submodules in repository.
        submodules: List of submodules to initialize. Default argument initializes all submodules.
        overwrite: Flag indicating if initialization should overwrite submodule entries.
        """
        pass

    def listall_branches(self, flag=None): # real signature unknown; restored from __doc__
        """
        listall_branches([flag]) -> [str, ...]
        
        Return a list with all the branches in the repository.
        
        The *flag* may be:
        
        - GIT_BRANCH_LOCAL - return all local branches (set by default)
        - GIT_BRANCH_REMOTE - return all remote-tracking branches
        - GIT_BRANCH_ALL - return local branches and remote-tracking branches
        """
        pass

    def listall_references(self): # real signature unknown; restored from __doc__
        """
        listall_references() -> [str, ...]
        
        Return a list with all the references in the repository.
        """
        pass

    def listall_reference_objects(self): # real signature unknown; restored from __doc__
        """
        listall_reference_objects() -> [Reference, ...]
        
        Return a list with all the reference objects in the repository.
        """
        pass

    def listall_submodules(self): # real signature unknown; restored from __doc__
        """
        listall_submodules() -> [str, ...]
        
        Return a list with all submodule paths in the repository.
        """
        pass

    def list_worktrees(self): # real signature unknown; restored from __doc__
        """
        list_worktrees() -> [str, ...]
        
        Return a list with all the worktrees of this repository.
        """
        pass

    def lookup_branch(self, branch_name, branch_type=None): # real signature unknown; restored from __doc__
        """
        lookup_branch(branch_name, [branch_type]) -> Branch
        
        Returns the Git reference for the given branch name (local or remote).
        If branch_type is GIT_BRANCH_REMOTE, you must include the remote name
        in the branch name (eg 'origin/master').
        """
        return Branch

    def lookup_note(self, annotated_id, ref=None): # real signature unknown; restored from __doc__
        """
        lookup_note(annotated_id [, ref]) -> Note
        
        Lookup a note for an annotated object in a repository.
        """
        return Note

    def lookup_reference(self, name): # real signature unknown; restored from __doc__
        """
        lookup_reference(name) -> Reference
        
        Lookup a reference by its name in a repository.
        """
        return Reference

    def lookup_worktree(self, name): # real signature unknown; restored from __doc__
        """
        lookup_worktree(name) -> Worktree
        
        Lookup a worktree from its name.
        """
        return Worktree

    def merge(self, id): # real signature unknown; restored from __doc__
        """
        merge(id)
        
        Merges the given id into HEAD.
        
        Merges the given commit(s) into HEAD, writing the results into the
        working directory. Any changes are staged for commit and any conflicts
        are written to the index. Callers should inspect the repository's
        index after this completes, resolve any conflicts and prepare a
        commit.
        """
        pass

    def merge_analysis(self, their_head, our_ref='HEAD'): # real signature unknown; restored from __doc__
        """
        merge_analysis(their_head, our_ref='HEAD') -> (Integer, Integer)
        
        Analyzes the given branch and determines the opportunities for
        merging it into a reference (defaults to HEAD).
        
        Parameters:
        
        our_ref
            The reference name (String) to perform the analysis from
        
        their_head
            Head (commit Oid) to merge into
        
        The first returned value is a mixture of the GIT_MERGE_ANALYSIS_NONE, _NORMAL,
        _UP_TO_DATE, _FASTFORWARD and _UNBORN flags.
        The second value is the user's preference from 'merge.ff'
        """
        pass

    def merge_base(self, oid, oid_1): # real signature unknown; restored from __doc__
        """
        merge_base(oid, oid) -> Oid
        
        Find as good common ancestors as possible for a merge.
        Returns None if there is no merge base between the commits
        """
        return Oid

    def notes(self, *args, **kwargs): # real signature unknown
        pass

    def path_is_ignored(self, *args, **kwargs): # real signature unknown
        """ Check if a path is ignored in the repository. """
        pass

    def read(self, oid): # real signature unknown; restored from __doc__
        """
        read(oid) -> type, data, size
        
        Read raw object data from the repository.
        """
        return type(*(), **{})

    def reset(self, oid, reset_type): # real signature unknown; restored from __doc__
        """
        reset(oid, reset_type)
        
        Resets the current head.
        
        Parameters:
        
        oid
            The oid of the commit to reset to.
        
        reset_type
            * GIT_RESET_SOFT: resets head to point to oid, but does not modify
              working copy, and leaves the changes in the index.
            * GIT_RESET_MIXED: resets head to point to oid, but does not modify
              working copy. It empties the index too.
            * GIT_RESET_HARD: resets head to point to oid, and resets too the
              working copy and the content of the index.
        """
        pass

    def revparse_single(self, revision): # real signature unknown; restored from __doc__
        """
        revparse_single(revision) -> Object
        
        Find an object, as specified by a revision string. See
        `man gitrevisions`, or the documentation for `git rev-parse` for
        information on the syntax accepted.
        """
        return Object

    def status(self): # real signature unknown; restored from __doc__
        """
        status() -> {str: int}
        
        Reads the status of the repository and returns a dictionary with file
        paths as keys and status flags as values. See pygit2.GIT_STATUS_*.
        """
        pass

    def status_file(self, path): # real signature unknown; restored from __doc__
        """
        status_file(path) -> int
        
        Returns the status of the given file path.
        """
        return 0

    def TreeBuilder(self, tree=None): # real signature unknown; restored from __doc__
        """
        TreeBuilder([tree]) -> TreeBuilder
        
        Create a TreeBuilder object for this repository.
        """
        return TreeBuilder

    def walk(self, oid, sort_mode=None): # real signature unknown; restored from __doc__
        """
        walk(oid[, sort_mode]) -> iterator
        
        Generator that traverses the history starting from the given commit.
        The following types of sorting could be used to control traversing
        direction:
        
        * GIT_SORT_NONE. This is the default sorting for new walkers.
          Sort the repository contents in no particular ordering
        * GIT_SORT_TOPOLOGICAL. Sort the repository contents in topological order
          (parents before children); this sorting mode can be combined with
          time sorting.
        * GIT_SORT_TIME. Sort the repository contents by commit time
        * GIT_SORT_REVERSE. Iterate through the repository contents in reverse
          order; this sorting mode can be combined with any of the above.
        
        Example:
        
          >>> from pygit2 import Repository
          >>> from pygit2 import GIT_SORT_TOPOLOGICAL, GIT_SORT_REVERSE
          >>> repo = Repository('.git')
          >>> for commit in repo.walk(repo.head.target, GIT_SORT_TOPOLOGICAL):
          ...    print(commit.message)
          >>> for commit in repo.walk(repo.head.target, GIT_SORT_TOPOLOGICAL | GIT_SORT_REVERSE):
          ...    print(commit.message)
          >>>
        """
        pass

    def write(self, type, data): # real signature unknown; restored from __doc__
        """
        write(type, data) -> Oid
        
        Write raw object data into the repository. First arg is the object
        type, the second one a buffer with data. Return the Oid of the created
        object.
        """
        return Oid

    def _disown(self, *args, **kwargs): # real signature unknown
        """ Mark the object as not-owned by us. For internal use only. """
        pass

    def _from_c(self, *args, **kwargs): # real signature unknown
        """ Init a Repository from a pointer. For internal use only. """
        pass

    def __init__(self, backend): # real signature unknown; restored from __doc__
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    default_signature = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Return the signature according to the repository's configuration"""

    head = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Current head reference of the repository."""

    head_is_detached = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """A repository's HEAD is detached when it points directly to a commit
instead of a branch."""

    head_is_unborn = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """An unborn branch is one named from HEAD but which doesn't exist in the
refs namespace, because it doesn't have any commit to point to."""

    is_bare = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Check if a repository is a bare repository."""

    is_empty = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Check if a repository is empty."""

    path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The normalized path to the git repository."""

    workdir = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """The normalized path to the working directory of the repository. If the
repository is bare, None will be returned."""

    _pointer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Get the repo's pointer. For internal use only."""



class Signature(object):
    """ Signature. """
    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    email = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Email address."""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Name."""

    offset = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Offset from UTC in minutes."""

    raw_email = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Email (bytes)."""

    raw_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Name (bytes)."""

    time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Unix time."""

    _encoding = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Encoding."""

    _pointer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Get the signature's pointer. For internal use only."""


    __hash__ = None


class Tag(Object):
    """ Tag objects. """
    def get_object(self): # real signature unknown; restored from __doc__
        """
        get_object() -> object
        
        Retrieves the object the current tag is pointing to.
        """
        return object()

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tag message."""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tag name."""

    raw_message = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tag message (bytes)."""

    raw_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tag name (bytes)."""

    tagger = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tagger."""

    target = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Tagged object."""



class Tree(Object):
    """ Tree objects. """
    def diff_to_index(self, index, flags=None, context_lines=None, interhunk_lines=None): # real signature unknown; restored from __doc__
        """
        diff_to_index(index, [flags, context_lines, interhunk_lines]) -> Diff
        
        Show the changes between the index and a given :py:class:`~pygit2.Tree`.
        
        Parameters:
        
        tree : :py:class:`~pygit2.Tree`
            The tree to diff.
        
        flag
            A GIT_DIFF_* constant.
        
        context_lines
            The number of unchanged lines that define the boundary of a hunk
            (and to display before and after).
        
        interhunk_lines
            The maximum number of unchanged lines between hunk boundaries before
            the hunks will be merged into a one.
        """
        return Diff

    def diff_to_tree(self, tree=None, flags=None, context_lines=None, interhunk_lines=None, swap=None): # real signature unknown; restored from __doc__
        """
        diff_to_tree([tree, flags, context_lines, interhunk_lines, swap]) -> Diff
        
        Show the changes between two trees.
        
        Parameters:
        
        tree: :py:class:`~pygit2.Tree`
            The tree to diff. If no tree is given the empty tree will be used
            instead.
        
        flag
            A GIT_DIFF_* constant.
        
        context_lines
            The number of unchanged lines that define the boundary of a hunk
            (and to display before and after).
        
        interhunk_lines
            The maximum number of unchanged lines between hunk boundaries before
            the hunks will be merged into a one.
        
        swap
            Instead of diffing a to b. Diff b to a.
        """
        return Diff

    def diff_to_workdir(self, flags=None, context_lines=None, interhunk_lines=None): # real signature unknown; restored from __doc__
        """
        diff_to_workdir([flags, context_lines, interhunk_lines]) -> Diff
        
        Show the changes between the :py:class:`~pygit2.Tree` and the workdir.
        
        Parameters:
        
        flag
            A GIT_DIFF_* constant.
        
        context_lines
            The number of unchanged lines that define the boundary of a hunk
            (and to display before and after).
        
        interhunk_lines
            The maximum number of unchanged lines between hunk boundaries before
            the hunks will be merged into a one.
        """
        return Diff

    def __contains__(self, *args, **kwargs): # real signature unknown
        """ Return key in self. """
        pass

    def __getitem__(self, *args, **kwargs): # real signature unknown
        """ Return self[key]. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    def __len__(self, *args, **kwargs): # real signature unknown
        """ Return len(self). """
        pass


class TreeBuilder(object):
    """ TreeBuilder objects. """
    def clear(self): # real signature unknown; restored from __doc__
        """
        clear()
        
        Clear all the entries in the builder.
        """
        pass

    def get(self, name): # real signature unknown; restored from __doc__
        """
        get(name) -> TreeEntry
        
        Return the TreeEntry for the given name, or None if there is not.
        """
        return TreeEntry

    def insert(self, name, oid, attr): # real signature unknown; restored from __doc__
        """
        insert(name, oid, attr)
        
        Insert or replace an entry in the treebuilder.
        
        Parameters:
        
        attr
            Available values are GIT_FILEMODE_BLOB,
            GIT_FILEMODE_BLOB_EXECUTABLE, GIT_FILEMODE_TREE, GIT_FILEMODE_LINK
            and GIT_FILEMODE_COMMIT.
        """
        pass

    def remove(self, name): # real signature unknown; restored from __doc__
        """
        remove(name)
        
        Remove an entry from the builder.
        """
        pass

    def write(self): # real signature unknown; restored from __doc__
        """
        write() -> Oid
        
        Write the tree to the given repository.
        """
        return Oid

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __len__(self, *args, **kwargs): # real signature unknown
        """ Return len(self). """
        pass


class TreeEntry(object):
    """ TreeEntry objects. """
    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    filemode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Filemode."""

    hex = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Hex oid."""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Object id."""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Name."""

    oid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Object id.
This attribute is deprecated. Please use 'id'"""

    type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Type."""

    _name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Name (bytes)."""


    __hash__ = None


class Walker(object):
    """ Revision walker. """
    def hide(self, oid): # real signature unknown; restored from __doc__
        """
        hide(oid)
        
        Mark a commit (and its ancestors) uninteresting for the output.
        """
        pass

    def push(self, oid): # real signature unknown; restored from __doc__
        """
        push(oid)
        
        Mark a commit to start traversal from.
        """
        pass

    def reset(self): # real signature unknown; restored from __doc__
        """
        reset()
        
        Reset the walking machinery for reuse.
        """
        pass

    def simplify_first_parent(self): # real signature unknown; restored from __doc__
        """
        simplify_first_parent()
        
        Simplify the history by first-parent.
        """
        pass

    def sort(self, mode): # real signature unknown; restored from __doc__
        """
        sort(mode)
        
        Change the sorting mode (this resets the walker).
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    def __next__(self, *args, **kwargs): # real signature unknown
        """ Implement next(self). """
        pass


class Worktree(object):
    """ Worktree object. """
    def prune(self, force=False): # real signature unknown; restored from __doc__
        """
        prune(force=False)
        
        Prune a worktree object.
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    is_prunable = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Is the worktree prunable with the given set of flags?
"""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Gets name worktree
"""

    path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Gets path worktree
"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>'

__spec__ = None # (!) real value is "ModuleSpec(name='_pygit2', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>, origin='/usr/lib/python3.8/site-packages/_pygit2.cpython-38-x86_64-linux-gnu.so')"

