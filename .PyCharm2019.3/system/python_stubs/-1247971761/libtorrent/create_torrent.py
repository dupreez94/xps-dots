# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class create_torrent(__Boost_Python.instance):
    # no doc
    def add_collection(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_collection( (create_torrent)arg1, (object)arg2) -> None :
        
            C++ signature :
                void add_collection(libtorrent::create_torrent {lvalue},boost::basic_string_view<char, std::char_traits<char> >)
        """
        pass

    def add_http_seed(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_http_seed( (create_torrent)arg1, (object)arg2) -> None :
        
            C++ signature :
                void add_http_seed(libtorrent::create_torrent {lvalue},boost::basic_string_view<char, std::char_traits<char> >)
        """
        pass

    def add_node(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_node( (create_torrent)arg1, (str)arg2, (int)arg3) -> None :
        
            C++ signature :
                void add_node(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,int)
        """
        pass

    def add_similar_torrent(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_similar_torrent( (create_torrent)arg1, (sha1_hash)arg2) -> None :
        
            C++ signature :
                void add_similar_torrent(libtorrent::create_torrent {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def add_tracker(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_tracker( (create_torrent)arg1, (str)announce_url [, (int)tier=0]) -> None :
        
            C++ signature :
                void add_tracker(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,int=0])
        """
        pass

    def add_url_seed(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_url_seed( (create_torrent)arg1, (object)arg2) -> None :
        
            C++ signature :
                void add_url_seed(libtorrent::create_torrent {lvalue},boost::basic_string_view<char, std::char_traits<char> >)
        """
        pass

    def files(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        files( (create_torrent)arg1) -> file_storage :
        
            C++ signature :
                libtorrent::file_storage files(libtorrent::create_torrent {lvalue})
        """
        pass

    def generate(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        generate( (create_torrent)arg1) -> object :
        
            C++ signature :
                libtorrent::entry generate(libtorrent::create_torrent {lvalue})
        """
        pass

    def num_pieces(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_pieces( (create_torrent)arg1) -> int :
        
            C++ signature :
                int num_pieces(libtorrent::create_torrent {lvalue})
        """
        pass

    def piece_length(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_length( (create_torrent)arg1) -> int :
        
            C++ signature :
                int piece_length(libtorrent::create_torrent {lvalue})
        """
        pass

    def piece_size(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_size( (create_torrent)arg1, (object)arg2) -> int :
        
            C++ signature :
                int piece_size(libtorrent::create_torrent {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def priv(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        priv( (create_torrent)arg1) -> bool :
        
            C++ signature :
                bool priv(libtorrent::create_torrent {lvalue})
        """
        pass

    def set_comment(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_comment( (create_torrent)arg1, (str)arg2) -> None :
        
            C++ signature :
                void set_comment(libtorrent::create_torrent {lvalue},char const*)
        """
        pass

    def set_creator(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_creator( (create_torrent)arg1, (str)arg2) -> None :
        
            C++ signature :
                void set_creator(libtorrent::create_torrent {lvalue},char const*)
        """
        pass

    def set_file_hash(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_file_hash( (create_torrent)arg1, (object)arg2, (object)arg3) -> None :
        
            C++ signature :
                void set_file_hash(libtorrent::create_torrent {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,bytes)
        """
        pass

    def set_hash(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_hash( (create_torrent)arg1, (object)arg2, (object)arg3) -> None :
        
            C++ signature :
                void set_hash(libtorrent::create_torrent {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>,bytes)
        """
        pass

    def set_priv(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_priv( (create_torrent)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void set_priv(libtorrent::create_torrent {lvalue},bool)
        """
        pass

    def set_root_cert(self, create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_root_cert( (create_torrent)arg1, (object)pem) -> None :
        
            C++ signature :
                void set_root_cert(libtorrent::create_torrent {lvalue},boost::basic_string_view<char, std::char_traits<char> >)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (file_storage)arg2) -> None :
        
            C++ signature :
                void __init__(_object*,libtorrent::file_storage {lvalue})
        
        __init__( (object)arg1, (torrent_info)ti) -> None :
        
            C++ signature :
                void __init__(_object*,libtorrent::torrent_info)
        
        __init__( (object)arg1, (file_storage)storage [, (int)piece_size=0 [, (int)pad_file_limit=-1 [, (object)flags=1]]]) -> None :
        
            C++ signature :
                void __init__(_object*,libtorrent::file_storage {lvalue} [,int=0 [,int=-1 [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::create_flags_tag, void>=1]]])
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    merkle = 2
    modification_time = 4
    optimize_alignment = 1
    symlinks = 8


