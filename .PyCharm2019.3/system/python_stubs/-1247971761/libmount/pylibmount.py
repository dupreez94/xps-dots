# encoding: utf-8
# module libmount.pylibmount
# from /usr/lib/python3.8/site-packages/libmount/pylibmount.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

MNT_ITER_BACKWARD = 1
MNT_ITER_FORWARD = 0

MNT_MS_COMMENT = 256
MNT_MS_ENCRYPTION = 65536
MNT_MS_GROUP = 64
MNT_MS_HELPER = 4096
MNT_MS_LOOP = 512
MNT_MS_NETDEV = 128
MNT_MS_NOAUTO = 4
MNT_MS_NOFAIL = 1024
MNT_MS_OFFSET = 16384
MNT_MS_OWNER = 32
MNT_MS_SIZELIMIT = 32768
MNT_MS_UHELPER = 2048
MNT_MS_USER = 8
MNT_MS_USERS = 16
MNT_MS_XCOMMENT = 8192

MS_BIND = 4096
MS_DIRSYNC = 128

MS_I_VERSION = 8388608

MS_MANDLOCK = 64

MS_MGC_MSK = 4294901760
MS_MGC_VAL = 3236757504

MS_MOVE = 8192
MS_NOATIME = 1024
MS_NODEV = 4
MS_NODIRATIME = 2048
MS_NOEXEC = 8
MS_NOSUID = 2
MS_OWNERSECURE = 6
MS_PRIVATE = 262144
MS_PROPAGATION = 1966080
MS_RDONLY = 1
MS_REC = 16384
MS_RELATIME = 2097152
MS_REMOUNT = 32
MS_SECURE = 14
MS_SHARED = 1048576
MS_SILENT = 32768
MS_SLAVE = 524288
MS_STRICTATIME = 16777216
MS_SYNCHRONOUS = 16
MS_UNBINDABLE = 131072

# functions

def error_out(*args, **kwargs): # real signature unknown
    pass

# classes

class Context(object):
    """ Context(source=None, target=None, fstype=None, options=None, mflags=0, fstype_pattern=None, options_pattern=None, fs=None, fstab=None, optsmode=0) """
    def append_options(self, optstr): # real signature unknown; restored from __doc__
        """
        append_options(optstr)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def apply_fstab(self): # real signature unknown; restored from __doc__
        """
        apply_fstab()
        
        This function is optional.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def disable_canonicalize(self, disable): # real signature unknown; restored from __doc__
        """
        disable_canonicalize(disable)
        
        Enable/disable paths canonicalization and tags evaluation. The libmount context
        canonicalizes paths when searching fstab and when preparing source and target paths
        for mount(2) syscall.
        
        This function has effect to the private (within context) fstab instance only
        (see Cxt.fstab).
        Returns self or raises an exception in case of an error.
        """
        pass

    def disable_helpers(self, disable): # real signature unknown; restored from __doc__
        """
        disable_helpers(disable)
        
        Enable/disable /sbin/[u]mount.* helpers (see mount(8) man page, option -i).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def disable_mtab(self, disable): # real signature unknown; restored from __doc__
        """
        disable_mtab(disable)
        
        Disable/enable mtab update (see mount(8) man page, option -n).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def disable_swapmatch(self, disable): # real signature unknown; restored from __doc__
        """
        disable_swapmatch(disable)
        
        Disable/enable swap between source and target for mount(8) if only one path
        is specified.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def do_mount(self): # real signature unknown; restored from __doc__
        """
        do_mount()
        
        Call mount(2) or mount.type helper. Unnecessary for Cxt.mount().
        
        Note that this function could be called only once. If you want to mount
        another source or target than you have to call Cxt.reset_context().
        
        If you want to call mount(2) for the same source and target with a different
        mount flags or fstype then call Cxt.reset_status() and then try
        again Cxt.do_mount().
        
        WARNING: non-zero return code does not mean that mount(2) syscall or
        mount.type helper wasn't successfully called.
        
        Check Cxt.status after error!
        
        Returns self on success or an exception in case of other errors.
        """
        pass

    def do_umount(self): # real signature unknown; restored from __doc__
        """
        do_umount()
        
        Umount filesystem by umount(2) or fork()+exec(/sbin/umount.type).
        Unnecessary for Cxt.umount().
        
        See also Cxt.disable_helpers().
        
        WARNING: non-zero return code does not mean that umount(2) syscall or
        umount.type helper wasn't successfully called.
        
        Check Cxt.status after error!
        
        Returns self on success or an exception in case of other errors.
        """
        pass

    def enable_fake(self, enable): # real signature unknown; restored from __doc__
        """
        enable_fake(enable)
        
        Enable/disable fake mounting (see mount(8) man page, option -f).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_force(self, enable): # real signature unknown; restored from __doc__
        """
        enable_force(enable)
        
        Enable/disable force umounting (see umount(8) man page, option -f).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_fork(self, enable): # real signature unknown; restored from __doc__
        """
        enable_fork(enable)
        
        Enable/disable fork(2) call in Cxt.next_mount()(not yet implemented) (see mount(8) man
        page, option -F).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_lazy(self, enable): # real signature unknown; restored from __doc__
        """
        enable_lazy(enable)
        
        Enable/disable lazy umount (see umount(8) man page, option -l).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_loopdel(self, enable): # real signature unknown; restored from __doc__
        """
        enable_loopdel(enable)
        
        Enable/disable loop delete (destroy) after umount (see umount(8), option -d)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_rdonly_umount(self, enable): # real signature unknown; restored from __doc__
        """
        enable_rdonly_umount(enable)
        
        Enable/disable read-only remount on failed umount(2)
         (see umount(8) man page, option -r).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_sloppy(self, enable): # real signature unknown; restored from __doc__
        """
        enable_sloppy(enable)
        
        Set/unset sloppy mounting (see mount(8) man page, option -s).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_verbose(self, enable): # real signature unknown; restored from __doc__
        """
        enable_verbose(enable)
        
        Enable/disable verbose output (TODO: not implemented yet)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def finalize_mount(self): # real signature unknown; restored from __doc__
        """
        finalize_mount()
        
        Mtab update, etc. Unnecessary for Cxt.mount(), but should be called
        after Cxt.do_mount(). See also Cxt.syscall_status.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def finalize_umount(self): # real signature unknown; restored from __doc__
        """
        finalize_umount()
        
        Mtab update, etc. Unnecessary for Cxt.umount(), but should be called
        after Cxt.do_umount(). See also Cxt.syscall_status.
        
        Returns self on success, raises LibmountError if target filesystem not found, or other exception on error.
        """
        pass

    def find_umount_fs(self, tgt, pfs): # real signature unknown; restored from __doc__
        """
        find_umount_fs(tgt, pfs)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def helper_executed(self): # real signature unknown; restored from __doc__
        """
        helper_executed()
        
        Returns True if mount.type helper has been executed, or False.
        """
        pass

    def helper_setopt(self, c, arg): # real signature unknown; restored from __doc__
        """
        helper_setopt(c, arg)
        
        This function applies [u]mount.type command line option (for example parsed
        by getopt or getopt_long) to cxt. All unknown options are ignored and
        then ValueError is raised.
        
        Returns self on success, raises ValueError if c is unknown or other exception in case of an error.
        """
        pass

    def init_helper(self, action, flags): # real signature unknown; restored from __doc__
        """
        init_helper(action, flags)
        
        This function informs libmount that it is used from [u]mount.type helper.
        
        The function also calls Cxt.disable_helpers() to avoid calling
        mount.type helpers recursively. If you really want to call another
        mount.type helper from your helper then you have to explicitly enable this
        feature by:
        
        Cxt.disable_helpers(False);
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def is_child(self): # real signature unknown; restored from __doc__
        """
        is_child()
        
        Returns True if mount -F enabled and the current context is child, or False
        """
        pass

    def is_fake(self): # real signature unknown; restored from __doc__
        """
        is_fake()
        
        Returns True if fake flag is enabled or False
        """
        pass

    def is_force(self): # real signature unknown; restored from __doc__
        """
        is_force()
        
        Returns True if force umounting flag is enabled or False
        """
        pass

    def is_fork(self): # real signature unknown; restored from __doc__
        """
        is_fork()
        
        Returns True if fork (mount -F) is enabled or False
        """
        pass

    def is_fs_mounted(self, fs, mounted): # real signature unknown; restored from __doc__
        """
        is_fs_mounted(fs, mounted)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def is_lazy(self): # real signature unknown; restored from __doc__
        """
        is_lazy()
        
        Returns True if lazy umount is enabled or False
        """
        pass

    def is_loopdel(self): # real signature unknown; restored from __doc__
        """
        is_loopdel()
        
        Returns True if loop device should be deleted after umount (umount -d) or False.
        """
        pass

    def is_nocanonicalize(self): # real signature unknown; restored from __doc__
        """
        is_nocanonicalize()
        
        Returns True if no-canonicalize mode enabled or False.
        """
        pass

    def is_nohelpers(self): # real signature unknown; restored from __doc__
        """
        is_nohelpers()
        
        Returns True if helpers are disabled (mount -i) or False.
        """
        pass

    def is_nomtab(self): # real signature unknown; restored from __doc__
        """
        is_nomtab()
        
        Returns True if no-mtab is enabled or False
        """
        pass

    def is_parent(self): # real signature unknown; restored from __doc__
        """
        is_parent()
        
        Returns True if mount -F enabled and the current context is parent, or False
        """
        pass

    def is_rdonly_umount(self): # real signature unknown; restored from __doc__
        """
        is_rdonly_umount()
        
        Enable/disable read-only remount on failed umount(2)
        (see umount(8) man page, option -r).
        
        Returns self on success, raises an exception in case of error.
        """
        pass

    def is_restricted(self): # real signature unknown; restored from __doc__
        """
        is_restricted()
        
        Returns False for unrestricted mount (user is root), or True for non-root mounts
        """
        pass

    def is_sloppy(self): # real signature unknown; restored from __doc__
        """
        is_sloppy()
        
        Returns True if sloppy flag is enabled or False
        """
        pass

    def is_swapmatch(self): # real signature unknown; restored from __doc__
        """
        is_swapmatch()
        
        Returns True if swap between source and target is allowed (default is True) or False.
        """
        pass

    def is_verbose(self): # real signature unknown; restored from __doc__
        """
        is_verbose()
        
        Returns True if verbose flag is enabled or False
        """
        pass

    def mount(self): # real signature unknown; restored from __doc__
        """
        mount()
        
        High-level, mounts filesystem by mount(2) or fork()+exec(/sbin/mount.type).
        
        This is similar to:
        
        Cxt.prepare_mount();
        Cxt.do_mount();
        Cxt.finalize_mount();
        
        See also Cxt.disable_helper().
        
        Note that this function could be called only once. If you want to mount with
        different setting than you have to call Cxt.reset_context(). It's NOT enough
        to call Cxt.reset_status() if you want call this function more than
        once, whole context has to be reset.
        
        WARNING: non-zero return code does not mean that mount(2) syscall or
        mount.type helper wasn't successfully called.
        
        Check Cxt.status after error!
        
        Returns self on success or an exception in case of other errors.
        """
        pass

    def prepare_mount(self): # real signature unknown; restored from __doc__
        """
        prepare_mount()
        
        Prepare context for mounting, unnecessary for Cxt.mount().
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def prepare_umount(self): # real signature unknown; restored from __doc__
        """
        prepare_umount()
        
        Prepare context for umounting, unnecessary for Cxt.umount().
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def reset_status(self): # real signature unknown; restored from __doc__
        """
        reset_status()
        
        Resets mount(2) and mount.type statuses, so Cxt.do_mount() or
        Cxt.do_umount() could be again called with the same settings.
        
        BE CAREFUL -- after this soft reset the libmount will NOT parse mount
        options, evaluate permissions or apply stuff from fstab.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def syscall_called(self): # real signature unknown; restored from __doc__
        """
        syscall_called()
        
        Returns True if mount(2) syscall has been called, or False.
        """
        pass

    def tab_applied(self): # real signature unknown; restored from __doc__
        """
        tab_applied()
        
        Returns True if fstab (or mtab) has been applied to the context, False otherwise.
        """
        pass

    def umount(self): # real signature unknown; restored from __doc__
        """
        umount()
        
        High-level, umounts filesystem by umount(2) or fork()+exec(/sbin/umount.type).
        
        This is similar to:
        
        Cxt.prepare_umount();
        Cxt.do_umount();
        Cxt.finalize_umount();
        
        See also Cxt.disable_helpers().
        
        WARNING: non-zero return code does not mean that umount(2) syscall or
        umount.type helper wasn't successfully called.
        
        Check Cxt.status after error!
        
        Returns self on success or an exception in case of other errors.
        """
        pass

    def __init__(self, source=None, target=None, fstype=None, options=None, mflags=0, fstype_pattern=None, options_pattern=None, fs=None, fstab=None, optsmode=0): # real signature unknown; restored from __doc__
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    fs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """filesystem description (type, mountpoint, device, ...)"""

    fstab = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab (or mtab for some remounts)"""

    fstype = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstype"""

    fstype_pattern = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstype_pattern"""

    mflags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mflags"""

    mountdata = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountdata"""

    mtab = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mtab entries"""

    options = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """options"""

    options_pattern = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """options_pattern"""

    optsmode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab optstr mode MNT_OPTSMODE_{AUTO,FORCE,IGNORE}"""

    source = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """source"""

    status = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """status"""

    syscall_errno = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """1: not_called yet, 0: success, <0: -errno"""

    tables_errcb = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """error callback function"""

    target = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """target"""

    user_mflags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """user mflags"""



class Error(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class Fs(object):
    """ Fs(source=None, root=None, target=None, fstype=None, options=None, attributes=None, freq=0, passno=0) """
    def append_options(self, optstr): # real signature unknown; restored from __doc__
        """
        append_options(optstr)
        
        Parses (splits) optstr and appends results to VFS, FS and userspace lists of options.
        """
        pass

    def copy_fs(self, dest=None): # real signature unknown; restored from __doc__
        """
        copy_fs(dest=None)
        
        If dest is None, a new object is created, if any fs field is already set, then the field is NOT overwritten.
        """
        pass

    def get_propagation(self, flags): # real signature unknown; restored from __doc__
        """
        get_propagation(flags)
        
        Note that this function set flags to zero if not found any propagation flag
        in mountinfo file. The kernel default is MS_PRIVATE, this flag is not stored
        in the mountinfo file.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def is_kernel(self): # real signature unknown; restored from __doc__
        """
        is_kernel()
        
        Returns 1 if the filesystem description is read from kernel e.g. /proc/mounts.
        """
        pass

    def is_netfs(self): # real signature unknown; restored from __doc__
        """
        is_netfs()
        
        Returns 1 if the filesystem is a network filesystem
        """
        pass

    def is_pseudofs(self): # real signature unknown; restored from __doc__
        """
        is_pseudofs()
        
        Returns 1 if the filesystem is a pseudo fs type (proc, cgroups)
        """
        pass

    def is_swaparea(self): # real signature unknown; restored from __doc__
        """
        is_swaparea()
        
        Returns 1 if the filesystem uses "swap" as a type
        """
        pass

    def match_fstype(self, pattern): # real signature unknown; restored from __doc__
        """
        match_fstype(pattern)
        
        pattern: filesystem name or comma delimited list(string) of names
        
        The pattern list of filesystem can be prefixed with a global
        "no" prefix to invert matching of the whole list. The "no" could
        also be used for individual items in the pattern list. So,
        "nofoo,bar" has the same meaning as "nofoo,nobar".
        "bar" : "nofoo,bar"	-> False   (global "no" prefix)
        "bar" : "foo,bar"		-> True
        "bar" : "foo,nobar"	-> False
        
        Returns True if type is matching, else False.
        """
        pass

    def match_options(self, options): # real signature unknown; restored from __doc__
        """
        match_options(options)
        
        options: comma delimited list of options (and nooptions)
        Returns True if fs type is matching to options else False.
        """
        pass

    def mnt_fs_append_attributes(self, *args, **kwargs): # real signature unknown
        """
        append_attributes(optstr)
        
        Appends mount attributes.
        """
        pass

    def mnt_fs_prepend_attributes(self, *args, **kwargs): # real signature unknown
        """
        prepend_attributes(optstr)
        
        Prepends mount attributes.
        """
        pass

    def prepend_options(self, optstr): # real signature unknown; restored from __doc__
        """
        prepend_options(optstr)
        
        Parses (splits) optstr and prepends results to VFS, FS and userspace lists of options.
        """
        pass

    def print_debug(self): # real signature unknown; restored from __doc__
        """ print_debug() """
        pass

    def streq_srcpath(self, srcpath): # real signature unknown; restored from __doc__
        """
        streq_srcpath(srcpath)
        
        Compares fs source path with path. The tailing slash is ignored.
        Returns True if fs source path equal to path, otherwise False.
        """
        pass

    def streq_target(self, target): # real signature unknown; restored from __doc__
        """
        streq_target(target)
        
        Compares fs target path with path. The tailing slash is ignored.
        See also Fs.match_target().
        Returns True if fs target path equal to path, otherwise False.
        """
        pass

    def __init__(self, source=None, root=None, target=None, fstype=None, options=None, attributes=None, freq=0, passno=0): # real signature unknown; restored from __doc__
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    attributes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mount attributes"""

    comment = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab entry comment"""

    devno = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[3]: st_dev"""

    freq = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab[5]: dump frequency in days"""

    fstype = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[9], fstab[3]: filesystem type"""

    fs_options = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[11]: fs-dependent options"""

    id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[1]: ID"""

    options = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab[4]: merged options"""

    opt_fields = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[7]: optional fields"""

    parent = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[2]: parent"""

    passno = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab[6]: pass number on parallel fsck"""

    priority = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """swaps[5]: swap priority"""

    root = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[4]: root of the mount within the FS"""

    size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """saps[3]: swaparea size"""

    source = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab[1], mountinfo[10], swaps[1]: source dev, file, dir or TAG"""

    srcpath = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mount source path or NULL in case of error or when the path is not defined."""

    swaptype = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """swaps[2]: device type"""

    tag = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """(Name, Value)"""

    target = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[5], fstab[2]: mountpoint"""

    tid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """/proc/<tid>/mountinfo, otherwise zero"""

    usedsize = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """swaps[4]: used size"""

    usr_options = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """userspace mount options"""

    vfs_options = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """mountinfo[6]: fs-independent (VFS) options"""



class Table(object):
    """ Table(path=None, errcb=None) """
    def add_fs(self, fs): # real signature unknown; restored from __doc__
        """
        add_fs(fs)
        
        Adds a new entry to tab.
        Returns self or raises an exception in case of an error.
        """
        pass

    def enable_comments(self, enable): # real signature unknown; restored from __doc__
        """
        enable_comments(enable)
        
        Enables parsing of comments.
        
        The initial (intro) file comment is accessible by
        Tab.intro_comment. The intro and the comment of the first fstabentry has to be separated by blank line.  The filesystem comments are
        accessible by Fs.comment. The tailing fstab comment is accessible
        by Tab.trailing_comment.
        
        <informalexample>
        <programlisting>
        #
        # Intro comment
        #
        
        # this comments belongs to the first fs
        LABEL=foo /mnt/foo auto defaults 1 2
        # this comments belongs to the second fs
        LABEL=bar /mnt/bar auto defaults 1 2 
        # tailing comment
        </programlisting>
        </informalexample>
        """
        pass

    def find_devno(self, devno, direction=None): # real signature unknown; restored from __doc__
        """
        find_devno(devno, [direction])
        
        Note that zero could be valid device number for root pseudo filesystem (e.g. tmpfs)
        Returns a tab entry or None
        """
        pass

    def find_mountpoint(self, path, direction=None): # real signature unknown; restored from __doc__
        """
        find_mountpoint(path, [direction])
        
        Returns a tab entry or None.
        """
        pass

    def find_pair(self, source, target, direction=None): # real signature unknown; restored from __doc__
        """
        find_pair(source, target, [direction])
        
        Returns a tab entry or None.
        """
        pass

    def find_source(self, source, direction=None): # real signature unknown; restored from __doc__
        """
        find_source(source, [direction])
        
        Returns a tab entry or None.
        """
        pass

    def find_srcpath(self, srcpath, direction=None): # real signature unknown; restored from __doc__
        """
        find_srcpath(srcpath, [direction])
        
        Try to lookup an entry in given tab, possible are four iterations, first
        with path, second with realpath(path), third with tags (LABEL, UUID, ..)
        from path and fourth with realpath(path) against realpath(entry->srcpath).
        
        The 2nd, 3rd and 4th iterations are not performed when tb cache is not
        set (not implemented yet).
        
        Note that None is a valid source path; it will be replaced with "none". The
        "none" is used in /proc/{mounts,self/mountinfo} for pseudo filesystems.
        
        Returns a tab entry or None.
        """
        pass

    def find_tag(self, tag, val, direction=None): # real signature unknown; restored from __doc__
        """
        find_tag(tag, val, [direction])
        
        Try to lookup an entry in given tab, first attempt is lookup by tag and
        val, for the second attempt the tag is evaluated (converted to the device
        name) and Tab.find_srcpath() is performed. The second attempt is not
        performed when tb cache is not set (not implemented yet).
        
        Returns a tab entry or NULL.
        """
        pass

    def find_target(self, target, direction=None): # real signature unknown; restored from __doc__
        """
        find_target(target, [direction])
        
        Try to lookup an entry in given tab, possible are three iterations, first
        with path, second with realpath(path) and third with realpath(path)
        against realpath(fs->target). The 2nd and 3rd iterations are not performed
        when tb cache is not set (cache not implemented yet).
        
        Returns a tab entry or None.
        """
        pass

    def is_fs_mounted(self, fstab_fs): # real signature unknown; restored from __doc__
        """
        is_fs_mounted(fstab_fs)
        
        Checks if the fstab_fs entry is already in the tb table. The "swap" is
        ignored. This function explicitly compares source, target and root of the
        filesystems.
        
        Note that source and target are canonicalized only if a cache for tb is
        defined (not implemented yet). The target canonicalization may
        trigger automount on autofs mountpoints!
        
        Don't use it if you want to know if a device is mounted, just use
        Tab.find_source() for the device.
        
        This function is designed mostly for "mount -a".
        
        Returns a boolean value.
        """
        pass

    def next_fs(self): # real signature unknown; restored from __doc__
        """
        next_fs()
        
        Returns the next Fs on success, raises an exception in case of an error and None at end of list.
        
        Example:
        <informalexample>
        <programlisting>
        import libmount
        import functools
        for fs in iter(functools.partial(tb.next_fs), None):
            dir = Fs.target
            print "mount point: {:s}".format(dir)
        
        </programlisting>
        </informalexample>
        
        lists all mountpoints from fstab in backward order.
        """
        pass

    def parse_dir(self, dir): # real signature unknown; restored from __doc__
        """
        parse_dir(dir)
        
        The directory:
        - files are sorted by strverscmp(3)
        - files that start with "." are ignored (e.g. ".10foo.fstab")
        - files without the ".fstab" extension are ignored
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def parse_file(self, file): # real signature unknown; restored from __doc__
        """
        parse_file(file)
        
        Parses whole table (e.g. /etc/mtab) and appends new records to the tab.
        
        The libmount parser ignores broken (syntax error) lines, these lines are
        reported to caller by errcb() function (see Tab.parser_errcb).
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def parse_fstab(self, fstab=None): # real signature unknown; restored from __doc__
        """
        parse_fstab([fstab])
        
        This function parses /etc/fstab and appends new lines to the tab. If the
        filename is a directory then Tab.parse_dir() is called.
        
        See also Tab.parser_errcb.
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def parse_mtab(self, mtab=None): # real signature unknown; restored from __doc__
        """
        parse_mtab([mtab])
        
        This function parses /etc/mtab or /proc/self/mountinfo
        /run/mount/utabs or /proc/mounts.
        
        See also Tab.parser_errcb().
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def parse_swaps(self, swaps): # real signature unknown; restored from __doc__
        """
        parse_swaps(swaps)
        
        This function parses /proc/swaps and appends new lines to the tab
        """
        pass

    def remove_fs(self, fs): # real signature unknown; restored from __doc__
        """
        remove_fs(fs)
        
        Returns self or raises an exception in case of an error.
        """
        pass

    def replace_file(self, filename): # real signature unknown; restored from __doc__
        """
        replace_file(filename)
        
        This function replaces filename with the new content from TableObject.
        """
        pass

    def write_file(self, path): # real signature unknown; restored from __doc__
        """
        write_file(path)
        
        This function writes tab to file(stream)
        """
        pass

    def __init__(self, path=None, errcb=None): # real signature unknown; restored from __doc__
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    errcb = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """parser error callback"""

    intro_comment = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab intro comment"""

    nents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """number of valid entries in tab"""

    trailing_comment = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """fstab trailing comment"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18e1400>'

__spec__ = None # (!) real value is "ModuleSpec(name='libmount.pylibmount', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18e1400>, origin='/usr/lib/python3.8/site-packages/libmount/pylibmount.so')"

