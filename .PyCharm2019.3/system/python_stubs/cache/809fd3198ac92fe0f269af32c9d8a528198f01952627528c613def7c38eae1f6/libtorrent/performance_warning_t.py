# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class performance_warning_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    bittyrant_with_no_uplimit = 8
    download_limit_too_low = 3
    names = {
        'bittyrant_with_no_uplimit': 8,
        'download_limit_too_low': 3,
        'outstanding_disk_buffer_limit_reached': 0,
        'outstanding_request_limit_reached': 1,
        'send_buffer_watermark_too_low': 4,
        'too_few_file_descriptors': 10,
        'too_few_outgoing_ports': 9,
        'too_high_disk_queue_limit': 6,
        'too_many_optimistic_unchoke_slots': 5,
        'upload_limit_too_low': 2,
    }
    outstanding_disk_buffer_limit_reached = 0
    outstanding_request_limit_reached = 1
    send_buffer_watermark_too_low = 4
    too_few_file_descriptors = 10
    too_few_outgoing_ports = 9
    too_high_disk_queue_limit = 6
    too_many_optimistic_unchoke_slots = 5
    upload_limit_too_low = 2
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        8: 8,
        9: 9,
        10: 10,
    }
    __slots__ = ()


