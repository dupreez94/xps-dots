# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M33d(__Boost_Python.instance):
    """ M33d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def determinant(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        determinant( (M33d)arg1) -> float :
            determinant() return the determinant of this matrix
        
            C++ signature :
                double determinant(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def equalWithAbsError(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (M33d)arg1, (M33d)arg2, (float)arg3) -> bool :
            m1.equalWithAbsError(m2,e) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(m1[i] - m2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>,double)
        """
        pass

    def equalWithRelError(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (M33d)arg1, (M33d)arg2, (float)arg3) -> bool :
            m1.equalWithAbsError(m2,e) true if the elements of m1 and m2 are the same with an absolute error of no more than e, i.e., abs(m1[i] - m2[i]) <= e * abs(m1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>,double)
        """
        pass

    def extractAndRemoveScalingAndShear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractAndRemoveScalingAndShear( (M33d)arg1, (V2d)arg2, (V2d)arg3 [, (int)arg4]) -> None :
            M.extractAndRemoveScalingAndShear(scl, shr, [exc]) -- extracts the scaling component of M into scl and the shearing component of M into shr.  Also removes the scaling and shearing components from M.  Returns 1 unless the scaling component is nearly 0, in which case 0 is returned. If optional arg. exc == 1, then if the scaling component is nearly 0, then MathExc is thrown. 
        
            C++ signature :
                void extractAndRemoveScalingAndShear(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue} [,int])
        """
        pass

    def extractEuler(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractEuler( (M33d)arg1, (V2d)arg2) -> None :
            M.extractEulerZYX(r) -- extracts the rotation component of M into r. Assumes that M contains no shear or non-uniform scaling; results are meaningless if it does.
        
            C++ signature :
                void extractEuler(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double> {lvalue})
        """
        pass

    def extractScaling(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractScaling( (M33d)arg1, (V2d)arg2 [, (int)arg3]) -> None :
            extract scaling
        
            C++ signature :
                void extractScaling(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double> {lvalue} [,int])
        """
        pass

    def extractScalingAndShear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractScalingAndShear( (M33d)arg1, (V2d)arg2, (V2d)arg3 [, (int)arg4]) -> None :
            extract scaling
        
            C++ signature :
                void extractScalingAndShear(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue} [,int])
        """
        pass

    def extractSHRT(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extractSHRT( (M33d)arg1, (V2d)arg2, (V2d)arg3, (V2d)arg4, (V2d)arg5 [, (int)arg6]) -> int :
            M.extractSHRT(Vs, Vh, Vr, Vt, [exc]) -- extracts the scaling component of M into Vs, the shearing component of M in Vh (as XY, XZ, YZ shear factors), the rotation of M into Vr (as Euler angles in the order XYZ), and the translaation of M into Vt. If optional arg. exc == 1, then if the scaling component is nearly 0, then MathExc is thrown. 
        
            C++ signature :
                int extractSHRT(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue} [,int])
        """
        pass

    def fastMinor(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fastMinor( (M33d)arg1, (int)arg2, (int)arg3, (int)arg4, (int)arg5) -> float :
            fastMinor() return the matrix minor using the specified rows and columns of this matrix
        
            C++ signature :
                double fastMinor(Imath_2_4::Matrix33<double> {lvalue},int,int,int,int)
        """
        pass

    def gjInverse(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        gjInverse( (M33d)arg1 [, (bool)arg2]) -> M33d :
            gjInverse() return a inverted copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> gjInverse(Imath_2_4::Matrix33<double> {lvalue} [,bool])
        """
        pass

    def gjInvert(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        gjInvert( (M33d)arg1 [, (bool)arg2]) -> M33d :
            gjInvert() invert this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> gjInvert(Imath_2_4::Matrix33<double> {lvalue} [,bool])
        """
        pass

    def inverse(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        inverse( (M33d)arg1 [, (bool)arg2]) -> M33d :
            inverse() return a inverted copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> inverse(Imath_2_4::Matrix33<double> {lvalue} [,bool])
        """
        pass

    def invert(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        invert( (M33d)arg1 [, (bool)arg2]) -> M33d :
            invert() invert this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> invert(Imath_2_4::Matrix33<double> {lvalue} [,bool])
        """
        pass

    def makeIdentity(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeIdentity( (M33d)arg1) -> None :
            makeIdentity() make this matrix the identity matrix
        
            C++ signature :
                void makeIdentity(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def minorOf(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        minorOf( (M33d)arg1, (int)arg2, (int)arg3) -> float :
            minorOf() return the matrix minor of the (row,col) element of this matrix
        
            C++ signature :
                double minorOf(Imath_2_4::Matrix33<double> {lvalue},int,int)
        """
        pass

    def multDirMatrix(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        multDirMatrix( (M33d)arg1, (V2d)arg2, (V2d)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double> {lvalue})
        
        multDirMatrix( (M33d)arg1, (V2d)arg2) -> V2d :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec2<double> multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        multDirMatrix( (M33d)arg1, (V2dArray)arg2) -> V2dArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        multDirMatrix( (M33d)arg1, (V2f)arg2, (V2f)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float> {lvalue})
        
        multDirMatrix( (M33d)arg1, (V2f)arg2) -> V2f :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec2<float> multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<float>)
        
        multDirMatrix( (M33d)arg1, (V2fArray)arg2) -> V2fArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > multDirMatrix(Imath_2_4::Matrix33<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def multVecMatrix(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        multVecMatrix( (M33d)arg1, (V2d)arg2, (V2d)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double> {lvalue})
        
        multVecMatrix( (M33d)arg1, (V2d)arg2) -> V2d :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec2<double> multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        multVecMatrix( (M33d)arg1, (V2dArray)arg2) -> V2dArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        multVecMatrix( (M33d)arg1, (V2f)arg2, (V2f)arg3) -> None :
            mult matrix
        
            C++ signature :
                void multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float> {lvalue})
        
        multVecMatrix( (M33d)arg1, (V2f)arg2) -> V2f :
            mult matrix
        
            C++ signature :
                Imath_2_4::Vec2<float> multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<float>)
        
        multVecMatrix( (M33d)arg1, (V2fArray)arg2) -> V2fArray :
            mult matrix
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > multVecMatrix(Imath_2_4::Matrix33<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def negate(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (M33d)arg1) -> M33d :
            negate() negate all entries in this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> negate(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def outerProduct(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        outerProduct( (M33d)arg1, (V3d)arg2, (V3d)arg3) -> None :
            M.outerProduct(Va,Vb) -- Performs the outer product, or tensor product, of two 3D vectors, Va and Vb
        
            C++ signature :
                void outerProduct(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def removeScaling(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        removeScaling( (M33d)arg1 [, (int)arg2]) -> int :
            remove scaling
        
            C++ signature :
                int removeScaling(Imath_2_4::Matrix33<double> {lvalue} [,int])
        """
        pass

    def removeScalingAndShear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        removeScalingAndShear( (M33d)arg1 [, (int)arg2]) -> int :
            remove scaling
        
            C++ signature :
                int removeScalingAndShear(Imath_2_4::Matrix33<double> {lvalue} [,int])
        """
        pass

    def rotate(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rotate( (M33d)arg1, (float)arg2) -> M33d :
            rotate matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> rotate(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def sansScaling(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        sansScaling( (M33d)arg1 [, (bool)arg2]) -> M33d :
            sans scaling
        
            C++ signature :
                Imath_2_4::Matrix33<double> sansScaling(Imath_2_4::Matrix33<double> [,bool])
        """
        pass

    def sansScalingAndShear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        sansScalingAndShear( (M33d)arg1 [, (bool)arg2]) -> M33d :
            sans scaling and shear
        
            C++ signature :
                Imath_2_4::Matrix33<double> sansScalingAndShear(Imath_2_4::Matrix33<double> [,bool])
        """
        pass

    def scale(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        scale( (M33d)arg1, (float)arg2) -> M33d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> scale(Imath_2_4::Matrix33<double> {lvalue},double)
        
        scale( (M33d)arg1, (V2d)arg2) -> M33d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> scale(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        scale( (M33d)arg1, (tuple)arg2) -> M33d :
            scale matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> scale(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        """
        pass

    def setRotation(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setRotation( (M33d)arg1, (float)arg2) -> M33d :
            setRotation()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setRotation(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def setScale(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setScale( (M33d)arg1, (float)arg2) -> M33d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setScale(Imath_2_4::Matrix33<double> {lvalue},double)
        
        setScale( (M33d)arg1, (V2d)arg2) -> M33d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setScale(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        setScale( (M33d)arg1, (tuple)arg2) -> M33d :
            setScale()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setScale(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        """
        pass

    def setShear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setShear( (M33d)arg1, (float)arg2) -> M33d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setShear(Imath_2_4::Matrix33<double> {lvalue},double)
        
        setShear( (M33d)arg1, (V2d)arg2) -> M33d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setShear(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        setShear( (M33d)arg1, (tuple)arg2) -> M33d :
            setShear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setShear(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        """
        pass

    def setTranslation(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setTranslation( (M33d)arg1, (V2d)arg2) -> M33d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setTranslation(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        setTranslation( (M33d)arg1, (tuple)arg2) -> M33d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setTranslation(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        
        setTranslation( (M33d)arg1, (object)arg2) -> M33d :
            setTranslation()
        
            C++ signature :
                Imath_2_4::Matrix33<double> setTranslation(Imath_2_4::Matrix33<double> {lvalue},boost::python::api::object)
        """
        pass

    def setValue(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (M33d)arg1, (M33d)arg2) -> None :
            setValue()
        
            C++ signature :
                void setValue(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def shear(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        shear( (M33d)arg1, (float)arg2) -> M33d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> shear(Imath_2_4::Matrix33<double> {lvalue},double)
        
        shear( (M33d)arg1, (V2d)arg2) -> M33d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> shear(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Vec2<double>)
        
        shear( (M33d)arg1, (tuple)arg2) -> M33d :
            shear()
        
            C++ signature :
                Imath_2_4::Matrix33<double> shear(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        """
        pass

    def singularValueDecomposition(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        singularValueDecomposition( (M33d)matrix, (bool)forcePositiveDeterminant) -> tuple :
            Decomposes the matrix using the singular value decomposition (SVD) into three
            matrices U, S, and V which have the following properties: 
              1. U and V are both orthonormal matrices, 
              2. S is the diagonal matrix of singular values, 
              3. U * S * V.transposed() gives back the original matrix.
            The result is returned as a tuple [U, S, V].  Note that since S is diagonal we
            don't need to return the entire matrix, so we return it as a three-vector.  
            
            The 'forcePositiveDeterminant' argument can be used to force the U and V^T to
            have positive determinant (that is, to be proper rotation matrices); if
            forcePositiveDeterminant is False, then the singular values are guaranteed to
            be nonnegative but the U and V matrices might contain negative scale along one
            of the axes; if forcePositiveDeterminant is True, then U and V cannot contain
            negative scale but S[2] might be negative.  
            
            Our SVD implementation uses two-sided Jacobi rotations to iteratively
            diagonalize the matrix, which should be quite robust and significantly faster
            than the more general SVD solver in LAPACK.  
            
        
            C++ signature :
                boost::python::tuple singularValueDecomposition(Imath_2_4::Matrix33<double>,bool)
        """
        pass

    def symmetricEigensolve(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        symmetricEigensolve( (M33d)arg1) -> tuple :
            Decomposes the matrix A using a symmetric eigensolver into matrices Q and S 
            which have the following properties: 
              1. Q is the orthonormal matrix of eigenvectors, 
              2. S is the diagonal matrix of eigenvalues, 
              3. Q * S * Q.transposed() gives back the original matrix.
            
            IMPORTANT: It is vital that the passed-in matrix be symmetric, or the result 
            won't make any sense.  This function will return an error if passed an 
            unsymmetric matrix.
            
            The result is returned as a tuple [Q, S].  Note that since S is diagonal 
            we don't need to return the entire matrix, so we return it as a three-vector. 
            
            Our eigensolver implementation uses one-sided Jacobi rotations to iteratively 
            diagonalize the matrix, which should be quite robust and significantly faster 
            than the more general symmetric eigenvalue solver in LAPACK.  
            
        
            C++ signature :
                boost::python::tuple symmetricEigensolve(Imath_2_4::Matrix33<double>)
        """
        pass

    def translate(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        translate( (M33d)arg1, (object)arg2) -> M33d :
            translate()
        
            C++ signature :
                Imath_2_4::Matrix33<double> translate(Imath_2_4::Matrix33<double> {lvalue},boost::python::api::object)
        
        translate( (M33d)arg1, (tuple)arg2) -> M33d :
            translate()
        
            C++ signature :
                Imath_2_4::Matrix33<double> translate(Imath_2_4::Matrix33<double> {lvalue},boost::python::tuple)
        """
        pass

    def translation(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        translation( (M33d)arg1) -> V2d :
            translation()
        
            C++ signature :
                Imath_2_4::Vec2<double> translation(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def transpose(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        transpose( (M33d)arg1) -> M33d :
            transpose() transpose this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> transpose(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def transposed(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        transposed( (M33d)arg1) -> M33d :
            transposed() return a transposed copy of this matrix
        
            C++ signature :
                Imath_2_4::Matrix33<double> transposed(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def __add__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __add__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        __add__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __add__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __copy__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (M33d)arg1) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __copy__(Imath_2_4::Matrix33<double>)
        """
        pass

    def __deepcopy__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (M33d)arg1, (dict)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __deepcopy__(Imath_2_4::Matrix33<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __div__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __eq__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (M33d)arg1, (M33d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __getitem__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M33d)arg1, (int)arg2) -> M33dRow :
        
            C++ signature :
                PyImath::MatrixRow<double, 3> __getitem__(Imath_2_4::Matrix33<double> {lvalue},long)
        """
        pass

    def __ge__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (M33d)arg1, (M33d)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __gt__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (M33d)arg1, (M33d)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __iadd__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (M33d)arg1, (M33f)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __iadd__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<float>)
        
        __iadd__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __iadd__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        __iadd__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __iadd__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __idiv__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __idiv__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __imul__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __imul__(Imath_2_4::Matrix33<double> {lvalue},double)
        
        __imul__( (M33d)arg1, (M33f)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __imul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<float> {lvalue})
        
        __imul__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __imul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (M33d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Matrix33<double>)
        
        __init__( (object)arg1) -> None :
            initialize to identity
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (float)arg2) -> None :
            initialize all entries to a single value
        
            C++ signature :
                void __init__(_object*,double)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (float)arg8, (float)arg9, (float)arg10) -> None :
            make from components
        
            C++ signature :
                void __init__(_object*,double,double,double,double,double,double,double,double,double)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (M33f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<float>)
        
        __init__( (object)arg1, (M33d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<double>)
        """
        pass

    def __isub__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (M33d)arg1, (M33f)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __isub__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<float>)
        
        __isub__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __isub__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        __isub__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __isub__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __itruediv__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __itruediv__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __len__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M33d)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Matrix33<double>)
        """
        pass

    def __le__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (M33d)arg1, (M33d)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __lt__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (M33d)arg1, (M33d)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __mul__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __mul__(Imath_2_4::Matrix33<double> {lvalue},double)
        
        __mul__( (M33d)arg1, (M33f)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __mul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<float> {lvalue})
        
        __mul__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __mul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def __neg__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (M33d)arg1) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __neg__(Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def __ne__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (M33d)arg1, (M33d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __radd__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __radd__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (M33d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Matrix33<double>)
        """
        pass

    def __rmul__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __rmul__(Imath_2_4::Matrix33<double> {lvalue},double)
        
        __rmul__( (M33d)arg1, (M33f)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __rmul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<float> {lvalue})
        
        __rmul__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __rmul__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double> {lvalue})
        """
        pass

    def __rsub__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __rsub__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __str__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (M33d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Matrix33<double>)
        """
        pass

    def __sub__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (M33d)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __sub__(Imath_2_4::Matrix33<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        __sub__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __sub__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    def __truediv__(self, M33d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (M33d)arg1, (float)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __truediv__(Imath_2_4::Matrix33<double> {lvalue},double)
        """
        pass

    __instance_size__ = 88


