# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2f(__Boost_Python.instance):
    """ V2f """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                float baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                float baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                float baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                float baseTypeSmallest()
        """
        return 0.0

    def closestVertex(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestVertex( (V2f)arg1, (V2f)arg2, (V2f)arg3, (V2f)arg4) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> closestVertex(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    def cross(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2f)arg1, (V2f)arg2) -> float :
            v1.cross(v2) right handed cross product
        
            C++ signature :
                float cross(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        cross( (V2f)arg1, (V2fArray)arg2) -> FloatArray :
            v1.cross(v2) right handed array cross product
        
            C++ signature :
                PyImath::FixedArray<float> cross(Imath_2_4::Vec2<float>,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2f)arg1, (V2f)arg2) -> float :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                float dot(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        dot( (V2f)arg1, (V2fArray)arg2) -> FloatArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<float> dot(Imath_2_4::Vec2<float>,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def equalWithAbsError(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V2f)arg1, (V2f)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>,float)
        
        equalWithAbsError( (V2f)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec2<float>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V2f)arg1, (V2f)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>,float)
        
        equalWithRelError( (V2f)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec2<float>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2f)arg1) -> float :
            length() magnitude of the vector
        
            C++ signature :
                float length(Imath_2_4::Vec2<float>)
        """
        pass

    def length2(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2f)arg1) -> float :
            length2() square magnitude of the vector
        
            C++ signature :
                float length2(Imath_2_4::Vec2<float>)
        """
        pass

    def negate(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V2f)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> negate(Imath_2_4::Vec2<float> {lvalue})
        """
        pass

    def normalize(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2f)arg1) -> V2f :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec2<float> normalize(Imath_2_4::Vec2<float> {lvalue})
        """
        pass

    def normalized(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2f)arg1) -> V2f :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec2<float> normalized(Imath_2_4::Vec2<float>)
        """
        pass

    def normalizedExc(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V2f)arg1) -> V2f :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec2<float> normalizedExc(Imath_2_4::Vec2<float>)
        """
        pass

    def normalizedNonNull(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V2f)arg1) -> V2f :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec2<float> normalizedNonNull(Imath_2_4::Vec2<float>)
        """
        pass

    def normalizeExc(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V2f)arg1) -> V2f :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec2<float> normalizeExc(Imath_2_4::Vec2<float> {lvalue})
        """
        pass

    def normalizeNonNull(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V2f)arg1) -> V2f :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec2<float> normalizeNonNull(Imath_2_4::Vec2<float> {lvalue})
        """
        pass

    def orthogonal(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> orthogonal(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    def project(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> project(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    def reflect(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> reflect(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    def setValue(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V2f)arg1, (float)arg2, (float)arg3) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec2<float> {lvalue},float,float)
        """
        pass

    def __add__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __add__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<int>)
        
        __add__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __add__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<double>)
        
        __add__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,float)
        
        __add__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __add__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __add__(Imath_2_4::Vec2<float>,boost::python::list)
        """
        pass

    def __copy__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2f)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __copy__(Imath_2_4::Vec2<float>)
        """
        pass

    def __deepcopy__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2f)arg1, (dict)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __deepcopy__(Imath_2_4::Vec2<float>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<int> {lvalue})
        
        __div__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float> {lvalue})
        
        __div__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<double> {lvalue})
        
        __div__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __div__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float>,boost::python::list)
        
        __div__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __div__(Imath_2_4::Vec2<float>,float)
        """
        pass

    def __eq__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2f)arg1, (V2f)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __eq__( (V2f)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec2<float>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2f)arg1, (int)arg2) -> float :
        
            C++ signature :
                float {lvalue} __getitem__(Imath_2_4::Vec2<float> {lvalue},long)
        """
        pass

    def __ge__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V2f)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec2<float>,boost::python::api::object)
        """
        pass

    def __gt__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V2f)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec2<float>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __iadd__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<int>)
        
        __iadd__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __iadd__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __iadd__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __iadd__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __idiv__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2f)arg1, (object)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __idiv__(Imath_2_4::Vec2<float> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<int>)
        
        __imul__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __imul__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<double>)
        
        __imul__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},float)
        
        __imul__( (V2f)arg1, (M33f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Matrix33<float>)
        
        __imul__( (V2f)arg1, (M33d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __imul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V2f)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<float>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __isub__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<int>)
        
        __isub__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __isub__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __isub__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __isub__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __itruediv__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2f)arg1, (object)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __itruediv__(Imath_2_4::Vec2<float> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2f)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec2<float>)
        """
        pass

    def __le__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V2f)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec2<float>,boost::python::api::object)
        """
        pass

    def __lt__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V2f)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec2<float>,boost::python::api::object)
        """
        pass

    def __mod__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (V2f)arg1, (V2f)arg2) -> float :
        
            C++ signature :
                float __mod__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    def __mul__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<int>)
        
        __mul__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __mul__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<double>)
        
        __mul__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,float)
        
        __mul__( (V2f)arg1, (FloatArray)arg2) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __mul__(Imath_2_4::Vec2<float>,PyImath::FixedArray<float>)
        
        __mul__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __mul__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float>,boost::python::list)
        
        __mul__( (V2f)arg1, (V2f)arg2) -> object :
        
            C++ signature :
                _object* __mul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __mul__( (V2f)arg1, (M33f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Matrix33<float>)
        
        __mul__( (V2f)arg1, (M33d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __mul__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __neg__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2f)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __neg__(Imath_2_4::Vec2<float>)
        """
        pass

    def __ne__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2f)arg1, (V2f)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __ne__( (V2f)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec2<float>,boost::python::tuple)
        """
        pass

    def __radd__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __radd__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __radd__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __radd__(Imath_2_4::Vec2<float>,float)
        
        __radd__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __radd__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __radd__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __radd__(Imath_2_4::Vec2<float>,boost::python::list)
        """
        pass

    def __rdiv__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rdiv__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __rdiv__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rdiv__(Imath_2_4::Vec2<float>,boost::python::list)
        
        __rdiv__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rdiv__(Imath_2_4::Vec2<float>,float)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V2f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec2<float>)
        """
        pass

    def __rmul__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rmul__(Imath_2_4::Vec2<float> {lvalue},float)
        
        __rmul__( (V2f)arg1, (FloatArray)arg2) -> V2fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<float> > __rmul__(Imath_2_4::Vec2<float>,PyImath::FixedArray<float>)
        
        __rmul__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rmul__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __rmul__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rmul__(Imath_2_4::Vec2<float>,boost::python::list)
        """
        pass

    def __rsub__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rsub__(Imath_2_4::Vec2<float>,float)
        
        __rsub__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rsub__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __rsub__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __rsub__(Imath_2_4::Vec2<float>,boost::python::list)
        """
        pass

    def __setitem__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2f)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec2<float> {lvalue},long,float)
        """
        pass

    def __str__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V2f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec2<float>)
        """
        pass

    def __sub__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __sub__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<int>)
        
        __sub__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __sub__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<double>)
        
        __sub__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,float)
        
        __sub__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __sub__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __sub__(Imath_2_4::Vec2<float>,boost::python::list)
        """
        pass

    def __truediv__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2f)arg1, (V2f)arg2) -> object :
        
            C++ signature :
                _object* __truediv__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float>)
        
        __truediv__( (V2f)arg1, (V2i)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<int> {lvalue})
        
        __truediv__( (V2f)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<float> {lvalue})
        
        __truediv__( (V2f)arg1, (V2d)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float> {lvalue},Imath_2_4::Vec2<double> {lvalue})
        
        __truediv__( (V2f)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float>,boost::python::tuple)
        
        __truediv__( (V2f)arg1, (list)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float>,boost::python::list)
        
        __truediv__( (V2f)arg1, (float)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> __truediv__(Imath_2_4::Vec2<float>,float)
        """
        pass

    def __xor__(self, V2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V2f)arg1, (V2f)arg2) -> float :
        
            C++ signature :
                float __xor__(Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 24


