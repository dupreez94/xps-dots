# encoding: utf-8
# module iex
# from /usr/lib/python3.8/site-packages/iex.so
# by generator 1.147
# no doc
# no imports

# functions

def testArgExcString(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    testArgExcString( (object)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > testArgExcString(Iex_2_4::ArgExc)
    """
    pass

def testBaseExcString(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    testBaseExcString( (object)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > testBaseExcString(Iex_2_4::BaseExc)
    """
    pass

def testCxxExceptions(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    testCxxExceptions( (int)arg1) -> None :
    
        C++ signature :
            void testCxxExceptions(int)
    """
    pass

def testMakeArgExc(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    testMakeArgExc( (str)arg1) -> object :
    
        C++ signature :
            Iex_2_4::ArgExc testMakeArgExc(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    """
    pass

def testMakeBaseExc(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    testMakeBaseExc( (str)arg1) -> object :
    
        C++ signature :
            Iex_2_4::BaseExc testMakeBaseExc(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    """
    pass

# classes

from .BaseExc import BaseExc
from .ArgExc import ArgExc
from .ErrnoExc import ErrnoExc
from .E2bigExc import E2bigExc
from .EaccesExc import EaccesExc
from .EaddrinuseExc import EaddrinuseExc
from .EaddrnotavailExc import EaddrnotavailExc
from .EadvExc import EadvExc
from .EafnosupportExc import EafnosupportExc
from .EagainExc import EagainExc
from .EalreadyExc import EalreadyExc
from .EbadeExc import EbadeExc
from .EbadfdExc import EbadfdExc
from .EbadfExc import EbadfExc
from .EbadmsgExc import EbadmsgExc
from .EbadrExc import EbadrExc
from .EbadrqcExc import EbadrqcExc
from .EbadsltExc import EbadsltExc
from .EbfontExc import EbfontExc
from .EbufsizeExc import EbufsizeExc
from .EbusyExc import EbusyExc
from .EcanceledExc import EcanceledExc
from .EcantextentExc import EcantextentExc
from .EchildExc import EchildExc
from .EchrngExc import EchrngExc
from .EclockcpuExc import EclockcpuExc
from .EcommExc import EcommExc
from .EconnabortedExc import EconnabortedExc
from .EconnrefusedExc import EconnrefusedExc
from .EconnresetExc import EconnresetExc
from .EcontrollerExc import EcontrollerExc
from .EdeadlkExc import EdeadlkExc
from .EdeadlockExc import EdeadlockExc
from .EdestaddrreqExc import EdestaddrreqExc
from .EdestroyedExc import EdestroyedExc
from .EdircorruptedExc import EdircorruptedExc
from .EdisjointExc import EdisjointExc
from .EdomExc import EdomExc
from .EdquotExc import EdquotExc
from .EemptyExc import EemptyExc
from .EendofminorExc import EendofminorExc
from .EenqueuedExc import EenqueuedExc
from .EexistExc import EexistExc
from .EfaultExc import EfaultExc
from .EfbigExc import EfbigExc
from .EgrouploopExc import EgrouploopExc
from .EhostdownExc import EhostdownExc
from .EhostunreachExc import EhostunreachExc
from .EidrmExc import EidrmExc
from .EilseqExc import EilseqExc
from .EinitExc import EinitExc
from .EinprogressExc import EinprogressExc
from .EintrExc import EintrExc
from .EinvalExc import EinvalExc
from .EinvalmodeExc import EinvalmodeExc
from .EinvalstateExc import EinvalstateExc
from .EinvaltimeExc import EinvaltimeExc
from .EioExc import EioExc
from .EioresidExc import EioresidExc
from .EisconnExc import EisconnExc
from .EisdirExc import EisdirExc
from .EisnamExc import EisnamExc
from .EjoinedExc import EjoinedExc
from .El2hltExc import El2hltExc
from .El2nsyncExc import El2nsyncExc
from .El3hltExc import El3hltExc
from .El3rstExc import El3rstExc
from .ElibaccExc import ElibaccExc
from .ElibbadExc import ElibbadExc
from .ElibexecExc import ElibexecExc
from .ElibmaxExc import ElibmaxExc
from .ElibscnExc import ElibscnExc
from .ElnrngExc import ElnrngExc
from .EloginlimExc import EloginlimExc
from .EloopExc import EloopExc
from .EmfileExc import EmfileExc
from .EmlinkExc import EmlinkExc
from .EmsgsizeExc import EmsgsizeExc
from .EmultihopExc import EmultihopExc
from .EmustrunExc import EmustrunExc
from .EnametoolongExc import EnametoolongExc
from .EnavailExc import EnavailExc
from .EnetdownExc import EnetdownExc
from .EnetresetExc import EnetresetExc
from .EnetunreachExc import EnetunreachExc
from .EnfileExc import EnfileExc
from .EnfsremoteExc import EnfsremoteExc
from .EnoanoExc import EnoanoExc
from .EnoattachExc import EnoattachExc
from .EnoattrExc import EnoattrExc
from .EnobufsExc import EnobufsExc
from .EnocsiExc import EnocsiExc
from .EnodataExc import EnodataExc
from .EnodevExc import EnodevExc
from .EnoentExc import EnoentExc
from .EnoexecExc import EnoexecExc
from .EnoexistExc import EnoexistExc
from .EnointrgroupExc import EnointrgroupExc
from .EnolckExc import EnolckExc
from .EnolimfileExc import EnolimfileExc
from .EnolinkExc import EnolinkExc
from .EnologinExc import EnologinExc
from .EnomemExc import EnomemExc
from .EnomsgExc import EnomsgExc
from .EnonetExc import EnonetExc
from .EnopkgExc import EnopkgExc
from .EnoprocExc import EnoprocExc
from .EnoprotooptExc import EnoprotooptExc
from .EnospcExc import EnospcExc
from .EnosrExc import EnosrExc
from .EnostrExc import EnostrExc
from .EnosysExc import EnosysExc
from .EnotblkExc import EnotblkExc
from .EnotconnExc import EnotconnExc
from .EnotcontrollerExc import EnotcontrollerExc
from .EnotdirExc import EnotdirExc
from .EnotemptyExc import EnotemptyExc
from .EnotenqueuedExc import EnotenqueuedExc
from .EnotjoinedExc import EnotjoinedExc
from .EnotnamExc import EnotnamExc
from .EnotsockExc import EnotsockExc
from .EnotstoppedExc import EnotstoppedExc
from .EnotsupExc import EnotsupExc
from .EnottyExc import EnottyExc
from .EnotuniqExc import EnotuniqExc
from .EnxioExc import EnxioExc
from .EopnotsuppExc import EopnotsuppExc
from .EoverflowExc import EoverflowExc
from .EpermExc import EpermExc
from .EpfnosupportExc import EpfnosupportExc
from .EpipeExc import EpipeExc
from .EproclimExc import EproclimExc
from .EprotoExc import EprotoExc
from .EprotonosupportExc import EprotonosupportExc
from .EprototypeExc import EprototypeExc
from .ErangeExc import ErangeExc
from .EremchgExc import EremchgExc
from .EremdevExc import EremdevExc
from .EremoteExc import EremoteExc
from .EremoteioExc import EremoteioExc
from .ErestartExc import ErestartExc
from .ErofsExc import ErofsExc
from .EshutdownExc import EshutdownExc
from .EsocktnosupportExc import EsocktnosupportExc
from .EspipeExc import EspipeExc
from .EsrchExc import EsrchExc
from .EsrmntExc import EsrmntExc
from .EstaleExc import EstaleExc
from .EstrpipeExc import EstrpipeExc
from .EtimedoutExc import EtimedoutExc
from .EtimeExc import EtimeExc
from .EtoomanyrefsExc import EtoomanyrefsExc
from .EtxtbsyExc import EtxtbsyExc
from .EucleanExc import EucleanExc
from .EunatchExc import EunatchExc
from .EusersExc import EusersExc
from .ExdevExc import ExdevExc
from .ExfullExc import ExfullExc
from .InputExc import InputExc
from .IoExc import IoExc
from .LogicExc import LogicExc
from .MathExc import MathExc
from .NoImplExc import NoImplExc
from .NullExc import NullExc
from .TypeExc import TypeExc
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='iex', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/iex.so')"

