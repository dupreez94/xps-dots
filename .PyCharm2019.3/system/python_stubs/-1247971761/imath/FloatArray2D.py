# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class FloatArray2D(__Boost_Python.instance):
    """ Fixed length 2D array of floats """
    def ifelse(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (FloatArray2D)arg1, (IntArray2D)arg2, (float)arg3) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> ifelse(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>,float)
        
        ifelse( (FloatArray2D)arg1, (IntArray2D)arg2, (FloatArray2D)arg3) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> ifelse(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<float>)
        """
        pass

    def item(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        item( (FloatArray2D)arg1, (int)arg2, (int)arg3) -> float :
        
            C++ signature :
                float item(PyImath::FixedArray2D<float> {lvalue},long,long)
        """
        pass

    def size(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (FloatArray2D)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple size(PyImath::FixedArray2D<float> {lvalue})
        """
        pass

    def __add__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __add__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __add__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __add__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __div__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __div__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __div__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __div__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __eq__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __eq__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __getitem__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (FloatArray2D)arg1, (object)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __getitem__(PyImath::FixedArray2D<float> {lvalue},_object*)
        
        __getitem__( (FloatArray2D)arg1, (IntArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __getitem__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>)
        """
        pass

    def __ge__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __ge__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __gt__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __gt__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __iadd__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __iadd__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __iadd__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __iadd__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __idiv__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __idiv__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __idiv__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __idiv__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __imul__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __imul__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __imul__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __imul__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long,unsigned long)
        
        __init__( (object)arg1, (FloatArray2D)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<float>)
        
        __init__( (object)arg1, (float)arg2, (int)arg3, (int)arg4) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,float,unsigned long,unsigned long)
        
        __init__( (object)arg1, (IntArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<int>)
        
        __init__( (object)arg1, (DoubleArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<double>)
        """
        pass

    def __ipow__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __ipow__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __ipow__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __ipow__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __isub__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __isub__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __isub__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __isub__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __itruediv__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __itruediv__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<float>)
        
        __itruediv__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> {lvalue} __itruediv__(PyImath::FixedArray2D<float> {lvalue},float)
        """
        pass

    def __len__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (FloatArray2D)arg1) -> int :
        
            C++ signature :
                unsigned long __len__(PyImath::FixedArray2D<float> {lvalue})
        """
        pass

    def __le__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __le__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __lt__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __lt__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __mul__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __mul__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __mul__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __mul__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __neg__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (FloatArray2D)arg1) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __neg__(PyImath::FixedArray2D<float>)
        """
        pass

    def __ne__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __ne__( (FloatArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __pow__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __pow__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __pow__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __pow__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __radd__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __radd__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __rmul__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __rpow__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rpow__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __rpow__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __rsub__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __rsub__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __setitem__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (FloatArray2D)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},_object*,float)
        
        __setitem__( (FloatArray2D)arg1, (IntArray2D)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>,float)
        
        __setitem__( (FloatArray2D)arg1, (object)arg2, (FloatArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},_object*,PyImath::FixedArray2D<float>)
        
        __setitem__( (FloatArray2D)arg1, (IntArray2D)arg2, (FloatArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<float>)
        
        __setitem__( (FloatArray2D)arg1, (object)arg2, (FloatArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},_object*,PyImath::FixedArray<float>)
        
        __setitem__( (FloatArray2D)arg1, (IntArray2D)arg2, (FloatArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<float> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray<float>)
        """
        pass

    def __sub__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __sub__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __sub__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __sub__(PyImath::FixedArray2D<float>,float)
        """
        pass

    def __truediv__(self, FloatArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (FloatArray2D)arg1, (FloatArray2D)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __truediv__(PyImath::FixedArray2D<float>,PyImath::FixedArray2D<float>)
        
        __truediv__( (FloatArray2D)arg1, (float)arg2) -> FloatArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<float> __truediv__(PyImath::FixedArray2D<float>,float)
        """
        pass

    __instance_size__ = 72


