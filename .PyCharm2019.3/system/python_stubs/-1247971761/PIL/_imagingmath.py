# encoding: utf-8
# module PIL._imagingmath
# from /usr/lib/python3.8/site-packages/PIL/_imagingmath.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

abs_F = 140495651941952
abs_I = 140495651939488

add_F = 140495651942688
add_I = 140495651939680

and_I = 140495651940496

diff_F = 140495651943968
diff_I = 140495651940272

div_F = 140495651943840
div_I = 140495651940016

eq_F = 140495651945152
eq_I = 140495651941280

ge_F = 140495651947504
ge_I = 140495651941840

gt_F = 140495651947072
gt_I = 140495651941728

invert_I = 140495651940400

le_F = 140495651946592
le_I = 140495651941616

lshift_I = 140495651940832

lt_F = 140495651946112
lt_I = 140495651941504

max_F = 140495651944768
max_I = 140495651941168

min_F = 140495651944384
min_I = 140495651941056

mod_F = 140495651948192
mod_I = 140495651940144

mul_F = 140495651943456
mul_I = 140495651939904

neg_F = 140495651942320
neg_I = 140495651939584

ne_F = 140495651945616
ne_I = 140495651941392

or_I = 140495651940608

pow_F = 140495651948480
pow_I = 140495651947936

rshift_I = 140495651940944

sub_F = 140495651943072
sub_I = 140495651939792

xor_I = 140495651940720

# functions

def binop(*args, **kwargs): # real signature unknown
    pass

def unop(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>'

__spec__ = None # (!) real value is "ModuleSpec(name='PIL._imagingmath', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>, origin='/usr/lib/python3.8/site-packages/PIL/_imagingmath.cpython-38-x86_64-linux-gnu.so')"

