# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class QuatdArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Quat """
    def angle(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        angle( (QuatdArray)arg1) -> DoubleArray :
            get rotation angle about the axis returned by axis() for each quat
        
            C++ signature :
                PyImath::FixedArray<double> angle(PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def axis(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        axis( (QuatdArray)arg1) -> V3dArray :
            get rotation axis for each quat
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > axis(PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def ifelse(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (QuatdArray)arg1, (IntArray)arg2, (Quatd)arg3) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > ifelse(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Quat<double>)
        
        ifelse( (QuatdArray)arg1, (IntArray)arg2, (QuatdArray)arg3) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > ifelse(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def orientToVectors(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orientToVectors( (QuatdArray)arg1, (V3dArray)forward, (V3dArray)up, (bool)alignForward) -> None :
            Sets the orientations to match the given forward and up vectors, matching the forward vector exactly if 'alignForward' is True, matching the up vector exactly if 'alignForward' is False.  If the vectors are already orthogonal, both vectors will be matched exactly.
        
            C++ signature :
                void orientToVectors(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >,PyImath::FixedArray<Imath_2_4::Vec3<double> >,bool)
        """
        pass

    def setAxisAngle(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setAxisAngle( (QuatdArray)arg1, (V3dArray)axis, (DoubleArray)angle) -> None :
            set the quaternion arrays from a given axis and angle
        
            C++ signature :
                void setAxisAngle(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >,PyImath::FixedArray<double>)
        """
        pass

    def setEulerXYZ(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setEulerXYZ( (QuatdArray)arg1, (V3dArray)euler) -> None :
            set the quaternion arrays from a given euler XYZ angle vector
        
            C++ signature :
                void setEulerXYZ(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def setRotation(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setRotation( (QuatdArray)arg1, (V3dArray)from, (V3dArray)to) -> None :
            set rotation angles for each quat
        
            C++ signature :
                void setRotation(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __copy__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (QuatdArray)arg1) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > __copy__(PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def __deepcopy__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (QuatdArray)arg1, (dict)arg2) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Quat<double> >,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (QuatdArray)arg1, (Quatd)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},Imath_2_4::Quat<double>)
        
        __eq__( (QuatdArray)arg1, (QuatdArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def __getitem__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (QuatdArray)arg1, (object)arg2) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},_object*)
        
        __getitem__( (QuatdArray)arg1, (IntArray)arg2) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (QuatdArray)arg1, (int)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __getitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},long)
        
        __getitem__( (QuatdArray)arg1, (int)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (QuatdArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        
        __init__( (object)arg1, (Quatd)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<double>,unsigned long)
        
        __init__( (object)arg1, (EulerdArray)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        
        __init__( (object)arg1, (QuatfArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def __len__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (QuatdArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue})
        """
        pass

    def __mul__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (QuatdArray)arg1, (QuatdArray)arg2) -> QuatdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<double> > __mul__(PyImath::FixedArray<Imath_2_4::Quat<double> >,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def __ne__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (QuatdArray)arg1, (Quatd)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},Imath_2_4::Quat<double>)
        
        __ne__( (QuatdArray)arg1, (QuatdArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (QuatdArray)arg1, (V3d)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Quat<double> >,Imath_2_4::Vec3<double>)
        
        __rmul__( (QuatdArray)arg1, (V3dArray)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Quat<double> >,PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __setitem__(self, QuatdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (QuatdArray)arg1, (object)arg2, (Quatd)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},_object*,Imath_2_4::Quat<double>)
        
        __setitem__( (QuatdArray)arg1, (IntArray)arg2, (Quatd)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Quat<double>)
        
        __setitem__( (QuatdArray)arg1, (object)arg2, (QuatdArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        
        __setitem__( (QuatdArray)arg1, (IntArray)arg2, (QuatdArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


