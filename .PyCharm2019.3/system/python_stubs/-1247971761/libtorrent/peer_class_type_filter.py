# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class peer_class_type_filter(__Boost_Python.instance):
    # no doc
    def add(self, peer_class_type_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add( (peer_class_type_filter)arg1, (peer_class_type_filter_socket_type_t)arg2, (object)arg3) -> None :
        
            C++ signature :
                void add(libtorrent::peer_class_type_filter {lvalue},libtorrent::peer_class_type_filter::socket_type_t,libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def allow(self, peer_class_type_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        allow( (peer_class_type_filter)arg1, (peer_class_type_filter_socket_type_t)arg2, (object)arg3) -> None :
        
            C++ signature :
                void allow(libtorrent::peer_class_type_filter {lvalue},libtorrent::peer_class_type_filter::socket_type_t,libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def apply(self, peer_class_type_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        apply( (peer_class_type_filter)arg1, (peer_class_type_filter_socket_type_t)arg2, (int)arg3) -> int :
        
            C++ signature :
                unsigned int apply(libtorrent::peer_class_type_filter {lvalue},libtorrent::peer_class_type_filter::socket_type_t,unsigned int)
        """
        pass

    def disallow(self, peer_class_type_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        disallow( (peer_class_type_filter)arg1, (peer_class_type_filter_socket_type_t)arg2, (object)arg3) -> None :
        
            C++ signature :
                void disallow(libtorrent::peer_class_type_filter {lvalue},libtorrent::peer_class_type_filter::socket_type_t,libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def remove(self, peer_class_type_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        remove( (peer_class_type_filter)arg1, (peer_class_type_filter_socket_type_t)arg2, (object)arg3) -> None :
        
            C++ signature :
                void remove(libtorrent::peer_class_type_filter {lvalue},libtorrent::peer_class_type_filter::socket_type_t,libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    i2p_socket = 4
    ssl_tcp_socket = 2
    ssl_utp_socket = 3
    tcp_socket = 0
    utp_socket = 1
    __instance_size__ = 56


