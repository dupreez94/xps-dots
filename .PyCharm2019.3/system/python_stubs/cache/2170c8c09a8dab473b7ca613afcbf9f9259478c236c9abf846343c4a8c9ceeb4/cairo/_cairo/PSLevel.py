# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class PSLevel(__cairo._IntEnum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    LEVEL_2 = 0
    LEVEL_3 = 1
    __map = {
        0: 'LEVEL_2',
        1: 'LEVEL_3',
    }


