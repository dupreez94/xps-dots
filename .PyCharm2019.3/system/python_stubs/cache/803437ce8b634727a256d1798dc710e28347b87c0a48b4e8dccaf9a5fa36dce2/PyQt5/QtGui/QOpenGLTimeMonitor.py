# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QOpenGLTimeMonitor(__PyQt5_QtCore.QObject):
    """ QOpenGLTimeMonitor(parent: QObject = None) """
    def create(self): # real signature unknown; restored from __doc__
        """ create(self) -> bool """
        return False

    def destroy(self): # real signature unknown; restored from __doc__
        """ destroy(self) """
        pass

    def isCreated(self): # real signature unknown; restored from __doc__
        """ isCreated(self) -> bool """
        return False

    def isResultAvailable(self): # real signature unknown; restored from __doc__
        """ isResultAvailable(self) -> bool """
        return False

    def objectIds(self): # real signature unknown; restored from __doc__
        """ objectIds(self) -> List[int] """
        return []

    def recordSample(self): # real signature unknown; restored from __doc__
        """ recordSample(self) -> int """
        return 0

    def reset(self): # real signature unknown; restored from __doc__
        """ reset(self) """
        pass

    def sampleCount(self): # real signature unknown; restored from __doc__
        """ sampleCount(self) -> int """
        return 0

    def setSampleCount(self, p_int): # real signature unknown; restored from __doc__
        """ setSampleCount(self, int) """
        pass

    def waitForIntervals(self): # real signature unknown; restored from __doc__
        """ waitForIntervals(self) -> List[int] """
        return []

    def waitForSamples(self): # real signature unknown; restored from __doc__
        """ waitForSamples(self) -> List[int] """
        return []

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


