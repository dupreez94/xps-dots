# encoding: utf-8
# module PyQt5._QOpenGLFunctions_4_1_Core
# from /usr/lib/python3.8/site-packages/PyQt5/_QOpenGLFunctions_4_1_Core.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtGui as __PyQt5_QtGui


# no functions
# classes

class QOpenGLFunctions_4_1_Core(__PyQt5_QtGui.QAbstractOpenGLFunctions):
    """ QOpenGLFunctions_4_1_Core() """
    def glActiveShaderProgram(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glActiveShaderProgram(self, int, int) """
        pass

    def glActiveTexture(self, p_int): # real signature unknown; restored from __doc__
        """ glActiveTexture(self, int) """
        pass

    def glAttachShader(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glAttachShader(self, int, int) """
        pass

    def glBeginConditionalRender(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBeginConditionalRender(self, int, int) """
        pass

    def glBeginQuery(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBeginQuery(self, int, int) """
        pass

    def glBeginQueryIndexed(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glBeginQueryIndexed(self, int, int, int) """
        pass

    def glBeginTransformFeedback(self, p_int): # real signature unknown; restored from __doc__
        """ glBeginTransformFeedback(self, int) """
        pass

    def glBindAttribLocation(self, p_int, p_int_1, p_str): # real signature unknown; restored from __doc__
        """ glBindAttribLocation(self, int, int, str) """
        pass

    def glBindBuffer(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindBuffer(self, int, int) """
        pass

    def glBindBufferBase(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glBindBufferBase(self, int, int, int) """
        pass

    def glBindFramebuffer(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindFramebuffer(self, int, int) """
        pass

    def glBindProgramPipeline(self, p_int): # real signature unknown; restored from __doc__
        """ glBindProgramPipeline(self, int) """
        pass

    def glBindRenderbuffer(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindRenderbuffer(self, int, int) """
        pass

    def glBindSampler(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindSampler(self, int, int) """
        pass

    def glBindTexture(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindTexture(self, int, int) """
        pass

    def glBindTransformFeedback(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindTransformFeedback(self, int, int) """
        pass

    def glBindVertexArray(self, p_int): # real signature unknown; restored from __doc__
        """ glBindVertexArray(self, int) """
        pass

    def glBlendColor(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glBlendColor(self, float, float, float, float) """
        pass

    def glBlendEquation(self, p_int): # real signature unknown; restored from __doc__
        """ glBlendEquation(self, int) """
        pass

    def glBlendEquationi(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBlendEquationi(self, int, int) """
        pass

    def glBlendEquationSeparate(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBlendEquationSeparate(self, int, int) """
        pass

    def glBlendEquationSeparatei(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glBlendEquationSeparatei(self, int, int, int) """
        pass

    def glBlendFunc(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBlendFunc(self, int, int) """
        pass

    def glBlendFunci(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glBlendFunci(self, int, int, int) """
        pass

    def glBlendFuncSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glBlendFuncSeparate(self, int, int, int, int) """
        pass

    def glBlendFuncSeparatei(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glBlendFuncSeparatei(self, int, int, int, int, int) """
        pass

    def glBlitFramebuffer(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, p_int_9): # real signature unknown; restored from __doc__
        """ glBlitFramebuffer(self, int, int, int, int, int, int, int, int, int, int) """
        pass

    def glBufferData(self, p_int, p_int_1, PYQT_OPENGL_ARRAY, p_int_2): # real signature unknown; restored from __doc__
        """ glBufferData(self, int, int, PYQT_OPENGL_ARRAY, int) """
        pass

    def glBufferSubData(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glBufferSubData(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCheckFramebufferStatus(self, p_int): # real signature unknown; restored from __doc__
        """ glCheckFramebufferStatus(self, int) -> int """
        return 0

    def glClampColor(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glClampColor(self, int, int) """
        pass

    def glClear(self, p_int): # real signature unknown; restored from __doc__
        """ glClear(self, int) """
        pass

    def glClearBufferfi(self, p_int, p_int_1, p_float, p_int_2): # real signature unknown; restored from __doc__
        """ glClearBufferfi(self, int, int, float, int) """
        pass

    def glClearColor(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glClearColor(self, float, float, float, float) """
        pass

    def glClearDepth(self, p_float): # real signature unknown; restored from __doc__
        """ glClearDepth(self, float) """
        pass

    def glClearDepthf(self, p_float): # real signature unknown; restored from __doc__
        """ glClearDepthf(self, float) """
        pass

    def glClearStencil(self, p_int): # real signature unknown; restored from __doc__
        """ glClearStencil(self, int) """
        pass

    def glColorMask(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColorMask(self, int, int, int, int) """
        pass

    def glColorMaski(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glColorMaski(self, int, int, int, int, int) """
        pass

    def glColorP3ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glColorP3ui(self, int, int) """
        pass

    def glColorP4ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glColorP4ui(self, int, int) """
        pass

    def glCompileShader(self, p_int): # real signature unknown; restored from __doc__
        """ glCompileShader(self, int) """
        pass

    def glCompressedTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage2D(self, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage3D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, p_int_9, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage3D(self, int, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCopyTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6): # real signature unknown; restored from __doc__
        """ glCopyTexImage1D(self, int, int, int, int, int, int, int) """
        pass

    def glCopyTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7): # real signature unknown; restored from __doc__
        """ glCopyTexImage2D(self, int, int, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage1D(self, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage2D(self, int, int, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage3D(self, int, int, int, int, int, int, int, int, int) """
        pass

    def glCreateProgram(self): # real signature unknown; restored from __doc__
        """ glCreateProgram(self) -> int """
        return 0

    def glCreateShader(self, p_int): # real signature unknown; restored from __doc__
        """ glCreateShader(self, int) -> int """
        return 0

    def glCullFace(self, p_int): # real signature unknown; restored from __doc__
        """ glCullFace(self, int) """
        pass

    def glDeleteBuffers(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteBuffers(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDeleteProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glDeleteProgram(self, int) """
        pass

    def glDeleteQueries(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteQueries(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDeleteShader(self, p_int): # real signature unknown; restored from __doc__
        """ glDeleteShader(self, int) """
        pass

    def glDeleteTextures(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteTextures(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDepthFunc(self, p_int): # real signature unknown; restored from __doc__
        """ glDepthFunc(self, int) """
        pass

    def glDepthMask(self, p_int): # real signature unknown; restored from __doc__
        """ glDepthMask(self, int) """
        pass

    def glDepthRange(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glDepthRange(self, float, float) """
        pass

    def glDepthRangef(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glDepthRangef(self, float, float) """
        pass

    def glDepthRangeIndexed(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glDepthRangeIndexed(self, int, float, float) """
        pass

    def glDetachShader(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glDetachShader(self, int, int) """
        pass

    def glDisable(self, p_int): # real signature unknown; restored from __doc__
        """ glDisable(self, int) """
        pass

    def glDisablei(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glDisablei(self, int, int) """
        pass

    def glDisableVertexAttribArray(self, p_int): # real signature unknown; restored from __doc__
        """ glDisableVertexAttribArray(self, int) """
        pass

    def glDrawArrays(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glDrawArrays(self, int, int, int) """
        pass

    def glDrawArraysInstanced(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glDrawArraysInstanced(self, int, int, int, int) """
        pass

    def glDrawBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glDrawBuffer(self, int) """
        pass

    def glDrawBuffers(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawBuffers(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawElements(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawElements(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawRangeElements(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawRangeElements(self, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawTransformFeedback(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glDrawTransformFeedback(self, int, int) """
        pass

    def glDrawTransformFeedbackStream(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glDrawTransformFeedbackStream(self, int, int, int) """
        pass

    def glEnable(self, p_int): # real signature unknown; restored from __doc__
        """ glEnable(self, int) """
        pass

    def glEnablei(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glEnablei(self, int, int) """
        pass

    def glEnableVertexAttribArray(self, p_int): # real signature unknown; restored from __doc__
        """ glEnableVertexAttribArray(self, int) """
        pass

    def glEndConditionalRender(self): # real signature unknown; restored from __doc__
        """ glEndConditionalRender(self) """
        pass

    def glEndQuery(self, p_int): # real signature unknown; restored from __doc__
        """ glEndQuery(self, int) """
        pass

    def glEndQueryIndexed(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glEndQueryIndexed(self, int, int) """
        pass

    def glEndTransformFeedback(self): # real signature unknown; restored from __doc__
        """ glEndTransformFeedback(self) """
        pass

    def glFinish(self): # real signature unknown; restored from __doc__
        """ glFinish(self) """
        pass

    def glFlush(self): # real signature unknown; restored from __doc__
        """ glFlush(self) """
        pass

    def glFramebufferRenderbuffer(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glFramebufferRenderbuffer(self, int, int, int, int) """
        pass

    def glFramebufferTexture(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glFramebufferTexture(self, int, int, int, int) """
        pass

    def glFramebufferTexture1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glFramebufferTexture1D(self, int, int, int, int, int) """
        pass

    def glFramebufferTexture2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glFramebufferTexture2D(self, int, int, int, int, int) """
        pass

    def glFramebufferTexture3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glFramebufferTexture3D(self, int, int, int, int, int, int) """
        pass

    def glFramebufferTextureLayer(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glFramebufferTextureLayer(self, int, int, int, int, int) """
        pass

    def glFrontFace(self, p_int): # real signature unknown; restored from __doc__
        """ glFrontFace(self, int) """
        pass

    def glGenBuffers(self, p_int): # real signature unknown; restored from __doc__
        """ glGenBuffers(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGenerateMipmap(self, p_int): # real signature unknown; restored from __doc__
        """ glGenerateMipmap(self, int) """
        pass

    def glGenQueries(self, p_int): # real signature unknown; restored from __doc__
        """ glGenQueries(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGenTextures(self, p_int): # real signature unknown; restored from __doc__
        """ glGenTextures(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGetActiveAttrib(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetActiveAttrib(self, int, int) -> Tuple[str, int, int] """
        pass

    def glGetActiveUniform(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetActiveUniform(self, int, int) -> Tuple[str, int, int] """
        pass

    def glGetAttachedShaders(self, p_int): # real signature unknown; restored from __doc__
        """ glGetAttachedShaders(self, int) -> Tuple[int, ...] """
        pass

    def glGetAttribLocation(self, p_int, p_str): # real signature unknown; restored from __doc__
        """ glGetAttribLocation(self, int, str) -> int """
        return 0

    def glGetBooleanv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetBooleanv(self, int) -> Union[bool, Tuple[bool, ...]] """
        pass

    def glGetBufferParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetBufferParameteriv(self, int, int) -> int """
        return 0

    def glGetDoublev(self, p_int): # real signature unknown; restored from __doc__
        """ glGetDoublev(self, int) -> Union[float, Tuple[float, ...]] """
        pass

    def glGetError(self): # real signature unknown; restored from __doc__
        """ glGetError(self) -> int """
        return 0

    def glGetFloatv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetFloatv(self, int) -> Union[float, Tuple[float, ...]] """
        pass

    def glGetIntegerv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetIntegerv(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGetProgramInfoLog(self, p_int): # real signature unknown; restored from __doc__
        """ glGetProgramInfoLog(self, int) -> bytes """
        return b""

    def glGetProgramiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetProgramiv(self, int, int) -> Union[int, Tuple[int, int, int]] """
        pass

    def glGetQueryiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetQueryiv(self, int, int) -> int """
        return 0

    def glGetShaderInfoLog(self, p_int): # real signature unknown; restored from __doc__
        """ glGetShaderInfoLog(self, int) -> bytes """
        return b""

    def glGetShaderiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetShaderiv(self, int, int) -> int """
        return 0

    def glGetShaderSource(self, p_int): # real signature unknown; restored from __doc__
        """ glGetShaderSource(self, int) -> bytes """
        return b""

    def glGetString(self, p_int): # real signature unknown; restored from __doc__
        """ glGetString(self, int) -> str """
        return ""

    def glGetTexLevelParameterfv(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glGetTexLevelParameterfv(self, int, int, int) -> float """
        return 0.0

    def glGetTexLevelParameteriv(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glGetTexLevelParameteriv(self, int, int, int) -> int """
        return 0

    def glGetTexParameterfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexParameterfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetTexParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexParameteriv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetUniformLocation(self, p_int, p_str): # real signature unknown; restored from __doc__
        """ glGetUniformLocation(self, int, str) -> int """
        return 0

    def glGetVertexAttribdv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribdv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetVertexAttribfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetVertexAttribiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribiv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glHint(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glHint(self, int, int) """
        pass

    def glIndexub(self, p_int): # real signature unknown; restored from __doc__
        """ glIndexub(self, int) """
        pass

    def glIndexubv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexubv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glIsBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glIsBuffer(self, int) -> int """
        return 0

    def glIsEnabled(self, p_int): # real signature unknown; restored from __doc__
        """ glIsEnabled(self, int) -> int """
        return 0

    def glIsEnabledi(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glIsEnabledi(self, int, int) -> int """
        return 0

    def glIsFramebuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glIsFramebuffer(self, int) -> int """
        return 0

    def glIsProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glIsProgram(self, int) -> int """
        return 0

    def glIsProgramPipeline(self, p_int): # real signature unknown; restored from __doc__
        """ glIsProgramPipeline(self, int) -> int """
        return 0

    def glIsQuery(self, p_int): # real signature unknown; restored from __doc__
        """ glIsQuery(self, int) -> int """
        return 0

    def glIsRenderbuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glIsRenderbuffer(self, int) -> int """
        return 0

    def glIsSampler(self, p_int): # real signature unknown; restored from __doc__
        """ glIsSampler(self, int) -> int """
        return 0

    def glIsShader(self, p_int): # real signature unknown; restored from __doc__
        """ glIsShader(self, int) -> int """
        return 0

    def glIsTexture(self, p_int): # real signature unknown; restored from __doc__
        """ glIsTexture(self, int) -> int """
        return 0

    def glIsTransformFeedback(self, p_int): # real signature unknown; restored from __doc__
        """ glIsTransformFeedback(self, int) -> int """
        return 0

    def glIsVertexArray(self, p_int): # real signature unknown; restored from __doc__
        """ glIsVertexArray(self, int) -> int """
        return 0

    def glLineWidth(self, p_float): # real signature unknown; restored from __doc__
        """ glLineWidth(self, float) """
        pass

    def glLinkProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glLinkProgram(self, int) """
        pass

    def glLogicOp(self, p_int): # real signature unknown; restored from __doc__
        """ glLogicOp(self, int) """
        pass

    def glMinSampleShading(self, p_float): # real signature unknown; restored from __doc__
        """ glMinSampleShading(self, float) """
        pass

    def glMultiTexCoordP1ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoordP1ui(self, int, int, int) """
        pass

    def glMultiTexCoordP2ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoordP2ui(self, int, int, int) """
        pass

    def glMultiTexCoordP3ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoordP3ui(self, int, int, int) """
        pass

    def glMultiTexCoordP4ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoordP4ui(self, int, int, int) """
        pass

    def glNormalP3ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glNormalP3ui(self, int, int) """
        pass

    def glPatchParameteri(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPatchParameteri(self, int, int) """
        pass

    def glPauseTransformFeedback(self): # real signature unknown; restored from __doc__
        """ glPauseTransformFeedback(self) """
        pass

    def glPixelStoref(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glPixelStoref(self, int, float) """
        pass

    def glPixelStorei(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPixelStorei(self, int, int) """
        pass

    def glPointParameterf(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glPointParameterf(self, int, float) """
        pass

    def glPointParameterfv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPointParameterfv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPointParameteri(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPointParameteri(self, int, int) """
        pass

    def glPointParameteriv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPointParameteriv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPointSize(self, p_float): # real signature unknown; restored from __doc__
        """ glPointSize(self, float) """
        pass

    def glPolygonMode(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPolygonMode(self, int, int) """
        pass

    def glPolygonOffset(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glPolygonOffset(self, float, float) """
        pass

    def glPrimitiveRestartIndex(self, p_int): # real signature unknown; restored from __doc__
        """ glPrimitiveRestartIndex(self, int) """
        pass

    def glProgramParameteri(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glProgramParameteri(self, int, int, int) """
        pass

    def glProgramUniform1d(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glProgramUniform1d(self, int, int, float) """
        pass

    def glProgramUniform1f(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glProgramUniform1f(self, int, int, float) """
        pass

    def glProgramUniform1i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glProgramUniform1i(self, int, int, int) """
        pass

    def glProgramUniform1ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glProgramUniform1ui(self, int, int, int) """
        pass

    def glProgramUniform2d(self, p_int, p_int_1, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glProgramUniform2d(self, int, int, float, float) """
        pass

    def glProgramUniform2f(self, p_int, p_int_1, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glProgramUniform2f(self, int, int, float, float) """
        pass

    def glProgramUniform2i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glProgramUniform2i(self, int, int, int, int) """
        pass

    def glProgramUniform2ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glProgramUniform2ui(self, int, int, int, int) """
        pass

    def glProgramUniform3d(self, p_int, p_int_1, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glProgramUniform3d(self, int, int, float, float, float) """
        pass

    def glProgramUniform3f(self, p_int, p_int_1, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glProgramUniform3f(self, int, int, float, float, float) """
        pass

    def glProgramUniform3i(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glProgramUniform3i(self, int, int, int, int, int) """
        pass

    def glProgramUniform3ui(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glProgramUniform3ui(self, int, int, int, int, int) """
        pass

    def glProgramUniform4d(self, p_int, p_int_1, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glProgramUniform4d(self, int, int, float, float, float, float) """
        pass

    def glProgramUniform4f(self, p_int, p_int_1, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glProgramUniform4f(self, int, int, float, float, float, float) """
        pass

    def glProgramUniform4i(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glProgramUniform4i(self, int, int, int, int, int, int) """
        pass

    def glProgramUniform4ui(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glProgramUniform4ui(self, int, int, int, int, int, int) """
        pass

    def glProvokingVertex(self, p_int): # real signature unknown; restored from __doc__
        """ glProvokingVertex(self, int) """
        pass

    def glQueryCounter(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glQueryCounter(self, int, int) """
        pass

    def glReadBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glReadBuffer(self, int) """
        pass

    def glReadPixels(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glReadPixels(self, int, int, int, int, int, int) -> Union[Tuple[float, ...], Tuple[int, ...]] """
        pass

    def glReleaseShaderCompiler(self): # real signature unknown; restored from __doc__
        """ glReleaseShaderCompiler(self) """
        pass

    def glRenderbufferStorage(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glRenderbufferStorage(self, int, int, int, int) """
        pass

    def glRenderbufferStorageMultisample(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glRenderbufferStorageMultisample(self, int, int, int, int, int) """
        pass

    def glResumeTransformFeedback(self): # real signature unknown; restored from __doc__
        """ glResumeTransformFeedback(self) """
        pass

    def glSampleCoverage(self, p_float, p_int): # real signature unknown; restored from __doc__
        """ glSampleCoverage(self, float, int) """
        pass

    def glSampleMaski(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glSampleMaski(self, int, int) """
        pass

    def glSamplerParameterf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glSamplerParameterf(self, int, int, float) """
        pass

    def glSamplerParameteri(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSamplerParameteri(self, int, int, int) """
        pass

    def glScissor(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glScissor(self, int, int, int, int) """
        pass

    def glScissorIndexed(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glScissorIndexed(self, int, int, int, int, int) """
        pass

    def glSecondaryColorP3ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glSecondaryColorP3ui(self, int, int) """
        pass

    def glStencilFunc(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glStencilFunc(self, int, int, int) """
        pass

    def glStencilFuncSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glStencilFuncSeparate(self, int, int, int, int) """
        pass

    def glStencilMask(self, p_int): # real signature unknown; restored from __doc__
        """ glStencilMask(self, int) """
        pass

    def glStencilMaskSeparate(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glStencilMaskSeparate(self, int, int) """
        pass

    def glStencilOp(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glStencilOp(self, int, int, int) """
        pass

    def glStencilOpSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glStencilOpSeparate(self, int, int, int, int) """
        pass

    def glTexBuffer(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexBuffer(self, int, int, int) """
        pass

    def glTexCoordP1ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoordP1ui(self, int, int) """
        pass

    def glTexCoordP2ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoordP2ui(self, int, int) """
        pass

    def glTexCoordP3ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoordP3ui(self, int, int) """
        pass

    def glTexCoordP4ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoordP4ui(self, int, int) """
        pass

    def glTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage1D(self, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage2DMultisample(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glTexImage2DMultisample(self, int, int, int, int, int, int) """
        pass

    def glTexImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage3D(self, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage3DMultisample(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6): # real signature unknown; restored from __doc__
        """ glTexImage3DMultisample(self, int, int, int, int, int, int, int) """
        pass

    def glTexParameterf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glTexParameterf(self, int, int, float) """
        pass

    def glTexParameterfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexParameterfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexParameteri(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexParameteri(self, int, int, int) """
        pass

    def glTexParameteriv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexParameteriv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, p_int_9, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage3D(self, int, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform1d(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glUniform1d(self, int, float) """
        pass

    def glUniform1f(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glUniform1f(self, int, float) """
        pass

    def glUniform1fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform1fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform1i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glUniform1i(self, int, int) """
        pass

    def glUniform1iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform1iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform1ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glUniform1ui(self, int, int) """
        pass

    def glUniform2d(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glUniform2d(self, int, float, float) """
        pass

    def glUniform2f(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glUniform2f(self, int, float, float) """
        pass

    def glUniform2fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform2fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform2i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glUniform2i(self, int, int, int) """
        pass

    def glUniform2iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform2iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform2ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glUniform2ui(self, int, int, int) """
        pass

    def glUniform3d(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glUniform3d(self, int, float, float, float) """
        pass

    def glUniform3f(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glUniform3f(self, int, float, float, float) """
        pass

    def glUniform3fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform3fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform3i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glUniform3i(self, int, int, int, int) """
        pass

    def glUniform3iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform3iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform3ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glUniform3ui(self, int, int, int, int) """
        pass

    def glUniform4d(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glUniform4d(self, int, float, float, float, float) """
        pass

    def glUniform4f(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glUniform4f(self, int, float, float, float, float) """
        pass

    def glUniform4fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform4fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform4i(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glUniform4i(self, int, int, int, int, int) """
        pass

    def glUniform4iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform4iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform4ui(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glUniform4ui(self, int, int, int, int, int) """
        pass

    def glUniformBlockBinding(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glUniformBlockBinding(self, int, int, int) """
        pass

    def glUniformMatrix2fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix2fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniformMatrix3fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix3fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniformMatrix4fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix4fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUnmapBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glUnmapBuffer(self, int) -> int """
        return 0

    def glUseProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glUseProgram(self, int) """
        pass

    def glUseProgramStages(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glUseProgramStages(self, int, int, int) """
        pass

    def glValidateProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glValidateProgram(self, int) """
        pass

    def glValidateProgramPipeline(self, p_int): # real signature unknown; restored from __doc__
        """ glValidateProgramPipeline(self, int) """
        pass

    def glVertexAttribDivisor(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertexAttribDivisor(self, int, int) """
        pass

    def glVertexAttribL1d(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glVertexAttribL1d(self, int, float) """
        pass

    def glVertexAttribL2d(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glVertexAttribL2d(self, int, float, float) """
        pass

    def glVertexAttribL3d(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glVertexAttribL3d(self, int, float, float, float) """
        pass

    def glVertexAttribL4d(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glVertexAttribL4d(self, int, float, float, float, float) """
        pass

    def glVertexAttribP1ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertexAttribP1ui(self, int, int, int, int) """
        pass

    def glVertexAttribP2ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertexAttribP2ui(self, int, int, int, int) """
        pass

    def glVertexAttribP3ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertexAttribP3ui(self, int, int, int, int) """
        pass

    def glVertexAttribP4ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertexAttribP4ui(self, int, int, int, int) """
        pass

    def glVertexAttribPointer(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttribPointer(self, int, int, int, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glVertexP2ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertexP2ui(self, int, int) """
        pass

    def glVertexP3ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertexP3ui(self, int, int) """
        pass

    def glVertexP4ui(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertexP4ui(self, int, int) """
        pass

    def glViewport(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glViewport(self, int, int, int, int) """
        pass

    def glViewportIndexedf(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glViewportIndexedf(self, int, float, float, float, float) """
        pass

    def initializeOpenGLFunctions(self): # real signature unknown; restored from __doc__
        """ initializeOpenGLFunctions(self) -> bool """
        return False

    def __init__(self): # real signature unknown; restored from __doc__
        pass


# variables with complex values



