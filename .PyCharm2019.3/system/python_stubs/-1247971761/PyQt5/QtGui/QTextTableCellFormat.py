# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QTextCharFormat import QTextCharFormat

class QTextTableCellFormat(QTextCharFormat):
    """
    QTextTableCellFormat()
    QTextTableCellFormat(QTextTableCellFormat)
    """
    def bottomBorder(self): # real signature unknown; restored from __doc__
        """ bottomBorder(self) -> float """
        return 0.0

    def bottomBorderBrush(self): # real signature unknown; restored from __doc__
        """ bottomBorderBrush(self) -> QBrush """
        return QBrush

    def bottomBorderStyle(self): # real signature unknown; restored from __doc__
        """ bottomBorderStyle(self) -> QTextFrameFormat.BorderStyle """
        pass

    def bottomPadding(self): # real signature unknown; restored from __doc__
        """ bottomPadding(self) -> float """
        return 0.0

    def isValid(self): # real signature unknown; restored from __doc__
        """ isValid(self) -> bool """
        return False

    def leftBorder(self): # real signature unknown; restored from __doc__
        """ leftBorder(self) -> float """
        return 0.0

    def leftBorderBrush(self): # real signature unknown; restored from __doc__
        """ leftBorderBrush(self) -> QBrush """
        return QBrush

    def leftBorderStyle(self): # real signature unknown; restored from __doc__
        """ leftBorderStyle(self) -> QTextFrameFormat.BorderStyle """
        pass

    def leftPadding(self): # real signature unknown; restored from __doc__
        """ leftPadding(self) -> float """
        return 0.0

    def rightBorder(self): # real signature unknown; restored from __doc__
        """ rightBorder(self) -> float """
        return 0.0

    def rightBorderBrush(self): # real signature unknown; restored from __doc__
        """ rightBorderBrush(self) -> QBrush """
        return QBrush

    def rightBorderStyle(self): # real signature unknown; restored from __doc__
        """ rightBorderStyle(self) -> QTextFrameFormat.BorderStyle """
        pass

    def rightPadding(self): # real signature unknown; restored from __doc__
        """ rightPadding(self) -> float """
        return 0.0

    def setBorder(self, p_float): # real signature unknown; restored from __doc__
        """ setBorder(self, float) """
        pass

    def setBorderBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setBorderBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setBorderStyle(self, QTextFrameFormat_BorderStyle): # real signature unknown; restored from __doc__
        """ setBorderStyle(self, QTextFrameFormat.BorderStyle) """
        pass

    def setBottomBorder(self, p_float): # real signature unknown; restored from __doc__
        """ setBottomBorder(self, float) """
        pass

    def setBottomBorderBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setBottomBorderBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setBottomBorderStyle(self, QTextFrameFormat_BorderStyle): # real signature unknown; restored from __doc__
        """ setBottomBorderStyle(self, QTextFrameFormat.BorderStyle) """
        pass

    def setBottomPadding(self, p_float): # real signature unknown; restored from __doc__
        """ setBottomPadding(self, float) """
        pass

    def setLeftBorder(self, p_float): # real signature unknown; restored from __doc__
        """ setLeftBorder(self, float) """
        pass

    def setLeftBorderBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setLeftBorderBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setLeftBorderStyle(self, QTextFrameFormat_BorderStyle): # real signature unknown; restored from __doc__
        """ setLeftBorderStyle(self, QTextFrameFormat.BorderStyle) """
        pass

    def setLeftPadding(self, p_float): # real signature unknown; restored from __doc__
        """ setLeftPadding(self, float) """
        pass

    def setPadding(self, p_float): # real signature unknown; restored from __doc__
        """ setPadding(self, float) """
        pass

    def setRightBorder(self, p_float): # real signature unknown; restored from __doc__
        """ setRightBorder(self, float) """
        pass

    def setRightBorderBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setRightBorderBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setRightBorderStyle(self, QTextFrameFormat_BorderStyle): # real signature unknown; restored from __doc__
        """ setRightBorderStyle(self, QTextFrameFormat.BorderStyle) """
        pass

    def setRightPadding(self, p_float): # real signature unknown; restored from __doc__
        """ setRightPadding(self, float) """
        pass

    def setTopBorder(self, p_float): # real signature unknown; restored from __doc__
        """ setTopBorder(self, float) """
        pass

    def setTopBorderBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setTopBorderBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setTopBorderStyle(self, QTextFrameFormat_BorderStyle): # real signature unknown; restored from __doc__
        """ setTopBorderStyle(self, QTextFrameFormat.BorderStyle) """
        pass

    def setTopPadding(self, p_float): # real signature unknown; restored from __doc__
        """ setTopPadding(self, float) """
        pass

    def topBorder(self): # real signature unknown; restored from __doc__
        """ topBorder(self) -> float """
        return 0.0

    def topBorderBrush(self): # real signature unknown; restored from __doc__
        """ topBorderBrush(self) -> QBrush """
        return QBrush

    def topBorderStyle(self): # real signature unknown; restored from __doc__
        """ topBorderStyle(self) -> QTextFrameFormat.BorderStyle """
        pass

    def topPadding(self): # real signature unknown; restored from __doc__
        """ topPadding(self) -> float """
        return 0.0

    def __init__(self, QTextTableCellFormat=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


