# encoding: utf-8
# module PyKF5.KAuth
# from /usr/lib/python3.8/site-packages/PyKF5/KAuth.so
# by generator 1.147
# no doc

# imports
import sip as __sip


# no functions
# classes

class KAuth(__sip.simplewrapper):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Action = None # (!) real value is "<class 'PyKF5.KAuth.KAuth.Action'>"
    ActionReply = None # (!) real value is "<class 'PyKF5.KAuth.KAuth.ActionReply'>"
    ExecuteJob = None # (!) real value is "<class 'PyKF5.KAuth.KAuth.ExecuteJob'>"
    HelperSupport = None # (!) real value is "<class 'PyKF5.KAuth.KAuth.HelperSupport'>"
    ObjectDecorator = None # (!) real value is "<class 'PyKF5.KAuth.KAuth.ObjectDecorator'>"


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KAuth', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KAuth.so')"

